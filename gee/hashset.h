#ifndef __VALAGEE_HASHSET_H__
#define __VALAGEE_HASHSET_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/set.h"

G_BEGIN_DECLS

#define VALA_TYPE_HASH_SET (vala_hash_set_get_type ())
#define VALA_HASH_SET(obj)                                                    \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_HASH_SET, ValaHashSet))
#define VALA_HASH_SET_CLASS(klass)                                            \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_HASH_SET, ValaHashSetClass))
#define VALA_IS_HASH_SET(obj)                                                 \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_HASH_SET))
#define VALA_IS_HASH_SET_CLASS(klass)                                         \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_HASH_SET))
#define VALA_HASH_SET_GET_CLASS(obj)                                          \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_HASH_SET, ValaHashSetClass))

typedef struct _ValaHashSetPrivate ValaHashSetPrivate;

typedef struct _ValaHashSet
{
  ValaSet parent_instance;
  ValaHashSetPrivate *priv;
} ValaHashSet;

typedef struct _ValaHashSetClass
{
  ValaSetClass parent_class;
} ValaHashSetClass;

GType vala_hash_set_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaHashSet, vala_iterable_unref)
void vala_hash_set_set_hash_func (ValaHashSet *self, GHashFunc value);
void vala_hash_set_set_equal_func (ValaHashSet *self, GEqualFunc value);
ValaHashSet *vala_hash_set_new (GType g_type, GBoxedCopyFunc g_dup_func,
                                GDestroyNotify g_destroy_func,
                                GHashFunc hash_func, GEqualFunc equal_func);
ValaHashSet *vala_hash_set_construct (GType object_type, GType g_type,
                                      GBoxedCopyFunc g_dup_func,
                                      GDestroyNotify g_destroy_func,
                                      GHashFunc hash_func,
                                      GEqualFunc equal_func);

G_END_DECLS

#endif
