#ifndef __VALAGEE_COLLECTION_H__
#define __VALAGEE_COLLECTION_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/iterable.h"

G_BEGIN_DECLS

#define VALA_TYPE_COLLECTION (vala_collection_get_type ())
#define VALA_COLLECTION(obj)                                                  \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_COLLECTION, ValaCollection))
#define VALA_COLLECTION_CLASS(klass)                                          \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_COLLECTION,                    \
                            ValaCollectionClass))
#define VALA_IS_COLLECTION(obj)                                               \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_COLLECTION))
#define VALA_IS_COLLECTION_CLASS(klass)                                       \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_COLLECTION))
#define VALA_COLLECTION_GET_CLASS(obj)                                        \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_COLLECTION,                    \
                              ValaCollectionClass))

typedef struct _ValaCollectionPrivate ValaCollectionPrivate;

typedef struct _ValaCollection
{
  ValaIterable parent_instance;
  ValaCollectionPrivate *priv;
} ValaCollection;

typedef struct _ValaCollectionClass
{
  ValaIterableClass parent_class;
  gint (*get_size) (ValaCollection *self);
  gboolean (*get_is_empty) (ValaCollection *self);
  gboolean (*contains) (ValaCollection *self, gconstpointer item);
  gboolean (*add) (ValaCollection *self, gconstpointer item);
  gboolean (*remove) (ValaCollection *self, gconstpointer item);
  void (*clear) (ValaCollection *self);
  gboolean (*add_all) (ValaCollection *self, ValaCollection *collection);
  gpointer *(*to_array) (ValaCollection *self, gint *result_length1);
} ValaCollectionClass;

GType vala_collection_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaCollection, vala_iterable_unref)

gint vala_collection_get_size (ValaCollection *self);
gboolean vala_collection_get_is_empty (ValaCollection *self);
gboolean vala_collection_contains (ValaCollection *self, gconstpointer item);
gboolean vala_collection_add (ValaCollection *self, gconstpointer item);
gboolean vala_collection_remove (ValaCollection *self, gconstpointer item);
void vala_collection_clear (ValaCollection *self);
gboolean vala_collection_add_all (ValaCollection *self,
                                  ValaCollection *collection);
gpointer *vala_collection_to_array (ValaCollection *self,
                                    gint *result_length);
ValaCollection *vala_collection_construct (GType object_type, GType g_type,
                                           GBoxedCopyFunc g_dup_func,
                                           GDestroyNotify g_destroy_func);

G_END_DECLS

#endif
