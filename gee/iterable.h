#ifndef __VALAGEE_ITERABLE_H__
#define __VALAGEE_ITERABLE_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/iterator.h"

G_BEGIN_DECLS

#define VALA_TYPE_ITERABLE (vala_iterable_get_type ())
#define VALA_ITERABLE(obj)                                                    \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_ITERABLE, ValaIterable))
#define VALA_ITERABLE_CLASS(klass)                                            \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_ITERABLE, ValaIterableClass))
#define VALA_IS_ITERABLE(obj)                                                 \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_ITERABLE))
#define VALA_IS_ITERABLE_CLASS(klass)                                         \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_ITERABLE))
#define VALA_ITERABLE_GET_CLASS(obj)                                          \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_ITERABLE, ValaIterableClass))

typedef struct _ValaIterablePrivate ValaIterablePrivate;

typedef struct _ValaIterable
{
  GTypeInstance parent_instance;
  volatile int ref_count;
  ValaIterablePrivate *priv;
} ValaIterable;

typedef struct _ValaIterableClass
{
  GTypeClass parent_class;
  void (*finalize) (ValaIterable *self);
  GType (*get_element_type) (ValaIterable *self);
  ValaIterator *(*iterator) (ValaIterable *self);
} ValaIterableClass;

gpointer vala_iterable_ref (gpointer instance);
void vala_iterable_unref (gpointer instance);
GParamSpec *vala_param_spec_iterable (const gchar *name, const gchar *nick,
                                      const gchar *blurb, GType object_type,
                                      GParamFlags flags);
void vala_value_set_iterable (GValue *value, gpointer v_object);
void vala_value_take_iterable (GValue *value, gpointer v_object);
gpointer vala_value_get_iterable (const GValue *value);
GType vala_iterable_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaIterable, vala_iterable_unref)

GType vala_iterable_get_element_type (ValaIterable *self);
ValaIterator *vala_iterable_iterator (ValaIterable *self);
ValaIterable *vala_iterable_construct (GType object_type, GType g_type,
                                       GBoxedCopyFunc g_dup_func,
                                       GDestroyNotify g_destroy_func);

G_END_DECLS

#endif
