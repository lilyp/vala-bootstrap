#ifndef __VALAGEE_SET_H__
#define __VALAGEE_SET_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/collection.h"

G_BEGIN_DECLS

#define VALA_TYPE_SET (vala_set_get_type ())
#define VALA_SET(obj)                                                         \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_SET, ValaSet))
#define VALA_SET_CLASS(klass)                                                 \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_SET, ValaSetClass))
#define VALA_IS_SET(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_SET))
#define VALA_IS_SET_CLASS(klass)                                              \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_SET))
#define VALA_SET_GET_CLASS(obj)                                               \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_SET, ValaSetClass))

typedef struct _ValaSetPrivate ValaSetPrivate;

typedef struct _ValaSet
{
  ValaCollection parent_instance;
  ValaSetPrivate *priv;
} ValaSet;

typedef struct _ValaSetClass
{
  ValaCollectionClass parent_class;
} ValaSetClass;

GType vala_set_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaSet, vala_iterable_unref)
ValaSet *vala_set_construct (GType object_type, GType g_type,
                             GBoxedCopyFunc g_dup_func,
                             GDestroyNotify g_destroy_func);

G_END_DECLS

#endif
