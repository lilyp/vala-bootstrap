#ifndef __VALAGEE_MAPITERATOR_H__
#define __VALAGEE_MAPITERATOR_H__

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define VALA_TYPE_MAP_ITERATOR (vala_map_iterator_get_type ())
#define VALA_MAP_ITERATOR(obj)                                                \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_MAP_ITERATOR, ValaMapIterator))
#define VALA_MAP_ITERATOR_CLASS(klass)                                        \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_MAP_ITERATOR,                  \
                            ValaMapIteratorClass))
#define VALA_IS_MAP_ITERATOR(obj)                                             \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_MAP_ITERATOR))
#define VALA_IS_MAP_ITERATOR_CLASS(klass)                                     \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_MAP_ITERATOR))
#define VALA_MAP_ITERATOR_GET_CLASS(obj)                                      \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_MAP_ITERATOR,                  \
                              ValaMapIteratorClass))

typedef struct _ValaMapIteratorPrivate ValaMapIteratorPrivate;

typedef struct _ValaMapIterator
{
  GTypeInstance parent_instance;
  volatile int ref_count;
  ValaMapIteratorPrivate *priv;
} ValaMapIterator;

typedef struct _ValaMapIteratorClass
{
  GTypeClass parent_class;
  void (*finalize) (ValaMapIterator *self);
  gboolean (*next) (ValaMapIterator *self);
  gpointer (*get_key) (ValaMapIterator *self);
  gpointer (*get_value) (ValaMapIterator *self);
} ValaMapIteratorClass;

gpointer vala_map_iterator_ref (gpointer instance);
void vala_map_iterator_unref (gpointer instance);
GParamSpec *vala_param_spec_map_iterator (const gchar *name, const gchar *nick,
                                          const gchar *blurb,
                                          GType object_type,
                                          GParamFlags flags);
void vala_value_set_map_iterator (GValue *value, gpointer v_object);
void vala_value_take_map_iterator (GValue *value, gpointer v_object);
gpointer vala_value_get_map_iterator (const GValue *value);
GType vala_map_iterator_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaMapIterator, vala_map_iterator_unref)

gboolean vala_map_iterator_next (ValaMapIterator *self);
gpointer vala_map_iterator_get_key (ValaMapIterator *self);
gpointer vala_map_iterator_get_value (ValaMapIterator *self);
ValaMapIterator *vala_map_iterator_construct (GType object_type, GType k_type,
                                              GBoxedCopyFunc k_dup_func,
                                              GDestroyNotify k_destroy_func,
                                              GType v_type,
                                              GBoxedCopyFunc v_dup_func,
                                              GDestroyNotify v_destroy_func);

G_END_DECLS

#endif
