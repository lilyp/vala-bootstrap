#ifndef __VALAGEE_LIST_H__
#define __VALAGEE_LIST_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/collection.h"

#define VALA_TYPE_LIST (vala_list_get_type ())
#define VALA_LIST(obj)                                                        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_LIST, ValaList))
#define VALA_LIST_CLASS(klass)                                                \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_LIST, ValaListClass))
#define VALA_IS_LIST(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_LIST))
#define VALA_IS_LIST_CLASS(klass)                                             \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_LIST))
#define VALA_LIST_GET_CLASS(obj)                                              \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_LIST, ValaListClass))

typedef struct _ValaListPrivate ValaListPrivate;

typedef struct _ValaList
{
  ValaCollection parent_instance;
  ValaListPrivate *priv;
} ValaList;

typedef struct _ValaListClass
{
  ValaCollectionClass parent_class;
  gpointer (*get) (ValaList *self, gint index);
  void (*set) (ValaList *self, gint index, gconstpointer item);
  gint (*index_of) (ValaList *self, gconstpointer item);
  void (*insert) (ValaList *self, gint index, gconstpointer item);
  gpointer (*remove_at) (ValaList *self, gint index);
  gpointer (*first) (ValaList *self);
  gpointer (*last) (ValaList *self);
  void (*insert_all) (ValaList *self, gint index, ValaCollection *collection);
  void (*sort) (ValaList *self, GCompareDataFunc compare_func,
                gpointer compare_func_target,
                GDestroyNotify compare_func_target_destroy_notify);
} ValaListClass;

GType vala_list_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaList, vala_iterable_unref)

gpointer vala_list_get (ValaList *self, gint index);
void vala_list_set (ValaList *self, gint index, gconstpointer item);
gint vala_list_index_of (ValaList *self, gconstpointer item);
void vala_list_insert (ValaList *self, gint index, gconstpointer item);
gpointer vala_list_remove_at (ValaList *self, gint index);
gpointer vala_list_first (ValaList *self);
gpointer vala_list_last (ValaList *self);
void vala_list_insert_all (ValaList *self, gint index,
                           ValaCollection *collection);
void vala_list_sort (ValaList *self, GCompareDataFunc compare_func,
                     gpointer compare_func_target,
                     GDestroyNotify compare_func_target_destroy_notify);
ValaList *vala_list_construct (GType object_type, GType g_type,
                               GBoxedCopyFunc g_dup_func,
                               GDestroyNotify g_destroy_func);

G_END_DECLS

#endif
