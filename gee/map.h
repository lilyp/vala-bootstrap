#ifndef __VALAGEE_MAP_H__
#define __VALAGEE_MAP_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/mapiterator.h"
#include "gee/set.h"

G_BEGIN_DECLS

#define VALA_TYPE_MAP (vala_map_get_type ())
#define VALA_MAP(obj)                                                         \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_MAP, ValaMap))
#define VALA_MAP_CLASS(klass)                                                 \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_MAP, ValaMapClass))
#define VALA_IS_MAP(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_MAP))
#define VALA_IS_MAP_CLASS(klass)                                              \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_MAP))
#define VALA_MAP_GET_CLASS(obj)                                               \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_MAP, ValaMapClass))

typedef struct _ValaMapPrivate ValaMapPrivate;

typedef struct _ValaMap
{
  GTypeInstance parent_instance;
  volatile int ref_count;
  ValaMapPrivate *priv;
} ValaMap;

typedef struct _ValaMapClass
{
  GTypeClass parent_class;
  void (*finalize) (ValaMap *self);
  gint (*get_size) (ValaMap *self);
  ValaSet *(*get_keys) (ValaMap *self);
  ValaCollection *(*get_values) (ValaMap *self);
  gboolean (*contains) (ValaMap *self, gconstpointer key);
  gpointer (*get) (ValaMap *self, gconstpointer key);
  void (*set) (ValaMap *self, gconstpointer key, gconstpointer value);
  gboolean (*remove) (ValaMap *self, gconstpointer key);
  void (*clear) (ValaMap *self);
  ValaMapIterator *(*map_iterator) (ValaMap *self);
} ValaMapClass;

gpointer vala_map_ref (gpointer instance);
void vala_map_unref (gpointer instance);
GParamSpec *vala_param_spec_map (const gchar *name, const gchar *nick,
                                 const gchar *blurb, GType object_type,
                                 GParamFlags flags);
void vala_value_set_map (GValue *value, gpointer v_object);
void vala_value_take_map (GValue *value, gpointer v_object);
gpointer vala_value_get_map (const GValue *value);
GType vala_map_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaMap, vala_map_unref)

gint vala_map_get_size (ValaMap *self);
ValaSet *vala_map_get_keys (ValaMap *self);
ValaCollection *vala_map_get_values (ValaMap *self);
gboolean vala_map_contains (ValaMap *self, gconstpointer key);
gpointer vala_map_get (ValaMap *self, gconstpointer key);
void vala_map_set (ValaMap *self, gconstpointer key, gconstpointer value);
gboolean vala_map_remove (ValaMap *self, gconstpointer key);
void vala_map_clear (ValaMap *self);
ValaMapIterator *vala_map_map_iterator (ValaMap *self);
ValaMap *vala_map_construct (GType object_type, GType k_type,
                             GBoxedCopyFunc k_dup_func,
                             GDestroyNotify k_destroy_func, GType v_type,
                             GBoxedCopyFunc v_dup_func,
                             GDestroyNotify v_destroy_func);

G_END_DECLS

#endif
