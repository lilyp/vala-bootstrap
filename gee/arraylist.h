#ifndef __VALAGEE_ARRAYLIST_H__
#define __VALAGEE_ARRAYLIST_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/list.h"

G_BEGIN_DECLS

#define VALA_TYPE_ARRAY_LIST (vala_array_list_get_type ())
#define VALA_ARRAY_LIST(obj)                                                  \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_ARRAY_LIST, ValaArrayList))
#define VALA_ARRAY_LIST_CLASS(klass)                                          \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_ARRAY_LIST, ValaArrayListClass))
#define VALA_IS_ARRAY_LIST(obj)                                               \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_ARRAY_LIST))
#define VALA_IS_ARRAY_LIST_CLASS(klass)                                       \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_ARRAY_LIST))
#define VALA_ARRAY_LIST_GET_CLASS(obj)                                        \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_ARRAY_LIST, ValaArrayListClass))

typedef struct _ValaArrayListPrivate ValaArrayListPrivate;

typedef struct _ValaArrayList
{
  ValaList parent_instance;
  gpointer *items;
  gint capacity;
  gint size;
  ValaArrayListPrivate *priv;
} ValaArrayList;

typedef struct _ValaArrayListClass
{
  ValaListClass parent_class;
} ValaArrayListClass;

GType vala_array_list_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaArrayList, vala_iterable_unref)
void vala_array_list_set_equal_func (ValaArrayList *self, GEqualFunc value);
ValaArrayList *vala_array_list_new (GType g_type, GBoxedCopyFunc g_dup_func,
                                    GDestroyNotify g_destroy_func,
                                    GEqualFunc equal_func);
ValaArrayList *vala_array_list_construct (GType object_type, GType g_type,
                                          GBoxedCopyFunc g_dup_func,
                                          GDestroyNotify g_destroy_func,
                                          GEqualFunc equal_func);

G_END_DECLS

#endif
