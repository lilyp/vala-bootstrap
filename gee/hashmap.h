#ifndef __VALAGEE_HASHMAP_H__
#define __VALAGEE_HASHMAP_H__

#include <glib-object.h>
#include <glib.h>

#include "gee/map.h"

G_BEGIN_DECLS

#define VALA_TYPE_HASH_MAP (vala_hash_map_get_type ())
#define VALA_HASH_MAP(obj)                                                    \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_HASH_MAP, ValaHashMap))
#define VALA_HASH_MAP_CLASS(klass)                                            \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_HASH_MAP, ValaHashMapClass))
#define VALA_IS_HASH_MAP(obj)                                                 \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_HASH_MAP))
#define VALA_IS_HASH_MAP_CLASS(klass)                                         \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_HASH_MAP))
#define VALA_HASH_MAP_GET_CLASS(obj)                                          \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_HASH_MAP, ValaHashMapClass))

typedef struct _ValaHashMapPrivate ValaHashMapPrivate;

typedef struct _ValaHashMap
{
  ValaMap parent_instance;
  ValaHashMapPrivate *priv;
} ValaHashMap;

typedef struct _ValaHashMapClass
{
  ValaMapClass parent_class;
} ValaHashMapClass;

GType vala_hash_map_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaHashMap, vala_map_unref)
void vala_hash_map_set_key_hash_func (ValaHashMap *self, GHashFunc value);
void vala_hash_map_set_key_equal_func (ValaHashMap *self, GEqualFunc value);
void vala_hash_map_set_value_equal_func (ValaHashMap *self, GEqualFunc value);
ValaHashMap *vala_hash_map_new (GType k_type, GBoxedCopyFunc k_dup_func,
                                GDestroyNotify k_destroy_func, GType v_type,
                                GBoxedCopyFunc v_dup_func,
                                GDestroyNotify v_destroy_func,
                                GHashFunc key_hash_func,
                                GEqualFunc key_equal_func,
                                GEqualFunc value_equal_func);
ValaHashMap *vala_hash_map_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, GHashFunc key_hash_func,
    GEqualFunc key_equal_func, GEqualFunc value_equal_func);

G_END_DECLS

#endif
