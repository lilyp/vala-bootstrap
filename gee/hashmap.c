/* hashmap.c
 *
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 * Copyright (C) 1997-2000  GLib Team and others
 * Copyright (C) 2007-2009  Jürg Billeter
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.priker@gmail.com>
 */

#include "hashmap.h"
#include "collection.h"
#include <glib-object.h>
#include <glib.h>

struct _ValaHashMapPrivate
{
  GType k_type;
  GBoxedCopyFunc k_dup_func;
  GDestroyNotify k_destroy_func;
  GType v_type;
  GBoxedCopyFunc v_dup_func;
  GDestroyNotify v_destroy_func;
  gint n_nodes;
  gint n_buckets;
  struct _ValaHashMapNode **nodes;
  gint stamp;
  GHashFunc key_hash_func;
  GEqualFunc key_equal_func;
  GEqualFunc value_equal_func;
};

typedef struct _ValaHashMapNode
{
  gpointer key;
  gpointer value;
  struct _ValaHashMapNode *next;
  guint key_hash;
} ValaHashMapNode;

#define VALA_HASH_MAP_TYPE_KEY_SET (vala_hash_map_key_set_get_type ())
#define VALA_HASH_MAP_KEY_SET(obj)                                            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_HASH_MAP_TYPE_KEY_SET,             \
                               ValaHashMapKeySet))
#define VALA_HASH_MAP_KEY_SET_CLASS(klass)                                    \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_HASH_MAP_TYPE_KEY_SET,              \
                            ValaHashMapKeySetClass))
#define VALA_HASH_MAP_IS_KEY_SET(obj)                                         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_HASH_MAP_TYPE_KEY_SET))
#define VALA_HASH_MAP_IS_KEY_SET_CLASS(klass)                                 \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_HASH_MAP_TYPE_KEY_SET))
#define VALA_HASH_MAP_KEY_SET_GET_CLASS(obj)                                  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_HASH_MAP_TYPE_KEY_SET,              \
                              ValaHashMapKeySetClass))

typedef struct _ValaHashMapKeySetPrivate
{
  GType k_type;
  GBoxedCopyFunc k_dup_func;
  GDestroyNotify k_destroy_func;
  GType v_type;
  GBoxedCopyFunc v_dup_func;
  GDestroyNotify v_destroy_func;
  ValaHashMap *map;
} ValaHashMapKeySetPrivate;

typedef struct _ValaHashMapKeySet
{
  ValaSet parent_instance;
  ValaHashMapKeySetPrivate *priv;
} ValaHashMapKeySet;

typedef struct _ValaHashMapKeySetClass
{
  ValaSetClass parent_class;
} ValaHashMapKeySetClass;

#define VALA_HASH_MAP_TYPE_VALUE_COLLECTION                                   \
  (vala_hash_map_value_collection_get_type ())
#define VALA_HASH_MAP_VALUE_COLLECTION(obj)                                   \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_HASH_MAP_TYPE_VALUE_COLLECTION,    \
                               ValaHashMapValueCollection))
#define VALA_HASH_MAP_VALUE_COLLECTION_CLASS(klass)                           \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_HASH_MAP_TYPE_VALUE_COLLECTION,     \
                            ValaHashMapValueCollectionClass))
#define VALA_HASH_MAP_IS_VALUE_COLLECTION(obj)                                \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_HASH_MAP_TYPE_VALUE_COLLECTION))
#define VALA_HASH_MAP_IS_VALUE_COLLECTION_CLASS(klass)                        \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_HASH_MAP_TYPE_VALUE_COLLECTION))
#define VALA_HASH_MAP_VALUE_COLLECTION_GET_CLASS(obj)                         \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_HASH_MAP_TYPE_VALUE_COLLECTION,     \
                              ValaHashMapValueCollectionClass))

typedef struct _ValaHashMapValueCollectionPrivate
{
  GType k_type;
  GBoxedCopyFunc k_dup_func;
  GDestroyNotify k_destroy_func;
  GType v_type;
  GBoxedCopyFunc v_dup_func;
  GDestroyNotify v_destroy_func;
  ValaHashMap *map;
} ValaHashMapValueCollectionPrivate;

typedef struct _ValaHashMapValueCollection
{
  ValaCollection parent_instance;
  ValaHashMapValueCollectionPrivate *priv;
} ValaHashMapValueCollection;

typedef struct _ValaHashMapValueCollectionClass
{
  ValaCollectionClass parent_class;
} ValaHashMapValueCollectionClass;

#define VALA_HASH_MAP_TYPE_MAP_ITERATOR                                       \
  (vala_hash_map_map_iterator_get_type ())
#define VALA_HASH_MAP_MAP_ITERATOR(obj)                                       \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_HASH_MAP_TYPE_MAP_ITERATOR,        \
                               ValaHashMapMapIterator))
#define VALA_HASH_MAP_MAP_ITERATOR_CLASS(klass)                               \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_HASH_MAP_TYPE_MAP_ITERATOR,         \
                            ValaHashMapMapIteratorClass))
#define VALA_HASH_MAP_IS_MAP_ITERATOR(obj)                                    \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_HASH_MAP_TYPE_MAP_ITERATOR))
#define VALA_HASH_MAP_IS_MAP_ITERATOR_CLASS(klass)                            \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_HASH_MAP_TYPE_MAP_ITERATOR))
#define VALA_HASH_MAP_MAP_ITERATOR_GET_CLASS(obj)                             \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_HASH_MAP_TYPE_MAP_ITERATOR,         \
                              ValaHashMapMapIteratorClass))

typedef struct _ValaHashMapMapIteratorPrivate
{
  GType k_type;
  GBoxedCopyFunc k_dup_func;
  GDestroyNotify k_destroy_func;
  GType v_type;
  GBoxedCopyFunc v_dup_func;
  GDestroyNotify v_destroy_func;
  ValaHashMap *map;
  gint index;
  ValaHashMapNode *node;
  gint stamp;
} ValaHashMapMapIteratorPrivate;

typedef struct _ValaHashMapMapIterator
{
  ValaMapIterator parent_instance;
  ValaHashMapMapIteratorPrivate *priv;
} ValaHashMapMapIterator;

typedef struct _ValaHashMapMapIteratorClass
{
  ValaMapIteratorClass parent_class;
} ValaHashMapMapIteratorClass;

#define VALA_HASH_MAP_TYPE_KEY_ITERATOR                                       \
  (vala_hash_map_key_iterator_get_type ())
#define VALA_HASH_MAP_KEY_ITERATOR(obj)                                       \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_HASH_MAP_TYPE_KEY_ITERATOR,        \
                               ValaHashMapKeyIterator))
#define VALA_HASH_MAP_KEY_ITERATOR_CLASS(klass)                               \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_HASH_MAP_TYPE_KEY_ITERATOR,         \
                            ValaHashMapKeyIteratorClass))
#define VALA_HASH_MAP_IS_KEY_ITERATOR(obj)                                    \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_HASH_MAP_TYPE_KEY_ITERATOR))
#define VALA_HASH_MAP_IS_KEY_ITERATOR_CLASS(klass)                            \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_HASH_MAP_TYPE_KEY_ITERATOR))
#define VALA_HASH_MAP_KEY_ITERATOR_GET_CLASS(obj)                             \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_HASH_MAP_TYPE_KEY_ITERATOR,         \
                              ValaHashMapKeyIteratorClass))

typedef struct _ValaHashMapKeyIteratorPrivate
{
  GType k_type;
  GBoxedCopyFunc k_dup_func;
  GDestroyNotify k_destroy_func;
  GType v_type;
  GBoxedCopyFunc v_dup_func;
  GDestroyNotify v_destroy_func;
  ValaHashMap *map;
  gint index;
  ValaHashMapNode *node;
  ValaHashMapNode *next;
  gint stamp;
} ValaHashMapKeyIteratorPrivate;

typedef struct _ValaHashMapKeyIterator
{
  ValaIterator parent_instance;
  ValaHashMapKeyIteratorPrivate *priv;
} ValaHashMapKeyIterator;

typedef struct _ValaHashMapKeyIteratorClass
{
  ValaIteratorClass parent_class;
} ValaHashMapKeyIteratorClass;

#define VALA_HASH_MAP_TYPE_VALUE_ITERATOR                                     \
  (vala_hash_map_value_iterator_get_type ())
#define VALA_HASH_MAP_VALUE_ITERATOR(obj)                                     \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_HASH_MAP_TYPE_VALUE_ITERATOR,      \
                               ValaHashMapValueIterator))
#define VALA_HASH_MAP_VALUE_ITERATOR_CLASS(klass)                             \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_HASH_MAP_TYPE_VALUE_ITERATOR,       \
                            ValaHashMapValueIteratorClass))
#define VALA_HASH_MAP_IS_VALUE_ITERATOR(obj)                                  \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_HASH_MAP_TYPE_VALUE_ITERATOR))
#define VALA_HASH_MAP_IS_VALUE_ITERATOR_CLASS(klass)                          \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_HASH_MAP_TYPE_VALUE_ITERATOR))
#define VALA_HASH_MAP_VALUE_ITERATOR_GET_CLASS(obj)                           \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_HASH_MAP_TYPE_VALUE_ITERATOR,       \
                              ValaHashMapValueIteratorClass))

typedef struct _ValaHashMapValueIteratorPrivate
{
  GType k_type;
  GBoxedCopyFunc k_dup_func;
  GDestroyNotify k_destroy_func;
  GType v_type;
  GBoxedCopyFunc v_dup_func;
  GDestroyNotify v_destroy_func;
  ValaHashMap *map;
  gint index;
  ValaHashMapNode *node;
  ValaHashMapNode *next;
  gint stamp;
} ValaHashMapValueIteratorPrivate;

typedef struct _ValaHashMapValueIterator
{
  ValaIterator parent_instance;
  ValaHashMapValueIteratorPrivate *priv;
} ValaHashMapValueIterator;

typedef struct _ValaHashMapValueIteratorClass
{
  ValaIteratorClass parent_class;
} ValaHashMapValueIteratorClass;

static gint vala_hash_map_private_offset;
static gpointer vala_hash_map_parent_class = NULL;
static gint vala_hash_map_key_set_private_offset;
static gpointer vala_hash_map_key_set_parent_class = NULL;
static gint vala_hash_map_map_iterator_private_offset;
static gpointer vala_hash_map_map_iterator_parent_class = NULL;
static gint vala_hash_map_key_iterator_private_offset;
static gpointer vala_hash_map_key_iterator_parent_class = NULL;
static gint vala_hash_map_value_collection_private_offset;
static gpointer vala_hash_map_value_collection_parent_class = NULL;
static gint vala_hash_map_value_iterator_private_offset;
static gpointer vala_hash_map_value_iterator_parent_class = NULL;

static void vala_hash_map_node_free (ValaHashMapNode *self);
#define VALA_HASH_MAP_MIN_SIZE 11
#define VALA_HASH_MAP_MAX_SIZE 13845163
static ValaSet *vala_hash_map_real_get_keys (ValaMap *base);
static ValaHashMapKeySet *
vala_hash_map_key_set_new (GType k_type, GBoxedCopyFunc k_dup_func,
                           GDestroyNotify k_destroy_func, GType v_type,
                           GBoxedCopyFunc v_dup_func,
                           GDestroyNotify v_destroy_func, ValaHashMap *map);
static ValaHashMapKeySet *vala_hash_map_key_set_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map);
static GType vala_hash_map_key_set_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static ValaCollection *vala_hash_map_real_get_values (ValaMap *base);
static ValaHashMapValueCollection *vala_hash_map_value_collection_new (
    GType k_type, GBoxedCopyFunc k_dup_func, GDestroyNotify k_destroy_func,
    GType v_type, GBoxedCopyFunc v_dup_func, GDestroyNotify v_destroy_func,
    ValaHashMap *map);
static ValaHashMapValueCollection *vala_hash_map_value_collection_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map);
static GType
vala_hash_map_value_collection_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static ValaMapIterator *vala_hash_map_real_map_iterator (ValaMap *base);
static ValaHashMapMapIterator *vala_hash_map_map_iterator_new (
    GType k_type, GBoxedCopyFunc k_dup_func, GDestroyNotify k_destroy_func,
    GType v_type, GBoxedCopyFunc v_dup_func, GDestroyNotify v_destroy_func,
    ValaHashMap *map);
static ValaHashMapMapIterator *vala_hash_map_map_iterator_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map);
static GType
vala_hash_map_map_iterator_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static ValaHashMapNode **vala_hash_map_lookup_node (ValaHashMap *self,
                                                    gconstpointer key);
static gboolean vala_hash_map_real_contains (ValaMap *base, gconstpointer key);
static gpointer vala_hash_map_real_get (ValaMap *base, gconstpointer key);
static void vala_hash_map_real_set (ValaMap *base, gconstpointer key,
                                    gconstpointer value);
static ValaHashMapNode *vala_hash_map_node_new (gpointer k, gpointer v,
                                                guint hash);
static void vala_hash_map_resize (ValaHashMap *self);
static gboolean vala_hash_map_real_remove (ValaMap *base, gconstpointer key);
static void vala_hash_map_real_clear (ValaMap *base);
static void vala_hash_map_node_instance_init (ValaHashMapNode *self);
static void vala_hash_map_key_set_set_map (ValaHashMapKeySet *self,
                                           ValaHashMap *value);
static GType vala_hash_map_key_set_real_get_element_type (ValaIterable *base);
static ValaIterator *vala_hash_map_key_set_real_iterator (ValaIterable *base);
static ValaHashMapKeyIterator *vala_hash_map_key_iterator_new (
    GType k_type, GBoxedCopyFunc k_dup_func, GDestroyNotify k_destroy_func,
    GType v_type, GBoxedCopyFunc v_dup_func, GDestroyNotify v_destroy_func,
    ValaHashMap *map);
static ValaHashMapKeyIterator *vala_hash_map_key_iterator_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map);
static GType
vala_hash_map_key_iterator_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static gboolean vala_hash_map_key_set_real_add (ValaCollection *base,
                                                gconstpointer key);
static void vala_hash_map_key_set_real_clear (ValaCollection *base);
static gboolean vala_hash_map_key_set_real_remove (ValaCollection *base,
                                                   gconstpointer key);
static gboolean vala_hash_map_key_set_real_contains (ValaCollection *base,
                                                     gconstpointer key);
static void vala_hash_map_key_set_finalize (ValaIterable *obj);
static GType vala_hash_map_key_set_get_type_once (void);
static void vala_hash_map_map_iterator_set_map (ValaHashMapMapIterator *self,
                                                ValaHashMap *value);
static gboolean vala_hash_map_map_iterator_real_next (ValaMapIterator *base);
static gpointer
vala_hash_map_map_iterator_real_get_key (ValaMapIterator *base);
static gpointer
vala_hash_map_map_iterator_real_get_value (ValaMapIterator *base);
static void vala_hash_map_map_iterator_finalize (ValaMapIterator *obj);
static GType vala_hash_map_map_iterator_get_type_once (void);
static void vala_hash_map_key_iterator_set_map (ValaHashMapKeyIterator *self,
                                                ValaHashMap *value);
static gboolean vala_hash_map_key_iterator_real_next (ValaIterator *base);
static gboolean vala_hash_map_key_iterator_real_has_next (ValaIterator *base);
static gpointer vala_hash_map_key_iterator_real_get (ValaIterator *base);
static void vala_hash_map_key_iterator_real_remove (ValaIterator *base);
static void vala_hash_map_key_iterator_finalize (ValaIterator *obj);
static GType vala_hash_map_key_iterator_get_type_once (void);
static void
vala_hash_map_value_collection_set_map (ValaHashMapValueCollection *self,
                                        ValaHashMap *value);
static GType
vala_hash_map_value_collection_real_get_element_type (ValaIterable *base);
static ValaIterator *
vala_hash_map_value_collection_real_iterator (ValaIterable *base);
static ValaHashMapValueIterator *vala_hash_map_value_iterator_new (
    GType k_type, GBoxedCopyFunc k_dup_func, GDestroyNotify k_destroy_func,
    GType v_type, GBoxedCopyFunc v_dup_func, GDestroyNotify v_destroy_func,
    ValaHashMap *map);
static ValaHashMapValueIterator *vala_hash_map_value_iterator_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map);
static GType
vala_hash_map_value_iterator_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static gboolean vala_hash_map_value_collection_real_add (ValaCollection *base,
                                                         gconstpointer value);
static void vala_hash_map_value_collection_real_clear (ValaCollection *base);
static gboolean
vala_hash_map_value_collection_real_remove (ValaCollection *base,
                                            gconstpointer value);
static gboolean
vala_hash_map_value_collection_real_contains (ValaCollection *base,
                                              gconstpointer value);
static void vala_hash_map_value_collection_finalize (ValaIterable *obj);
static GType vala_hash_map_value_collection_get_type_once (void);
static void
vala_hash_map_value_iterator_set_map (ValaHashMapValueIterator *self,
                                      ValaHashMap *value);
static gboolean vala_hash_map_value_iterator_real_next (ValaIterator *base);
static gboolean
vala_hash_map_value_iterator_real_has_next (ValaIterator *base);
static gpointer vala_hash_map_value_iterator_real_get (ValaIterator *base);
static void vala_hash_map_value_iterator_real_remove (ValaIterator *base);
static void vala_hash_map_value_iterator_finalize (ValaIterator *obj);
static GType vala_hash_map_value_iterator_get_type_once (void);
static void vala_hash_map_finalize (ValaMap *obj);
static GType vala_hash_map_get_type_once (void);
G_GNUC_INTERNAL void vala_array_free (gpointer, gint, GDestroyNotify);

static inline gpointer
vala_hash_map_get_instance_private (ValaHashMap *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_map_private_offset);
}

static gint
vala_hash_map_real_get_size (ValaMap *base)
{
  return ((ValaHashMap *)base)->priv->n_nodes;
}

void
vala_hash_map_set_key_hash_func (ValaHashMap *self, GHashFunc value)
{
  g_return_if_fail (self != NULL);
  self->priv->key_hash_func = value;
}

void
vala_hash_map_set_key_equal_func (ValaHashMap *self, GEqualFunc value)
{
  g_return_if_fail (self != NULL);
  self->priv->key_equal_func = value;
}

void
vala_hash_map_set_value_equal_func (ValaHashMap *self, GEqualFunc value)
{
  g_return_if_fail (self != NULL);
  self->priv->value_equal_func = value;
}

ValaHashMap *
vala_hash_map_construct (GType object_type, GType k_type,
                         GBoxedCopyFunc k_dup_func,
                         GDestroyNotify k_destroy_func, GType v_type,
                         GBoxedCopyFunc v_dup_func,
                         GDestroyNotify v_destroy_func,
                         GHashFunc key_hash_func, GEqualFunc key_equal_func,
                         GEqualFunc value_equal_func)
{
  ValaHashMap *self = (ValaHashMap *)vala_map_construct (object_type,
                                                         k_type,
                                                         k_dup_func,
                                                         k_destroy_func,
                                                         v_type,
                                                         v_dup_func,
                                                         v_destroy_func);
  self->priv->k_type = k_type;
  self->priv->k_dup_func = k_dup_func;
  self->priv->k_destroy_func = k_destroy_func;
  self->priv->v_type = v_type;
  self->priv->v_dup_func = v_dup_func;
  self->priv->v_destroy_func = v_destroy_func;
  vala_hash_map_set_key_hash_func (self, key_hash_func);
  vala_hash_map_set_key_equal_func (self, key_equal_func);
  vala_hash_map_set_value_equal_func (self, value_equal_func);
  self->priv->n_buckets = VALA_HASH_MAP_MIN_SIZE;
  self->priv->nodes = g_new0 (ValaHashMapNode *, self->priv->n_buckets + 1);
  return self;
}

ValaHashMap *
vala_hash_map_new (GType k_type, GBoxedCopyFunc k_dup_func,
                   GDestroyNotify k_destroy_func, GType v_type,
                   GBoxedCopyFunc v_dup_func, GDestroyNotify v_destroy_func,
                   GHashFunc key_hash_func, GEqualFunc key_equal_func,
                   GEqualFunc value_equal_func)
{
  return vala_hash_map_construct (VALA_TYPE_HASH_MAP, k_type, k_dup_func,
                                  k_destroy_func, v_type, v_dup_func,
                                  v_destroy_func, key_hash_func,
                                  key_equal_func, value_equal_func);
}

static ValaSet *
vala_hash_map_real_get_keys (ValaMap *base)
{
  ValaHashMap *self = (ValaHashMap *)base;
  return (ValaSet *)vala_hash_map_key_set_new (self->priv->k_type,
                                               self->priv->k_dup_func,
                                               self->priv->k_destroy_func,
                                               self->priv->v_type,
                                               self->priv->v_dup_func,
                                               self->priv->v_destroy_func,
                                               self);
}

static ValaCollection *
vala_hash_map_real_get_values (ValaMap *base)
{
  ValaHashMap *self = (ValaHashMap *)base;
  return (ValaCollection *)
    vala_hash_map_value_collection_new (self->priv->k_type,
                                        self->priv->k_dup_func,
                                        self->priv->k_destroy_func,
                                        self->priv->v_type,
                                        self->priv->v_dup_func,
                                        self->priv->v_destroy_func,
                                        self);
}

static ValaMapIterator *
vala_hash_map_real_map_iterator (ValaMap *base)
{
  ValaHashMap *self = (ValaHashMap *)base;
  return (ValaMapIterator *)
    vala_hash_map_map_iterator_new (self->priv->k_type,
                                    self->priv->k_dup_func,
                                    self->priv->k_destroy_func,
                                    self->priv->v_type,
                                    self->priv->v_dup_func,
                                    self->priv->v_destroy_func,
                                    self);
}

static ValaHashMapNode **
vala_hash_map_lookup_node (ValaHashMap *self, gconstpointer key)
{
  guint hash_value = (guint) self->priv->key_hash_func (key);
  ValaHashMapNode **node =
    self->priv->nodes + (hash_value % self->priv->n_buckets);

  while (*node)
    {
      if (hash_value == (*node)->key_hash &&
          self->priv->key_equal_func ((*node)->key, key))
        break;
      node = &(*node)->next;
    }

  return node;
}

static gboolean
vala_hash_map_real_contains (ValaMap *base, gconstpointer key)
{
  ValaHashMap *self = (ValaHashMap *)base;
  ValaHashMapNode *node = *vala_hash_map_lookup_node (self, key);
  return node != NULL;
}

static gpointer
vala_hash_map_real_get (ValaMap *base, gconstpointer key)
{
  ValaHashMap *self = (ValaHashMap *)base;
  ValaHashMapNode *node = *vala_hash_map_lookup_node (self, key);

  if (node)
    {
      gpointer val = node->value;
      if (val && self->priv->v_dup_func)
        return self->priv->v_dup_func (val);
      else
        return val;
    }
  return NULL;
}

static void
vala_hash_map_real_set (ValaMap *base, gconstpointer key, gconstpointer value)
{
  ValaHashMap *self = (ValaHashMap *)base;
  ValaHashMapNode **node = vala_hash_map_lookup_node (self, key);
  gpointer val = (gpointer) value;

  if (*node)
    {
      if ((*node)->value && self->priv->v_destroy_func)
        self->priv->v_destroy_func ((gpointer) (*node)->value);

      if (val && self->priv->v_dup_func)
        (*node)->value = self->priv->v_dup_func (val);
      else
        (*node)->value = val;
    }
  else
    {
      guint hash_value = self->priv->key_hash_func (key);
      gpointer k_copy, v_copy;

      if (key && self->priv->k_dup_func)
        k_copy = self->priv->k_dup_func ((gpointer) key);
      else
        k_copy = (gpointer) key;

      if (value && self->priv->v_dup_func)
        v_copy = self->priv->v_dup_func ((gpointer) value);
      else
        v_copy = (gpointer) value;

      *node = vala_hash_map_node_new (k_copy, v_copy, hash_value);
      self->priv->n_nodes += 1;
      vala_hash_map_resize (self);
    }

  self->priv->stamp += 1;
}

static gboolean
vala_hash_map_real_remove (ValaMap *base, gconstpointer key)
{
  ValaHashMap *self = (ValaHashMap *)base;
  ValaHashMapNode **node = vala_hash_map_lookup_node (self, key);
  if (*node)
    {
      ValaHashMapNode *next = (*node)->next;
      (*node)->next = NULL;

      if ((*node)->key && self->priv->k_destroy_func)
        self->priv->k_destroy_func ((*node)->key);

      if ((*node)->value && self->priv->v_destroy_func)
        self->priv->v_destroy_func ((*node)->value);

      vala_hash_map_node_free (*node);
      *node = next;
      self->priv->n_nodes -= 1;
      vala_hash_map_resize (self);
      self->priv->stamp += 1;
      return TRUE;
    }

  return FALSE;
}

static void
vala_hash_map_real_clear (ValaMap *base)
{
  ValaHashMap *self = (ValaHashMap *)base;
  for (gint i = 0; i < self->priv->n_buckets; ++i)
    {
      ValaHashMapNode *node = self->priv->nodes[i];
      self->priv->nodes[i] = NULL;
      while (node)
        {
          ValaHashMapNode *next = node->next;
          node->next = NULL;

          if (node->key && self->priv->k_destroy_func)
            self->priv->k_destroy_func (node->key);

          if (node->value && self->priv->v_destroy_func)
            self->priv->v_destroy_func (node->value);

          vala_hash_map_node_free (node);
          node = next;
        }
    }
  self->priv->n_nodes = 0;
  vala_hash_map_resize (self);
}

static void
vala_hash_map_resize (ValaHashMap *self)
{
  if ((self->priv->n_buckets >= (3 * self->priv->n_nodes) &&
       self->priv->n_buckets >= VALA_HASH_MAP_MIN_SIZE) ||
      ((3 * self->priv->n_buckets) <= self->priv->n_nodes &&
       self->priv->n_buckets < VALA_HASH_MAP_MAX_SIZE))
    {
      gint new_array_size =
        (gint)g_spaced_primes_closest ((guint)self->priv->n_nodes);
      new_array_size = CLAMP(new_array_size,
                             VALA_HASH_MAP_MIN_SIZE,
                             VALA_HASH_MAP_MAX_SIZE);
      ValaHashMapNode **new_nodes = g_new0 (ValaHashMapNode *, new_array_size + 1);

      for (gint i = 0; i < self->priv->n_buckets; ++i)
        {
          ValaHashMapNode *node = self->priv->nodes[i];
          while (node)
            {
              ValaHashMapNode *next = node->next;
              gint hash_val = node->key_hash % new_array_size;
              node->next = new_nodes[hash_val];
              new_nodes[hash_val] = node;
              node = next;
            }
          self->priv->nodes[i] = NULL;
        }
      vala_array_free (self->priv->nodes, self->priv->n_buckets,
                       (GDestroyNotify)vala_hash_map_node_free);
      self->priv->nodes = new_nodes;
      self->priv->n_buckets = new_array_size;
    }
}

static ValaHashMapNode *
vala_hash_map_node_new (gpointer k, gpointer v, guint hash)
{
  ValaHashMapNode *self = g_slice_new0 (ValaHashMapNode);
  vala_hash_map_node_instance_init (self);
  self->key = k;
  self->value = v;
  self->key_hash = hash;
  return self;
}

static void
vala_hash_map_node_instance_init (ValaHashMapNode *self)
{
}

static void
vala_hash_map_node_free (ValaHashMapNode *self)
{
  if (self->next)
    vala_hash_map_node_free (self->next);
  g_slice_free (ValaHashMapNode, self);
}

static inline gpointer
vala_hash_map_key_set_get_instance_private (ValaHashMapKeySet *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_map_key_set_private_offset);
}

static void
vala_hash_map_key_set_set_map (ValaHashMapKeySet *self, ValaHashMap *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  self->priv->map = vala_map_ref (value);
}

static ValaHashMapKeySet *
vala_hash_map_key_set_construct (GType object_type, GType k_type,
                                 GBoxedCopyFunc k_dup_func,
                                 GDestroyNotify k_destroy_func, GType v_type,
                                 GBoxedCopyFunc v_dup_func,
                                 GDestroyNotify v_destroy_func,
                                 ValaHashMap *map)
{
  g_return_val_if_fail (map != NULL, NULL);
  ValaHashMapKeySet *self =
    (ValaHashMapKeySet *)vala_set_construct (object_type,
                                             k_type,
                                             k_dup_func,
                                             k_destroy_func);
  self->priv->k_type = k_type;
  self->priv->k_dup_func = k_dup_func;
  self->priv->k_destroy_func = k_destroy_func;
  self->priv->v_type = v_type;
  self->priv->v_dup_func = v_dup_func;
  self->priv->v_destroy_func = v_destroy_func;
  vala_hash_map_key_set_set_map (self, map);
  return self;
}

static ValaHashMapKeySet *
vala_hash_map_key_set_new (GType k_type, GBoxedCopyFunc k_dup_func,
                           GDestroyNotify k_destroy_func, GType v_type,
                           GBoxedCopyFunc v_dup_func,
                           GDestroyNotify v_destroy_func, ValaHashMap *map)
{
  return vala_hash_map_key_set_construct (VALA_HASH_MAP_TYPE_KEY_SET, k_type,
                                          k_dup_func, k_destroy_func, v_type,
                                          v_dup_func, v_destroy_func, map);
}

static GType
vala_hash_map_key_set_real_get_element_type (ValaIterable *base)
{
  return ((ValaHashMapKeySet *)base)->priv->k_type;
}

static ValaIterator *
vala_hash_map_key_set_real_iterator (ValaIterable *base)
{
  ValaHashMapKeySet *self = (ValaHashMapKeySet *)base;
  return (ValaIterator *)
    vala_hash_map_key_iterator_new (self->priv->k_type,
                                    self->priv->k_dup_func,
                                    self->priv->k_destroy_func,
                                    self->priv->v_type,
                                    self->priv->v_dup_func,
                                    self->priv->v_destroy_func,
                                    self->priv->map);
}

static gint
vala_hash_map_key_set_real_get_size (ValaCollection *base)
{
  ValaHashMapKeySet *self = (ValaHashMapKeySet *)base;
  return vala_map_get_size ((ValaMap *)self->priv->map);
}

static gboolean
vala_hash_map_key_set_real_add (ValaCollection *base, gconstpointer key)
{
  g_assert_not_reached ();
}

static void
vala_hash_map_key_set_real_clear (ValaCollection *base)
{
  g_assert_not_reached ();
}

static gboolean
vala_hash_map_key_set_real_remove (ValaCollection *base, gconstpointer key)
{
  g_assert_not_reached ();
}

static gboolean
vala_hash_map_key_set_real_contains (ValaCollection *base, gconstpointer key)
{
  ValaHashMapKeySet *self = (ValaHashMapKeySet *)base;
  return vala_map_contains ((ValaMap *)self->priv->map, key);
}

static void
vala_hash_map_key_set_class_init (ValaHashMapKeySetClass *klass,
                                  gpointer klass_data)
{
  vala_hash_map_key_set_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERABLE_CLASS (klass)->finalize = vala_hash_map_key_set_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_hash_map_key_set_private_offset);
  VALA_ITERABLE_CLASS (klass)->get_element_type = vala_hash_map_key_set_real_get_element_type;
  VALA_ITERABLE_CLASS (klass)->iterator = vala_hash_map_key_set_real_iterator;
  VALA_COLLECTION_CLASS (klass)->add = vala_hash_map_key_set_real_add;
  VALA_COLLECTION_CLASS (klass)->clear = vala_hash_map_key_set_real_clear;
  VALA_COLLECTION_CLASS (klass)->remove = vala_hash_map_key_set_real_remove;
  VALA_COLLECTION_CLASS (klass)->contains = vala_hash_map_key_set_real_contains;
  VALA_COLLECTION_CLASS (klass)->get_size = vala_hash_map_key_set_real_get_size;
}

static void
vala_hash_map_key_set_instance_init (ValaHashMapKeySet *self, gpointer klass)
{
  self->priv = vala_hash_map_key_set_get_instance_private (self);
}

static void
vala_hash_map_key_set_finalize (ValaIterable *obj)
{
  ValaHashMapKeySet *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_HASH_MAP_TYPE_KEY_SET,
                                     ValaHashMapKeySet);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  VALA_ITERABLE_CLASS (vala_hash_map_key_set_parent_class)->finalize (obj);
}

static GType
vala_hash_map_key_set_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashMapKeySetClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_map_key_set_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashMapKeySet),
      0,
      (GInstanceInitFunc)vala_hash_map_key_set_instance_init,
      NULL
    };
  GType vala_hash_map_key_set_type_id =
    g_type_register_static (VALA_TYPE_SET, "ValaHashMapKeySet",
                            &g_define_type_info, 0);
  vala_hash_map_key_set_private_offset =
    g_type_add_instance_private (vala_hash_map_key_set_type_id,
                                 sizeof (ValaHashMapKeySetPrivate));
  return vala_hash_map_key_set_type_id;
}

static GType
vala_hash_map_key_set_get_type (void)
{
  static volatile gsize vala_hash_map_key_set_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_map_key_set_type_id__volatile))
    {
      GType vala_hash_map_key_set_type_id;
      vala_hash_map_key_set_type_id = vala_hash_map_key_set_get_type_once ();
      g_once_init_leave (&vala_hash_map_key_set_type_id__volatile,
                         vala_hash_map_key_set_type_id);
    }
  return vala_hash_map_key_set_type_id__volatile;
}

static inline gpointer
vala_hash_map_map_iterator_get_instance_private (ValaHashMapMapIterator *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_map_map_iterator_private_offset);
}

static void
vala_hash_map_map_iterator_set_map (ValaHashMapMapIterator *self,
                                    ValaHashMap *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  self->priv->map = vala_map_ref (value);
  self->priv->stamp = value->priv->stamp;
}

static ValaHashMapMapIterator *
vala_hash_map_map_iterator_construct (GType object_type, GType k_type,
                                      GBoxedCopyFunc k_dup_func,
                                      GDestroyNotify k_destroy_func,
                                      GType v_type, GBoxedCopyFunc v_dup_func,
                                      GDestroyNotify v_destroy_func,
                                      ValaHashMap *map)
{
  g_return_val_if_fail (map != NULL, NULL);
  ValaHashMapMapIterator *self =
    (ValaHashMapMapIterator *)vala_map_iterator_construct (object_type,
                                                           k_type,
                                                           k_dup_func,
                                                           k_destroy_func,
                                                           v_type,
                                                           v_dup_func,
                                                           v_destroy_func);
  self->priv->k_type = k_type;
  self->priv->k_dup_func = k_dup_func;
  self->priv->k_destroy_func = k_destroy_func;
  self->priv->v_type = v_type;
  self->priv->v_dup_func = v_dup_func;
  self->priv->v_destroy_func = v_destroy_func;
  vala_hash_map_map_iterator_set_map (self, map);
  return self;
}

static ValaHashMapMapIterator *
vala_hash_map_map_iterator_new (GType k_type, GBoxedCopyFunc k_dup_func,
                                GDestroyNotify k_destroy_func, GType v_type,
                                GBoxedCopyFunc v_dup_func,
                                GDestroyNotify v_destroy_func,
                                ValaHashMap *map)
{
  return vala_hash_map_map_iterator_construct (VALA_HASH_MAP_TYPE_MAP_ITERATOR,
                                               k_type,
                                               k_dup_func,
                                               k_destroy_func,
                                               v_type,
                                               v_dup_func,
                                               v_destroy_func,
                                               map);
}

static gboolean
vala_hash_map_map_iterator_real_next (ValaMapIterator *base)
{
  ValaHashMapMapIterator *self  = (ValaHashMapMapIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);
  if (self->priv->node)
    self->priv->node = self->priv->node->next;

  while (++self->priv->index < map->priv->n_buckets &&
         !self->priv->node)
    self->priv->node = map->priv->nodes[self->priv->index];

  return self->priv->node != NULL;
}

static gpointer
vala_hash_map_map_iterator_real_get_key (ValaMapIterator *base)
{
  ValaHashMapMapIterator *self = (ValaHashMapMapIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);
  g_assert (self->priv->node != NULL);
  gpointer key = self->priv->node->key;
  if (key && self->priv->k_dup_func)
    return self->priv->k_dup_func (key);
  else
    return key;
}

static gpointer
vala_hash_map_map_iterator_real_get_value (ValaMapIterator *base)
{
  ValaHashMapMapIterator *self = (ValaHashMapMapIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);
  g_assert (self->priv->node != NULL);
  gpointer value = self->priv->node->value;
  if (value && self->priv->k_dup_func)
    return self->priv->k_dup_func (value);
  else
    return value;
}

static void
vala_hash_map_map_iterator_class_init (ValaHashMapMapIteratorClass *klass,
                                       gpointer klass_data)
{
  vala_hash_map_map_iterator_parent_class = g_type_class_peek_parent (klass);
  VALA_MAP_ITERATOR_CLASS (klass)->finalize = vala_hash_map_map_iterator_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_hash_map_map_iterator_private_offset);
  VALA_MAP_ITERATOR_CLASS (klass)->next = vala_hash_map_map_iterator_real_next;
  VALA_MAP_ITERATOR_CLASS (klass)->get_key = vala_hash_map_map_iterator_real_get_key;
  VALA_MAP_ITERATOR_CLASS (klass)->get_value = vala_hash_map_map_iterator_real_get_value;
}

static void
vala_hash_map_map_iterator_instance_init (ValaHashMapMapIterator *self,
                                          gpointer klass)
{
  self->priv = vala_hash_map_map_iterator_get_instance_private (self);
  self->priv->index = -1;
}

static void
vala_hash_map_map_iterator_finalize (ValaMapIterator *obj)
{
  ValaHashMapMapIterator *self =
    G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_HASH_MAP_TYPE_MAP_ITERATOR,
                                ValaHashMapMapIterator);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  VALA_MAP_ITERATOR_CLASS (vala_hash_map_map_iterator_parent_class)->finalize (obj);
}

static GType
vala_hash_map_map_iterator_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashMapMapIteratorClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_map_map_iterator_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashMapMapIterator),
      0,
      (GInstanceInitFunc)vala_hash_map_map_iterator_instance_init,
      NULL
    };
  GType vala_hash_map_map_iterator_type_id =
    g_type_register_static (VALA_TYPE_MAP_ITERATOR, "ValaHashMapMapIterator",
                            &g_define_type_info, 0);
  vala_hash_map_map_iterator_private_offset =
    g_type_add_instance_private (vala_hash_map_map_iterator_type_id,
                                 sizeof (ValaHashMapMapIteratorPrivate));
  return vala_hash_map_map_iterator_type_id;
}

static GType
vala_hash_map_map_iterator_get_type (void)
{
  static volatile gsize vala_hash_map_map_iterator_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_map_map_iterator_type_id__volatile))
    {
      GType vala_hash_map_map_iterator_type_id;
      vala_hash_map_map_iterator_type_id
          = vala_hash_map_map_iterator_get_type_once ();
      g_once_init_leave (&vala_hash_map_map_iterator_type_id__volatile,
                         vala_hash_map_map_iterator_type_id);
    }
  return vala_hash_map_map_iterator_type_id__volatile;
}

static inline gpointer
vala_hash_map_key_iterator_get_instance_private (ValaHashMapKeyIterator *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_map_key_iterator_private_offset);
}

static void
vala_hash_map_key_iterator_set_map (ValaHashMapKeyIterator *self,
                                    ValaHashMap *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  self->priv->map = vala_map_ref (value);
  self->priv->stamp = value->priv->stamp;
}

static ValaHashMapKeyIterator *
vala_hash_map_key_iterator_construct (GType object_type, GType k_type,
                                      GBoxedCopyFunc k_dup_func,
                                      GDestroyNotify k_destroy_func,
                                      GType v_type, GBoxedCopyFunc v_dup_func,
                                      GDestroyNotify v_destroy_func,
                                      ValaHashMap *map)
{
  g_return_val_if_fail (map != NULL, NULL);
  ValaHashMapKeyIterator *self =
    (ValaHashMapKeyIterator *)vala_iterator_construct (object_type,
                                                       k_type,
                                                       k_dup_func,
                                                       k_destroy_func);
  self->priv->k_type = k_type;
  self->priv->k_dup_func = k_dup_func;
  self->priv->k_destroy_func = k_destroy_func;
  self->priv->v_type = v_type;
  self->priv->v_dup_func = v_dup_func;
  self->priv->v_destroy_func = v_destroy_func;
  vala_hash_map_key_iterator_set_map (self, map);
  return self;
}

static ValaHashMapKeyIterator *
vala_hash_map_key_iterator_new (GType k_type, GBoxedCopyFunc k_dup_func,
                                GDestroyNotify k_destroy_func, GType v_type,
                                GBoxedCopyFunc v_dup_func,
                                GDestroyNotify v_destroy_func,
                                ValaHashMap *map)
{
  return vala_hash_map_key_iterator_construct (VALA_HASH_MAP_TYPE_KEY_ITERATOR,
                                               k_type,
                                               k_dup_func,
                                               k_destroy_func,
                                               v_type,
                                               v_dup_func,
                                               v_destroy_func,
                                               map);
}

static gboolean
vala_hash_map_key_iterator_real_next (ValaIterator *base)
{
  ValaHashMapKeyIterator *self = (ValaHashMapKeyIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);
  if (!vala_iterator_has_next ((ValaIterator *)self))
    return FALSE;
  self->priv->node = self->priv->next;
  self->priv->next = NULL;
  return self->priv->node != NULL;
}

static gboolean
vala_hash_map_key_iterator_real_has_next (ValaIterator *base)
{
  ValaHashMapKeyIterator *self = (ValaHashMapKeyIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);

  if (!self->priv->next)
    {
      if (self->priv->node)
        self->priv->next = self->priv->node->next;

      while (!self->priv->next &&
             ++self->priv->index < map->priv->n_buckets)
        self->priv->next = map->priv->nodes[self->priv->index];
    }

  return self->priv->next != NULL;
}

static gpointer
vala_hash_map_key_iterator_real_get (ValaIterator *base)
{
  ValaHashMapKeyIterator *self = (ValaHashMapKeyIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);
  g_assert (self->priv->node != NULL);

  gpointer key = self->priv->node->key;
  if (key && self->priv->k_dup_func)
    return self->priv->k_dup_func (key);
  else
    return key;
}

static void
vala_hash_map_key_iterator_real_remove (ValaIterator *base)
{
  g_assert_not_reached ();
}

static gboolean
vala_hash_map_key_iterator_real_get_valid (ValaIterator *base)
{
  ValaHashMapKeyIterator *self = (ValaHashMapKeyIterator *)base;
  return self->priv->node != NULL;
}

static void
vala_hash_map_key_iterator_class_init (ValaHashMapKeyIteratorClass *klass,
                                       gpointer klass_data)
{
  vala_hash_map_key_iterator_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERATOR_CLASS (klass)->finalize = vala_hash_map_key_iterator_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_hash_map_key_iterator_private_offset);
  VALA_ITERATOR_CLASS (klass)->next = vala_hash_map_key_iterator_real_next;
  VALA_ITERATOR_CLASS (klass)->has_next = vala_hash_map_key_iterator_real_has_next;
  VALA_ITERATOR_CLASS (klass)->get = vala_hash_map_key_iterator_real_get;
  VALA_ITERATOR_CLASS (klass)->remove = vala_hash_map_key_iterator_real_remove;
  VALA_ITERATOR_CLASS (klass)->get_valid = vala_hash_map_key_iterator_real_get_valid;
}

static void
vala_hash_map_key_iterator_instance_init (ValaHashMapKeyIterator *self,
                                          gpointer klass)
{
  self->priv = vala_hash_map_key_iterator_get_instance_private (self);
  self->priv->index = -1;
}

static void
vala_hash_map_key_iterator_finalize (ValaIterator *obj)
{
  ValaHashMapKeyIterator *self =
    G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_HASH_MAP_TYPE_KEY_ITERATOR,
                                ValaHashMapKeyIterator);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  VALA_ITERATOR_CLASS (vala_hash_map_key_iterator_parent_class)->finalize (obj);
}

static GType
vala_hash_map_key_iterator_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashMapKeyIteratorClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_map_key_iterator_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashMapKeyIterator),
      0,
      (GInstanceInitFunc)vala_hash_map_key_iterator_instance_init,
      NULL
    };
  GType vala_hash_map_key_iterator_type_id =
    g_type_register_static (VALA_TYPE_ITERATOR, "ValaHashMapKeyIterator",
                            &g_define_type_info, 0);
  vala_hash_map_key_iterator_private_offset =
    g_type_add_instance_private (vala_hash_map_key_iterator_type_id,
                                 sizeof (ValaHashMapKeyIteratorPrivate));
  return vala_hash_map_key_iterator_type_id;
}

static GType
vala_hash_map_key_iterator_get_type (void)
{
  static volatile gsize vala_hash_map_key_iterator_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_map_key_iterator_type_id__volatile))
    {
      GType vala_hash_map_key_iterator_type_id;
      vala_hash_map_key_iterator_type_id
          = vala_hash_map_key_iterator_get_type_once ();
      g_once_init_leave (&vala_hash_map_key_iterator_type_id__volatile,
                         vala_hash_map_key_iterator_type_id);
    }
  return vala_hash_map_key_iterator_type_id__volatile;
}

static inline gpointer
vala_hash_map_value_collection_get_instance_private (
    ValaHashMapValueCollection *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_map_value_collection_private_offset);
}

static void
vala_hash_map_value_collection_set_map (ValaHashMapValueCollection *self,
                                        ValaHashMap *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  self->priv->map = vala_map_ref (value);
}

static ValaHashMapValueCollection *
vala_hash_map_value_collection_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map)
{
  g_return_val_if_fail (map != NULL, NULL);
  ValaHashMapValueCollection *self =
    (ValaHashMapValueCollection *)vala_collection_construct (object_type,
                                                             v_type,
                                                             v_dup_func,
                                                             v_destroy_func);
  self->priv->k_type = k_type;
  self->priv->k_dup_func = k_dup_func;
  self->priv->k_destroy_func = k_destroy_func;
  self->priv->v_type = v_type;
  self->priv->v_dup_func = v_dup_func;
  self->priv->v_destroy_func = v_destroy_func;
  vala_hash_map_value_collection_set_map (self, map);
  return self;
}

static ValaHashMapValueCollection *
vala_hash_map_value_collection_new (GType k_type, GBoxedCopyFunc k_dup_func,
                                    GDestroyNotify k_destroy_func,
                                    GType v_type, GBoxedCopyFunc v_dup_func,
                                    GDestroyNotify v_destroy_func,
                                    ValaHashMap *map)
{
  return vala_hash_map_value_collection_construct (VALA_HASH_MAP_TYPE_VALUE_COLLECTION,
                                                   k_type,
                                                   k_dup_func,
                                                   k_destroy_func,
                                                   v_type,
                                                   v_dup_func,
                                                   v_destroy_func,
                                                   map);
}

static GType
vala_hash_map_value_collection_real_get_element_type (ValaIterable *base)
{
  return ((ValaHashMapValueCollection *)base)->priv->v_type;
}

static ValaIterator *
vala_hash_map_value_collection_real_iterator (ValaIterable *base)
{
  ValaHashMapValueCollection *self = (ValaHashMapValueCollection *)base;
  return
    (ValaIterator *)
    vala_hash_map_value_iterator_new (self->priv->k_type,
                                      self->priv->k_dup_func,
                                      self->priv->k_destroy_func,
                                      self->priv->v_type,
                                      self->priv->v_dup_func,
                                      self->priv->v_destroy_func,
                                      self->priv->map);
}

static gint
vala_hash_map_value_collection_real_get_size (ValaCollection *base)
{
  ValaHashMapValueCollection *self = (ValaHashMapValueCollection *)base;
  return vala_map_get_size ((ValaMap *)self->priv->map);
}

static gboolean
vala_hash_map_value_collection_real_add (ValaCollection *base,
                                         gconstpointer value)
{
  g_assert_not_reached ();
}

static void
vala_hash_map_value_collection_real_clear (ValaCollection *base)
{
  g_assert_not_reached ();
}

static gboolean
vala_hash_map_value_collection_real_remove (ValaCollection *base,
                                            gconstpointer value)
{
  g_assert_not_reached ();
}

static gboolean
vala_hash_map_value_collection_real_contains (ValaCollection *base,
                                              gconstpointer value)
{
  ValaHashMapValueCollection *self = (ValaHashMapValueCollection *)base;
  ValaHashMap *map = self->priv->map;
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    {
      gpointer v = vala_iterator_get (it);
      gboolean found = map->priv->value_equal_func (v, value);
      if (v && self->priv->v_destroy_func)
        self->priv->v_destroy_func (v);

      if (found)
        {
          vala_iterator_unref (it);
          return TRUE;
        }
    }

  if (it)
    vala_iterator_unref (it);
  return FALSE;
}

static void
vala_hash_map_value_collection_class_init (
    ValaHashMapValueCollectionClass *klass, gpointer klass_data)
{
  vala_hash_map_value_collection_parent_class
      = g_type_class_peek_parent (klass);
  VALA_ITERABLE_CLASS (klass)->finalize = vala_hash_map_value_collection_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_hash_map_value_collection_private_offset);
  VALA_ITERABLE_CLASS (klass)->get_element_type = vala_hash_map_value_collection_real_get_element_type;
  VALA_ITERABLE_CLASS (klass)->iterator = vala_hash_map_value_collection_real_iterator;
  VALA_COLLECTION_CLASS (klass)->add = vala_hash_map_value_collection_real_add;
  VALA_COLLECTION_CLASS (klass)->clear = vala_hash_map_value_collection_real_clear;
  VALA_COLLECTION_CLASS (klass)->remove = vala_hash_map_value_collection_real_remove;
  VALA_COLLECTION_CLASS (klass)->contains = vala_hash_map_value_collection_real_contains;
  VALA_COLLECTION_CLASS (klass)->get_size = vala_hash_map_value_collection_real_get_size;
}

static void
vala_hash_map_value_collection_instance_init (ValaHashMapValueCollection *self,
                                              gpointer klass)
{
  self->priv = vala_hash_map_value_collection_get_instance_private (self);
}

static void
vala_hash_map_value_collection_finalize (ValaIterable *obj)
{
  ValaHashMapValueCollection *self =
    G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_HASH_MAP_TYPE_VALUE_COLLECTION,
                                ValaHashMapValueCollection);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  VALA_ITERABLE_CLASS (vala_hash_map_value_collection_parent_class)->finalize (obj);
}

static GType
vala_hash_map_value_collection_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashMapValueCollectionClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_map_value_collection_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashMapValueCollection),
      0,
      (GInstanceInitFunc)vala_hash_map_value_collection_instance_init,
      NULL
    };
  GType vala_hash_map_value_collection_type_id =
    g_type_register_static (VALA_TYPE_COLLECTION, "ValaHashMapValueCollection",
                            &g_define_type_info, 0);
  vala_hash_map_value_collection_private_offset =
    g_type_add_instance_private (vala_hash_map_value_collection_type_id,
                                 sizeof (ValaHashMapValueCollectionPrivate));
  return vala_hash_map_value_collection_type_id;
}

static GType
vala_hash_map_value_collection_get_type (void)
{
  static volatile gsize vala_hash_map_value_collection_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_map_value_collection_type_id__volatile))
    {
      GType vala_hash_map_value_collection_type_id;
      vala_hash_map_value_collection_type_id
          = vala_hash_map_value_collection_get_type_once ();
      g_once_init_leave (&vala_hash_map_value_collection_type_id__volatile,
                         vala_hash_map_value_collection_type_id);
    }
  return vala_hash_map_value_collection_type_id__volatile;
}

static inline gpointer
vala_hash_map_value_iterator_get_instance_private (
    ValaHashMapValueIterator *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_map_value_iterator_private_offset);
}

static void
vala_hash_map_value_iterator_set_map (ValaHashMapValueIterator *self,
                                      ValaHashMap *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  self->priv->map = vala_map_ref (value);
  self->priv->stamp = value->priv->stamp;
}

static ValaHashMapValueIterator *
vala_hash_map_value_iterator_construct (
    GType object_type, GType k_type, GBoxedCopyFunc k_dup_func,
    GDestroyNotify k_destroy_func, GType v_type, GBoxedCopyFunc v_dup_func,
    GDestroyNotify v_destroy_func, ValaHashMap *map)
{
  g_return_val_if_fail (map != NULL, NULL);
  ValaHashMapValueIterator *self =
    (ValaHashMapValueIterator *)vala_iterator_construct (object_type,
                                                         v_type,
                                                         v_dup_func,
                                                         v_destroy_func);
  self->priv->k_type = k_type;
  self->priv->k_dup_func = k_dup_func;
  self->priv->k_destroy_func = k_destroy_func;
  self->priv->v_type = v_type;
  self->priv->v_dup_func = v_dup_func;
  self->priv->v_destroy_func = v_destroy_func;
  vala_hash_map_value_iterator_set_map (self, map);
  return self;
}

static ValaHashMapValueIterator *
vala_hash_map_value_iterator_new (GType k_type, GBoxedCopyFunc k_dup_func,
                                  GDestroyNotify k_destroy_func, GType v_type,
                                  GBoxedCopyFunc v_dup_func,
                                  GDestroyNotify v_destroy_func,
                                  ValaHashMap *map)
{
  return vala_hash_map_value_iterator_construct (VALA_HASH_MAP_TYPE_VALUE_ITERATOR,
                                                 k_type,
                                                 k_dup_func,
                                                 k_destroy_func,
                                                 v_type,
                                                 v_dup_func,
                                                 v_destroy_func,
                                                 map);
}

static gboolean
vala_hash_map_value_iterator_real_next (ValaIterator *base)
{
  ValaHashMapValueIterator *self = (ValaHashMapValueIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);

  if (!vala_iterator_has_next ((ValaIterator *)self))
    return FALSE;
  self->priv->node = self->priv->next;
  self->priv->next = NULL;
  return self->priv->node != NULL;
}

static gboolean
vala_hash_map_value_iterator_real_has_next (ValaIterator *base)
{
  ValaHashMapValueIterator *self = (ValaHashMapValueIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);

  if (!self->priv->next)
    {
      if (self->priv->node)
        self->priv->next = self->priv->node->next;

      while (!self->priv->next &&
             ++self->priv->index < map->priv->n_buckets)
        self->priv->next = map->priv->nodes[self->priv->index];
    }

  return self->priv->next != NULL;
}

static gpointer
vala_hash_map_value_iterator_real_get (ValaIterator *base)
{
  ValaHashMapValueIterator *self = (ValaHashMapValueIterator *)base;
  ValaHashMap *map = self->priv->map;
  g_assert (self->priv->stamp == map->priv->stamp);
  g_assert (self->priv->node != NULL);

  gpointer value = self->priv->node->value;
  if (value && self->priv->v_dup_func)
    return self->priv->v_dup_func (value);
  else
    return value;
}

static void
vala_hash_map_value_iterator_real_remove (ValaIterator *base)
{
  g_assert_not_reached ();
}

static gboolean
vala_hash_map_value_iterator_real_get_valid (ValaIterator *base)
{
  ValaHashMapValueIterator *self = (ValaHashMapValueIterator *)base;
  return self->priv->node != NULL;
}

static void
vala_hash_map_value_iterator_class_init (ValaHashMapValueIteratorClass *klass,
                                         gpointer klass_data)
{
  vala_hash_map_value_iterator_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERATOR_CLASS (klass)->finalize = vala_hash_map_value_iterator_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_hash_map_value_iterator_private_offset);
  VALA_ITERATOR_CLASS (klass)->next = vala_hash_map_value_iterator_real_next;
  VALA_ITERATOR_CLASS (klass)->has_next = vala_hash_map_value_iterator_real_has_next;
  VALA_ITERATOR_CLASS (klass)->get = vala_hash_map_value_iterator_real_get;
  VALA_ITERATOR_CLASS (klass)->remove = vala_hash_map_value_iterator_real_remove;
  VALA_ITERATOR_CLASS (klass)->get_valid = vala_hash_map_value_iterator_real_get_valid;
}

static void
vala_hash_map_value_iterator_instance_init (ValaHashMapValueIterator *self,
                                            gpointer klass)
{
  self->priv = vala_hash_map_value_iterator_get_instance_private (self);
  self->priv->index = -1;
}

static void
vala_hash_map_value_iterator_finalize (ValaIterator *obj)
{
  ValaHashMapValueIterator *self =
    G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_HASH_MAP_TYPE_VALUE_ITERATOR,
                                ValaHashMapValueIterator);
  if (self->priv->map)
    vala_map_unref (self->priv->map);
  VALA_ITERATOR_CLASS (vala_hash_map_value_iterator_parent_class)->finalize (obj);
}

static GType
vala_hash_map_value_iterator_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashMapValueIteratorClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_map_value_iterator_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashMapValueIterator),
      0,
      (GInstanceInitFunc)vala_hash_map_value_iterator_instance_init,
      NULL
    };
  GType vala_hash_map_value_iterator_type_id =
    g_type_register_static (VALA_TYPE_ITERATOR, "ValaHashMapValueIterator",
                            &g_define_type_info, 0);
  vala_hash_map_value_iterator_private_offset =
    g_type_add_instance_private (vala_hash_map_value_iterator_type_id,
                                 sizeof (ValaHashMapValueIteratorPrivate));
  return vala_hash_map_value_iterator_type_id;
}

static GType
vala_hash_map_value_iterator_get_type (void)
{
  static volatile gsize vala_hash_map_value_iterator_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_map_value_iterator_type_id__volatile))
    {
      GType vala_hash_map_value_iterator_type_id;
      vala_hash_map_value_iterator_type_id
          = vala_hash_map_value_iterator_get_type_once ();
      g_once_init_leave (&vala_hash_map_value_iterator_type_id__volatile,
                         vala_hash_map_value_iterator_type_id);
    }
  return vala_hash_map_value_iterator_type_id__volatile;
}

static void
vala_hash_map_class_init (ValaHashMapClass *klass, gpointer klass_data)
{
  vala_hash_map_parent_class = g_type_class_peek_parent (klass);
  VALA_MAP_CLASS (klass)->finalize = vala_hash_map_finalize;
  g_type_class_adjust_private_offset (klass, &vala_hash_map_private_offset);
  VALA_MAP_CLASS (klass)->get_keys = vala_hash_map_real_get_keys;
  VALA_MAP_CLASS (klass)->get_values = vala_hash_map_real_get_values;
  VALA_MAP_CLASS (klass)->map_iterator = vala_hash_map_real_map_iterator;
  VALA_MAP_CLASS (klass)->contains = vala_hash_map_real_contains;
  VALA_MAP_CLASS (klass)->get = vala_hash_map_real_get;
  VALA_MAP_CLASS (klass)->set = vala_hash_map_real_set;
  VALA_MAP_CLASS (klass)->remove = vala_hash_map_real_remove;
  VALA_MAP_CLASS (klass)->clear = vala_hash_map_real_clear;
  VALA_MAP_CLASS (klass)->get_size = vala_hash_map_real_get_size;
}

static void
vala_hash_map_instance_init (ValaHashMap *self, gpointer klass)
{
  self->priv = vala_hash_map_get_instance_private (self);
  self->priv->stamp = 0;
}

static void
vala_hash_map_finalize (ValaMap *obj)
{
  ValaHashMap *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_HASH_MAP, ValaHashMap);
  vala_map_clear ((ValaMap *)self);
  vala_array_free (self->priv->nodes, self->priv->n_buckets,
                   (GDestroyNotify)vala_hash_map_node_free);
  VALA_MAP_CLASS (vala_hash_map_parent_class)->finalize (obj);
}

/**
 * Hashtable implementation of the Map interface.
 */
static GType
vala_hash_map_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashMapClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_map_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashMap),
      0,
      (GInstanceInitFunc)vala_hash_map_instance_init,
      NULL
    };
  GType vala_hash_map_type_id =
    g_type_register_static (VALA_TYPE_MAP, "ValaHashMap",
                            &g_define_type_info, 0);
  vala_hash_map_private_offset =
    g_type_add_instance_private (vala_hash_map_type_id,
                                 sizeof (ValaHashMapPrivate));
  return vala_hash_map_type_id;
}

GType
vala_hash_map_get_type (void)
{
  static volatile gsize vala_hash_map_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_map_type_id__volatile))
    {
      GType vala_hash_map_type_id;
      vala_hash_map_type_id = vala_hash_map_get_type_once ();
      g_once_init_leave (&vala_hash_map_type_id__volatile,
                         vala_hash_map_type_id);
    }
  return vala_hash_map_type_id__volatile;
}
