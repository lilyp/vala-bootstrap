/* array.c
 *
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 */

#include <glib.h>
#include <glib-object.h>
#include <string.h>

G_GNUC_INTERNAL void
vala_array_destroy (gpointer *array, gint length, GDestroyNotify destroy)
{
  if (array && destroy)
    for (gint i = 0; i < length; ++i)
      if (array[i])
        destroy (array[i]);
}

G_GNUC_INTERNAL void
vala_array_free (gpointer *array, gint length, GDestroyNotify destroy)
{
  vala_array_destroy (array, length, destroy);
  g_free (array);
}

G_GNUC_INTERNAL void
vala_array_move (gpointer array, gsize element_size, gint src, gint dest,
                 gint length)
{
  char *arr = (char *)array;

  memmove (arr + (dest * element_size),
           arr + (src * element_size),
           length * element_size);
  if ((src < dest) && ((src + length) > dest))
    memset (arr + (src * element_size), 0, (dest - src) * element_size);
  else if ((src > dest) && (src < (dest + length)))
    memset (arr + ((dest + length) * element_size), 0,
            (src - dest) * element_size);
  else if (src != dest)
    memset (arr + (src * element_size), 0, length * element_size);
}

G_GNUC_INTERNAL void
vala_array_add (gpointer **array, gint *size, gint *capacity, gpointer value)
{
  if ((*size) == (*capacity))
    {
      *capacity = (*size) ? (2 * (*capacity)) : 4;
      *array = g_renew (gpointer, *array, (*capacity) + 1);
    }
  (*array)[(*size)++] = value;
  (*array)[*size] = NULL;
}
