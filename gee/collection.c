/* collection.c
 *
 * Copyright (C) 2007  Jürg Billeter
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.priker@gmail.com>
 */

#include "collection.h"
#include <float.h>
#include <glib-object.h>
#include <glib.h>
#include <math.h>

struct _ValaCollectionPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
};

static gint vala_collection_private_offset;
static gpointer vala_collection_parent_class = NULL;

static gboolean vala_collection_real_contains (ValaCollection *self,
                                               gconstpointer item);
static gboolean vala_collection_real_add (ValaCollection *self,
                                          gconstpointer item);
static gboolean vala_collection_real_remove (ValaCollection *self,
                                             gconstpointer item);
static void vala_collection_real_clear (ValaCollection *self);
static gboolean vala_collection_real_add_all (ValaCollection *self,
                                              ValaCollection *collection);
static gpointer *vala_collection_real_to_array (ValaCollection *self,
                                                gint *length);
static gboolean *vala_collection_to_bool_array (ValaCollection *self,
                                                gint *length);
static gchar *vala_collection_to_char_array (ValaCollection *self,
                                             gint *length);
static guchar *vala_collection_to_uchar_array (ValaCollection *self,
                                               gint *length);
static gint *vala_collection_to_int_array (ValaCollection *self,
                                           gint *length);
static guint *vala_collection_to_uint_array (ValaCollection *self,
                                             gint *length);
static gint64 **vala_collection_to_int64_array (ValaCollection *self,
                                                gint *length);
static guint64 **vala_collection_to_uint64_array (ValaCollection *self,
                                                  gint *length);
static glong *vala_collection_to_long_array (ValaCollection *self,
                                             gint *length);
static gulong *vala_collection_to_ulong_array (ValaCollection *self,
                                               gint *length);
static gfloat **vala_collection_to_float_array (ValaCollection *self,
                                                gint *length);
static gdouble **vala_collection_to_double_array (ValaCollection *self,
                                                  gint *length);
static GType vala_collection_get_type_once (void);

static inline gpointer
vala_collection_get_instance_private (ValaCollection *self)
{
  return G_STRUCT_MEMBER_P (self, vala_collection_private_offset);
}

gint
vala_collection_get_size (ValaCollection *self)
{
  g_return_val_if_fail (self != NULL, 0);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->get_size)
    return klass->get_size (self);
  return -1;
}

gboolean
vala_collection_get_is_empty (ValaCollection *self)
{
  g_return_val_if_fail (self != NULL, FALSE);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->get_is_empty)
    return klass->get_is_empty (self);
  return FALSE;
}

static gboolean
vala_collection_real_get_is_empty (ValaCollection *base)
{
  return vala_collection_get_size (base) == 0;
}

/**
 * Determines whether this collection contains the specified item.
 *
 * @param item the item to locate in the collection
 *
 * @return true if item is found, false otherwise
 */
static gboolean
vala_collection_real_contains (ValaCollection *self, gconstpointer item)
{
  g_critical ("Type `%s' does not implement abstract method "
              "`vala_collection_contains'",
              g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return FALSE;
}

gboolean
vala_collection_contains (ValaCollection *self, gconstpointer item)
{
  g_return_val_if_fail (self != NULL, FALSE);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->contains)
    return klass->contains (self, item);
  return FALSE;
}

/**
 * Adds an item to this collection. Must not be called on read-only
 * collections.
 *
 * @param item the item to add to the collection
 *
 * @return true if the collection has been changed, false otherwise
 */
static gboolean
vala_collection_real_add (ValaCollection *self, gconstpointer item)
{
  g_critical ("Type `%s' does not implement abstract method "
              "`vala_collection_add'",
              g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return FALSE;
}

gboolean
vala_collection_add (ValaCollection *self, gconstpointer item)
{
  g_return_val_if_fail (self != NULL, FALSE);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->add)
    return klass->add (self, item);
  return FALSE;
}

/**
 * Removes the first occurrence of an item from this collection. Must not
 * be called on read-only collections.
 *
 * @param item the item to remove from the collection
 *
 * @return true if the collection has been changed, false otherwise
 */
static gboolean
vala_collection_real_remove (ValaCollection *self, gconstpointer item)
{
  g_critical ("Type `%s' does not implement abstract method "
              "`vala_collection_remove'",
              g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return FALSE;
}

gboolean
vala_collection_remove (ValaCollection *self, gconstpointer item)
{
  g_return_val_if_fail (self != NULL, FALSE);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->remove)
    return klass->remove (self, item);
  return FALSE;
}

/**
 * Removes all items from this collection. Must not be called on
 * read-only collections.
 */
static void
vala_collection_real_clear (ValaCollection *self)
{
  g_critical ("Type `%s' does not implement abstract method "
              "`vala_collection_clear'",
              g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return;
}

void
vala_collection_clear (ValaCollection *self)
{
  g_return_if_fail (self != NULL);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->clear)
    klass->clear (self);
}

/**
 * Adds all items in the input collection to this collection.
 *
 * @param collection the collection which items will be added to this
 *                   collection.
 *
 * @return ``true`` if the collection has been changed, ``false`` otherwise
 */
static gboolean
vala_collection_real_add_all (ValaCollection *self, ValaCollection *collection)
{
  g_return_val_if_fail (collection != NULL, FALSE);
  ValaIterator *iter = vala_iterable_iterator ((ValaIterable *)collection);
  gboolean changed = FALSE;

  while (vala_iterator_next (iter))
    {
      gpointer item = vala_iterator_get (iter);
      if (!vala_collection_contains (self, item))
        {
          vala_collection_add (self, item);
          changed = TRUE;
        }
    }

  vala_iterator_unref (iter);
  return changed;
}

gboolean
vala_collection_add_all (ValaCollection *self, ValaCollection *collection)
{
  g_return_val_if_fail (self != NULL, FALSE);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->add_all)
    return klass->add_all (self, collection);
  return FALSE;
}

/**
 * Returns an array containing all of items from this collection.
 *
 * @return an array containing all of items from this collection
 */
static gpointer *
vala_collection_real_to_array (ValaCollection *self, gint *length)
{
  GType t = self->priv->g_type;
  self = G_TYPE_CHECK_INSTANCE_CAST (self, VALA_TYPE_COLLECTION,
                                     ValaCollection);

  if (t == G_TYPE_BOOLEAN)
    return (gpointer *)vala_collection_to_bool_array (self, length);
  else if (t == G_TYPE_CHAR)
    return (gpointer *)vala_collection_to_char_array (self, length);
  else if (t == G_TYPE_UCHAR)
    return (gpointer *)vala_collection_to_uchar_array (self, length);
  else if (t == G_TYPE_INT)
    return (gpointer *)vala_collection_to_int_array (self, length);
  else if (t == G_TYPE_UINT)
    return (gpointer *)vala_collection_to_uint_array (self, length);
  else if (t == G_TYPE_INT64)
    return (gpointer *)vala_collection_to_int64_array (self, length);
  else if (t == G_TYPE_UINT64)
    return (gpointer *)vala_collection_to_uint64_array (self, length);
  else if (t == G_TYPE_LONG)
    return (gpointer *)vala_collection_to_long_array (self, length);
  else if (t == G_TYPE_ULONG)
    return (gpointer *)vala_collection_to_ulong_array (self, length);
  else if (t == G_TYPE_FLOAT)
    return (gpointer *)vala_collection_to_float_array (self, length);
  else if (t == G_TYPE_DOUBLE)
    return (gpointer *)vala_collection_to_double_array (self, length);
  else if (G_TYPE_IS_ENUM (t) || G_TYPE_IS_FLAGS (t))
    return (gpointer *)vala_collection_to_int_array (self, length);
  else
    {
      gint index = 0, len = vala_collection_get_size (self);
      gpointer *array = g_new0 (gpointer, len);
      ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
      while (vala_iterator_next (it))
        {
          gpointer elt = vala_iterator_get (it);
          array[index++] = elt;
          if (elt && self->priv->g_destroy_func)
            self->priv->g_destroy_func (elt);
        }
      vala_iterator_unref (it);
      *length = len;
      return array;
    }
}

gpointer *
vala_collection_to_array (ValaCollection *self, gint *result_length1)
{
  g_return_val_if_fail (self != NULL, NULL);
  ValaCollectionClass *klass = VALA_COLLECTION_GET_CLASS (self);
  if (klass->to_array)
    return klass->to_array (self, result_length1);
  return NULL;
}

static gboolean *
vala_collection_to_bool_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gboolean *array = g_new0 (gboolean, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gboolean) ((gintptr) vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static gchar *
vala_collection_to_char_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gchar *array = g_new0 (gchar, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gchar) ((gintptr) vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static guchar *
vala_collection_to_uchar_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  guchar *array = g_new0 (guchar, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (guchar) ((guintptr) vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
}

static gint *
vala_collection_to_int_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gint *array = g_new0 (gint, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gint) ((gintptr) vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static guint *
vala_collection_to_uint_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  guint *array = g_new0 (guint, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (guint) ((guintptr) vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static gint64 **
vala_collection_to_int64_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gint64 **array = g_new0 (gint64 *, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gint64 *) vala_iterator_get (it);
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static guint64 **
vala_collection_to_uint64_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  guint64 **array = g_new0 (guint64 *, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (guint64 *) vala_iterator_get (it);
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static glong *
vala_collection_to_long_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  glong *array = g_new0 (glong *, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (glong) ((gintptr)vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static gulong *
vala_collection_to_ulong_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gulong *array = g_new0 (gulong *, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gulong) ((guintptr)vala_iterator_get (it));
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static gfloat **
vala_collection_to_float_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gfloat **array = g_new0 (gfloat *, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gfloat *) vala_iterator_get (it);
  vala_iterator_unref (it);
  *length = len;
  return array;
}

static gdouble **
vala_collection_to_double_array (ValaCollection *self, gint *length)
{
  g_return_val_if_fail (self != NULL, NULL);
  gint index = 0, len = vala_collection_get_size (self);
  gdouble **array = g_new0 (gdouble *, len);
  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)self);
  while (vala_iterator_next (it))
    array[index++] = (gdouble *) vala_iterator_get (it);
  vala_iterator_unref (it);
  *length = len;
  return array;
}

ValaCollection *
vala_collection_construct (GType object_type, GType g_type,
                           GBoxedCopyFunc g_dup_func,
                           GDestroyNotify g_destroy_func)
{
  ValaCollection *self = NULL;
  self =
    (ValaCollection *)
    vala_iterable_construct (object_type,
                             g_type,
                             (GBoxedCopyFunc)g_dup_func,
                             (GDestroyNotify)g_destroy_func);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  return self;
}

static void
vala_collection_class_init (ValaCollectionClass *klass, gpointer klass_data)
{
  vala_collection_parent_class = g_type_class_peek_parent (klass);
  g_type_class_adjust_private_offset (klass, &vala_collection_private_offset);
  VALA_COLLECTION_CLASS (klass)->contains = vala_collection_real_contains;
  VALA_COLLECTION_CLASS (klass)->add = vala_collection_real_add;
  VALA_COLLECTION_CLASS (klass)->remove = vala_collection_real_remove;
  VALA_COLLECTION_CLASS (klass)->clear = vala_collection_real_clear;
  VALA_COLLECTION_CLASS (klass)->add_all = vala_collection_real_add_all;
  VALA_COLLECTION_CLASS (klass)->to_array = vala_collection_real_to_array;
  VALA_COLLECTION_CLASS (klass)->get_is_empty = vala_collection_real_get_is_empty;
}

static void
vala_collection_instance_init (ValaCollection *self, gpointer klass)
{
  self->priv = vala_collection_get_instance_private (self);
}

/**
 * Serves as the base interface for implementing collection classes. Defines
 * size, iteration, and modification methods.
 */
static GType
vala_collection_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCollectionClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_collection_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCollection),
      0,
      (GInstanceInitFunc)vala_collection_instance_init,
      NULL
    };
  GType vala_collection_type_id =
    g_type_register_static (VALA_TYPE_ITERABLE, "ValaCollection",
                            &g_define_type_info, G_TYPE_FLAG_ABSTRACT);
  vala_collection_private_offset =
    g_type_add_instance_private (vala_collection_type_id,
                                 sizeof (ValaCollectionPrivate));
  return vala_collection_type_id;
}

GType
vala_collection_get_type (void)
{
  static volatile gsize vala_collection_type_id__volatile = 0;
  if (g_once_init_enter (&vala_collection_type_id__volatile))
    {
      GType vala_collection_type_id;
      vala_collection_type_id = vala_collection_get_type_once ();
      g_once_init_leave (&vala_collection_type_id__volatile,
                         vala_collection_type_id);
    }
  return vala_collection_type_id__volatile;
}
