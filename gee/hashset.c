/* hashset.c
 *
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 * Copyright (C) 1997-2000  GLib Team and others
 * Copyright (C) 2007-2009  Jürg Billeter
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.priker@gmail.com>
 */

#include "hashset.h"
#include <glib-object.h>
#include <glib.h>

typedef struct _ValaHashSetNode
{
  gpointer key;
  struct _ValaHashSetNode *next;
  guint key_hash;
} ValaHashSetNode;


struct _ValaHashSetPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
  gint n_nodes;
  gint n_buckets;
  ValaHashSetNode **nodes;
  gint stamp;
  GHashFunc hash_func;
  GEqualFunc equal_func;
};

#define VALA_HASH_SET_TYPE_ITERATOR (vala_hash_set_iterator_get_type ())
#define VALA_HASH_SET_ITERATOR(obj)                                           \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_HASH_SET_TYPE_ITERATOR,            \
                               ValaHashSetIterator))
#define VALA_HASH_SET_ITERATOR_CLASS(klass)                                   \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_HASH_SET_TYPE_ITERATOR,             \
                            ValaHashSetIteratorClass))
#define VALA_HASH_SET_IS_ITERATOR(obj)                                        \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_HASH_SET_TYPE_ITERATOR))
#define VALA_HASH_SET_IS_ITERATOR_CLASS(klass)                                \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_HASH_SET_TYPE_ITERATOR))
#define VALA_HASH_SET_ITERATOR_GET_CLASS(obj)                                 \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_HASH_SET_TYPE_ITERATOR,             \
                              ValaHashSetIteratorClass))

typedef struct _ValaHashSetIteratorPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
  ValaHashSet *set;
  gint index;
  ValaHashSetNode *node;
  ValaHashSetNode *next;
  gint stamp;
} ValaHashSetIteratorPrivate;

typedef struct _ValaHashSetIterator
{
  ValaIterator parent_instance;
  ValaHashSetIteratorPrivate *priv;
} ValaHashSetIterator;

typedef struct _ValaHashSetIteratorClass
{
  ValaIteratorClass parent_class;
} ValaHashSetIteratorClass;

static gint vala_hash_set_private_offset;
static gpointer vala_hash_set_parent_class = NULL;
static gint vala_hash_set_iterator_private_offset;
static gpointer vala_hash_set_iterator_parent_class = NULL;

static void vala_hash_set_node_free (ValaHashSetNode *self);
#define VALA_HASH_SET_MIN_SIZE 11
#define VALA_HASH_SET_MAX_SIZE 13845163
static ValaHashSetNode **vala_hash_set_lookup_node (ValaHashSet *self,
                                                    gconstpointer key);
static gboolean vala_hash_set_real_contains (ValaCollection *base,
                                             gconstpointer key);
static GType vala_hash_set_real_get_element_type (ValaIterable *base);
static ValaIterator *vala_hash_set_real_iterator (ValaIterable *base);
static ValaHashSetIterator *
vala_hash_set_iterator_new (GType g_type, GBoxedCopyFunc g_dup_func,
                            GDestroyNotify g_destroy_func, ValaHashSet *set);
static ValaHashSetIterator *vala_hash_set_iterator_construct (
    GType object_type, GType g_type, GBoxedCopyFunc g_dup_func,
    GDestroyNotify g_destroy_func, ValaHashSet *set);
static GType vala_hash_set_iterator_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static gboolean vala_hash_set_real_add (ValaCollection *base,
                                        gconstpointer key);
static ValaHashSetNode *vala_hash_set_node_new (gpointer k, guint hash);
static void vala_hash_set_resize (ValaHashSet *self);
static gboolean vala_hash_set_real_remove (ValaCollection *base,
                                           gconstpointer key);
static void vala_hash_set_real_clear (ValaCollection *base);
static inline gboolean vala_hash_set_remove_helper (ValaHashSet *self,
                                                    gconstpointer key);
static void vala_hash_set_node_instance_init (ValaHashSetNode *self);
static void vala_hash_set_iterator_set_set (ValaHashSetIterator *self,
                                            ValaHashSet *value);
static gboolean vala_hash_set_iterator_real_next (ValaIterator *base);
static gboolean vala_hash_set_iterator_real_has_next (ValaIterator *base);
static gpointer vala_hash_set_iterator_real_get (ValaIterator *base);
static void vala_hash_set_iterator_real_remove (ValaIterator *base);
static void vala_hash_set_iterator_finalize (ValaIterator *obj);
static GType vala_hash_set_iterator_get_type_once (void);
static void vala_hash_set_finalize (ValaIterable *obj);
static GType vala_hash_set_get_type_once (void);
G_GNUC_INTERNAL void vala_array_free (gpointer, gint, GDestroyNotify);

static inline gpointer
vala_hash_set_get_instance_private (ValaHashSet *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_set_private_offset);
}

static gint
vala_hash_set_real_get_size (ValaCollection *base)
{
  return ((ValaHashSet *)base)->priv->n_nodes;
}

void
vala_hash_set_set_hash_func (ValaHashSet *self, GHashFunc value)
{
  g_return_if_fail (self != NULL);
  self->priv->hash_func = value;
}

void
vala_hash_set_set_equal_func (ValaHashSet *self, GEqualFunc value)
{
  g_return_if_fail (self != NULL);
  self->priv->equal_func = value;
}

ValaHashSet *
vala_hash_set_construct (GType object_type, GType g_type,
                         GBoxedCopyFunc g_dup_func,
                         GDestroyNotify g_destroy_func, GHashFunc hash_func,
                         GEqualFunc equal_func)
{
  ValaHashSet *self =
    (ValaHashSet *)vala_set_construct (object_type,
                                       g_type,
                                       g_dup_func,
                                       g_destroy_func);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  vala_hash_set_set_hash_func (self, hash_func);
  vala_hash_set_set_equal_func (self, equal_func);
  self->priv->n_buckets = VALA_HASH_SET_MIN_SIZE;
  self->priv->nodes = g_new0 (ValaHashSetNode *, self->priv->n_buckets + 1);
  return self;
}

ValaHashSet *
vala_hash_set_new (GType g_type, GBoxedCopyFunc g_dup_func,
                   GDestroyNotify g_destroy_func, GHashFunc hash_func,
                   GEqualFunc equal_func)
{
  return vala_hash_set_construct (VALA_TYPE_HASH_SET, g_type, g_dup_func,
                                  g_destroy_func, hash_func, equal_func);
}

static ValaHashSetNode **
vala_hash_set_lookup_node (ValaHashSet *self, gconstpointer key)
{
  guint hash_value = (guint) self->priv->hash_func (key);
  ValaHashSetNode **node =
    self->priv->nodes + (hash_value % self->priv->n_buckets);

  while (*node)
    {
      if (hash_value == (*node)->key_hash &&
          self->priv->equal_func ((*node)->key, key))
        break;
      node = &(*node)->next;
    }

  return node;
}

static gboolean
vala_hash_set_real_contains (ValaCollection *base, gconstpointer key)
{
  ValaHashSet *self = (ValaHashSet *)base;
  ValaHashSetNode *node = *vala_hash_set_lookup_node (self, key);
  return node != NULL;
}

static GType
vala_hash_set_real_get_element_type (ValaIterable *base)
{
  return ((ValaHashSet *)base)->priv->g_type;
}

static ValaIterator *
vala_hash_set_real_iterator (ValaIterable *base)
{
  ValaHashSet *self = (ValaHashSet *)base;
  return vala_hash_set_iterator_new (self->priv->g_type,
                                     self->priv->g_dup_func,
                                     self->priv->g_destroy_func,
                                     self);
}

static gboolean
vala_hash_set_real_add (ValaCollection *base, gconstpointer key)
{
  ValaHashSet *self = (ValaHashSet *)base;
  ValaHashSetNode **node = vala_hash_set_lookup_node (self, key);
  if (*node)
    return FALSE;
  else
    {
      guint hash_value = self->priv->hash_func (key);
      gpointer k_copy;
      if (key && self->priv->g_dup_func)
        k_copy = self->priv->g_dup_func ((gpointer) key);
      else
        k_copy = (gpointer) key;
      *node = vala_hash_set_node_new (k_copy, hash_value);
      self->priv->n_nodes += 1;
      vala_hash_set_resize (self);
      self->priv->stamp += 1;
      return TRUE;
    }
}


static gboolean
vala_hash_set_real_remove (ValaCollection *base, gconstpointer key)
{
  ValaHashSet *self = (ValaHashSet *)base;
  ValaHashSetNode **node = vala_hash_set_lookup_node (self, key);
  if (*node)
    {
      ValaHashSetNode *next = (*node)->next;
      (*node)->next = NULL;
      if ((*node)->key && self->priv->g_destroy_func)
        self->priv->g_destroy_func ((*node)->key);
      vala_hash_set_node_free (*node);
      *node = next;
      self->priv->n_nodes -= 1;
      vala_hash_set_resize (self);
      return TRUE;
    }
  return FALSE;
}

static void
vala_hash_set_real_clear (ValaCollection *base)
{
  ValaHashSet *self = (ValaHashSet *)base;
  for (gint i = 0; i < self->priv->n_buckets; ++i)
    {
      ValaHashSetNode *node = self->priv->nodes[i];
      self->priv->nodes[i] = NULL;
      while (node)
        {
          ValaHashSetNode *next = node->next;
          node->next = NULL;

          if (node->key && self->priv->g_destroy_func)
            self->priv->g_destroy_func (node->key);

          vala_hash_set_node_free (node);
          node = next;
        }
    }
  self->priv->n_nodes = 0;
  vala_hash_set_resize (self);
}

static void
vala_hash_set_resize (ValaHashSet *self)
{
  if ((self->priv->n_buckets >= (3 * self->priv->n_nodes) &&
       self->priv->n_buckets >= VALA_HASH_SET_MIN_SIZE) ||
      ((3 * self->priv->n_buckets) <= self->priv->n_nodes &&
       self->priv->n_buckets < VALA_HASH_SET_MAX_SIZE))
    {
      gint new_array_size =
        (gint)g_spaced_primes_closest ((guint)self->priv->n_nodes);
      new_array_size = CLAMP(new_array_size,
                             VALA_HASH_SET_MIN_SIZE,
                             VALA_HASH_SET_MAX_SIZE);
      ValaHashSetNode **new_nodes = g_new0 (ValaHashSetNode *, new_array_size + 1);

      for (gint i = 0; i < self->priv->n_buckets; ++i)
        {
          ValaHashSetNode *node = self->priv->nodes[i];
          while (node)
            {
              ValaHashSetNode *next = node->next;
              gint hash_val = node->key_hash % new_array_size;
              node->next = new_nodes[hash_val];
              new_nodes[hash_val] = node;
              node = next;
            }
          self->priv->nodes[i] = NULL;
        }
      vala_array_free (self->priv->nodes, self->priv->n_buckets,
                       (GDestroyNotify)vala_hash_set_node_free);
      self->priv->nodes = new_nodes;
      self->priv->n_buckets = new_array_size;
    }
}

static ValaHashSetNode *
vala_hash_set_node_new (gpointer k, guint hash)
{
  ValaHashSetNode *self = g_slice_new0 (ValaHashSetNode);
  vala_hash_set_node_instance_init (self);
  self->key = k;
  self->key_hash = hash;
  return self;
}

static void
vala_hash_set_node_instance_init (ValaHashSetNode *self)
{
}

static void
vala_hash_set_node_free (ValaHashSetNode *self)
{
  if (self->next)
    vala_hash_set_node_free (self->next);
  g_slice_free (ValaHashSetNode, self);
}

static inline gpointer
vala_hash_set_iterator_get_instance_private (ValaHashSetIterator *self)
{
  return G_STRUCT_MEMBER_P (self, vala_hash_set_iterator_private_offset);
}

static void
vala_hash_set_iterator_set_set (ValaHashSetIterator *self, ValaHashSet *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->set)
    vala_iterable_unref (self->priv->set);
  self->priv->set = vala_iterable_ref (value);
  self->priv->stamp = value->priv->stamp;
}

static ValaHashSetIterator *
vala_hash_set_iterator_construct (GType object_type, GType g_type,
                                  GBoxedCopyFunc g_dup_func,
                                  GDestroyNotify g_destroy_func,
                                  ValaHashSet *set)
{
  g_return_val_if_fail (set != NULL, NULL);
  ValaHashSetIterator *self =
    (ValaHashSetIterator *)vala_iterator_construct (object_type,
                                                    g_type,
                                                    g_dup_func,
                                                    g_destroy_func);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  vala_hash_set_iterator_set_set (self, set);
  return self;
}

static ValaHashSetIterator *
vala_hash_set_iterator_new (GType g_type, GBoxedCopyFunc g_dup_func,
                            GDestroyNotify g_destroy_func, ValaHashSet *set)
{
  return vala_hash_set_iterator_construct (VALA_HASH_SET_TYPE_ITERATOR, g_type,
                                           g_dup_func, g_destroy_func, set);
}

static gboolean
vala_hash_set_iterator_real_next (ValaIterator *base)
{
  ValaHashSetIterator *self = (ValaHashSetIterator *)base;
  ValaHashSet *set = self->priv->set;
  g_assert (self->priv->stamp == set->priv->stamp);
  if (!vala_iterator_has_next ((ValaIterator *)self))
    return FALSE;
  self->priv->node = self->priv->next;
  self->priv->next = NULL;
  return self->priv->node != NULL;
}

static gboolean
vala_hash_set_iterator_real_has_next (ValaIterator *base)
{
  ValaHashSetIterator *self = (ValaHashSetIterator *)base;
  ValaHashSet *set = self->priv->set;
  g_assert (self->priv->stamp == set->priv->stamp);

  if (!self->priv->next)
    {
      if (self->priv->node)
        self->priv->next = self->priv->node->next;

      while (!self->priv->next &&
             ++self->priv->index < set->priv->n_buckets)
        self->priv->next = set->priv->nodes[self->priv->index];
    }

  return self->priv->next != NULL;
}

static gpointer
vala_hash_set_iterator_real_get (ValaIterator *base)
{
  ValaHashSetIterator *self = (ValaHashSetIterator *)base;
  ValaHashSet *set = self->priv->set;
  g_assert (self->priv->stamp == set->priv->stamp);

  gpointer key = (gpointer) self->priv->node->key;
  if (key && self->priv->g_dup_func)
    return self->priv->g_dup_func (key);
  return key;
}

static void
vala_hash_set_iterator_real_remove (ValaIterator *base)
{
  ValaHashSetIterator *self = (ValaHashSetIterator *)base;
  ValaHashSet *set = self->priv->set;
  g_assert (self->priv->stamp == set->priv->stamp);
  g_assert (self->priv->node != NULL);

  gpointer key = self->priv->node->key;
  vala_iterator_has_next ((ValaIterator *)self);
  vala_hash_set_real_remove (set, key);
  self->priv->stamp = set->priv->stamp;
}

static gboolean
vala_hash_set_iterator_real_get_valid (ValaIterator *base)
{
  ValaHashSetIterator *self = (ValaHashSetIterator *)base;
  return self->priv->node != NULL;
}

static void
vala_hash_set_iterator_class_init (ValaHashSetIteratorClass *klass,
                                   gpointer klass_data)
{
  vala_hash_set_iterator_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERATOR_CLASS (klass)->finalize = vala_hash_set_iterator_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_hash_set_iterator_private_offset);
  VALA_ITERATOR_CLASS (klass)->next = vala_hash_set_iterator_real_next;
  VALA_ITERATOR_CLASS (klass)->has_next = vala_hash_set_iterator_real_has_next;
  VALA_ITERATOR_CLASS (klass)->get = vala_hash_set_iterator_real_get;
  VALA_ITERATOR_CLASS (klass)->remove = vala_hash_set_iterator_real_remove;
  VALA_ITERATOR_CLASS (klass)->get_valid = vala_hash_set_iterator_real_get_valid;
}

static void
vala_hash_set_iterator_instance_init (ValaHashSetIterator *self,
                                      gpointer klass)
{
  self->priv = vala_hash_set_iterator_get_instance_private (self);
  self->priv->index = -1;
  self->priv->stamp = 0;
}

static void
vala_hash_set_iterator_finalize (ValaIterator *obj)
{
  ValaHashSetIterator *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_HASH_SET_TYPE_ITERATOR,
                                     ValaHashSetIterator);
  if (self->priv->set)
    vala_iterable_unref (self->priv->set);
  VALA_ITERATOR_CLASS (vala_hash_set_iterator_parent_class)->finalize (obj);
}

static GType
vala_hash_set_iterator_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashSetIteratorClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_set_iterator_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashSetIterator),
      0,
      (GInstanceInitFunc)vala_hash_set_iterator_instance_init,
      NULL
    };
  GType vala_hash_set_iterator_type_id =
    g_type_register_static (VALA_TYPE_ITERATOR, "ValaHashSetIterator",
                            &g_define_type_info, 0);
  vala_hash_set_iterator_private_offset =
    g_type_add_instance_private (vala_hash_set_iterator_type_id,
                                 sizeof (ValaHashSetIteratorPrivate));
  return vala_hash_set_iterator_type_id;
}

static GType
vala_hash_set_iterator_get_type (void)
{
  static volatile gsize vala_hash_set_iterator_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_set_iterator_type_id__volatile))
    {
      GType vala_hash_set_iterator_type_id;
      vala_hash_set_iterator_type_id = vala_hash_set_iterator_get_type_once ();
      g_once_init_leave (&vala_hash_set_iterator_type_id__volatile,
                         vala_hash_set_iterator_type_id);
    }
  return vala_hash_set_iterator_type_id__volatile;
}

static void
vala_hash_set_class_init (ValaHashSetClass *klass, gpointer klass_data)
{
  vala_hash_set_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERABLE_CLASS (klass)->finalize = vala_hash_set_finalize;
  g_type_class_adjust_private_offset (klass, &vala_hash_set_private_offset);
  VALA_COLLECTION_CLASS (klass)->contains = vala_hash_set_real_contains;
  VALA_ITERABLE_CLASS (klass)->get_element_type = vala_hash_set_real_get_element_type;
  VALA_ITERABLE_CLASS (klass)->iterator = vala_hash_set_real_iterator;
  VALA_COLLECTION_CLASS (klass)->add = vala_hash_set_real_add;
  VALA_COLLECTION_CLASS (klass)->remove = vala_hash_set_real_remove;
  VALA_COLLECTION_CLASS (klass)->clear = vala_hash_set_real_clear;
  VALA_COLLECTION_CLASS (klass)->get_size = vala_hash_set_real_get_size;
}

static void
vala_hash_set_instance_init (ValaHashSet *self, gpointer klass)
{
  self->priv = vala_hash_set_get_instance_private (self);
  self->priv->stamp = 0;
}

static void
vala_hash_set_finalize (ValaIterable *obj)
{
  ValaHashSet *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_HASH_SET, ValaHashSet);
  vala_collection_clear ((ValaCollection *)self);
  vala_array_free (self->priv->nodes, self->priv->n_buckets,
                   (GDestroyNotify)vala_hash_set_node_free);
  VALA_ITERABLE_CLASS (vala_hash_set_parent_class)->finalize (obj);
}

/**
 * Hashtable implementation of the Set interface.
 */
static GType
vala_hash_set_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaHashSetClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_hash_set_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaHashSet),
      0,
      (GInstanceInitFunc)vala_hash_set_instance_init,
      NULL
    };
  GType vala_hash_set_type_id =
    g_type_register_static (VALA_TYPE_SET, "ValaHashSet",
                            &g_define_type_info, 0);
  vala_hash_set_private_offset =
    g_type_add_instance_private (vala_hash_set_type_id,
                                 sizeof (ValaHashSetPrivate));
  return vala_hash_set_type_id;
}

GType
vala_hash_set_get_type (void)
{
  static volatile gsize vala_hash_set_type_id__volatile = 0;
  if (g_once_init_enter (&vala_hash_set_type_id__volatile))
    {
      GType vala_hash_set_type_id;
      vala_hash_set_type_id = vala_hash_set_get_type_once ();
      g_once_init_leave (&vala_hash_set_type_id__volatile,
                         vala_hash_set_type_id);
    }
  return vala_hash_set_type_id__volatile;
}
