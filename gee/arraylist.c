/* arraylist.c
 *
 * Copyright (C) 2004-2005  Novell, Inc
 * Copyright (C) 2005  David Waite
 * Copyright (C) 2007-2008  Jürg Billeter
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.priker@gmail.com>
 */

#include "arraylist.h"
#include <glib-object.h>
#include <glib.h>

#define VALA_ARRAY_LIST_TYPE_ITERATOR (vala_array_list_iterator_get_type ())
#define VALA_ARRAY_LIST_ITERATOR(obj)                                         \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_ARRAY_LIST_TYPE_ITERATOR,          \
                               ValaArrayListIterator))
#define VALA_ARRAY_LIST_ITERATOR_CLASS(klass)                                 \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_ARRAY_LIST_TYPE_ITERATOR,           \
                            ValaArrayListIteratorClass))
#define VALA_ARRAY_LIST_IS_ITERATOR(obj)                                      \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_ARRAY_LIST_TYPE_ITERATOR))
#define VALA_ARRAY_LIST_IS_ITERATOR_CLASS(klass)                              \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_ARRAY_LIST_TYPE_ITERATOR))
#define VALA_ARRAY_LIST_ITERATOR_GET_CLASS(obj)                               \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_ARRAY_LIST_TYPE_ITERATOR,           \
                              ValaArrayListIteratorClass))

typedef struct _ValaArrayListIterator ValaArrayListIterator;
typedef struct _ValaArrayListIteratorClass ValaArrayListIteratorClass;
typedef struct _ValaArrayListIteratorPrivate ValaArrayListIteratorPrivate;

struct _ValaArrayListPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
  GEqualFunc equal_func;
  gint stamp;
};

struct _ValaArrayListIterator
{
  ValaIterator parent_instance;
  gboolean removed;
  gint stamp;
  ValaArrayListIteratorPrivate *priv;
};

struct _ValaArrayListIteratorClass
{
  ValaIteratorClass parent_class;
};

struct _ValaArrayListIteratorPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
  ValaArrayList *list;
  gint index;
};

static gint vala_array_list_private_offset;
static gpointer vala_array_list_parent_class = NULL;
static gint vala_array_list_iterator_private_offset;
static gpointer vala_array_list_iterator_parent_class = NULL;

static GType vala_array_list_real_get_element_type (ValaIterable *base);
static ValaIterator *vala_array_list_real_iterator (ValaIterable *base);
static ValaArrayListIterator *
vala_array_list_iterator_new (GType g_type, GBoxedCopyFunc g_dup_func,
                              GDestroyNotify g_destroy_func,
                              ValaArrayList *list);
static ValaArrayListIterator *vala_array_list_iterator_construct (
    GType object_type, GType g_type, GBoxedCopyFunc g_dup_func,
    GDestroyNotify g_destroy_func, ValaArrayList *list);
static GType
vala_array_list_iterator_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static gboolean vala_array_list_real_contains (ValaCollection *base,
                                               gconstpointer item);
static gint vala_array_list_real_index_of (ValaList *base, gconstpointer item);
static gpointer vala_array_list_real_get (ValaList *base, gint index);
static void vala_array_list_real_set (ValaList *base, gint index,
                                      gconstpointer item);
static gboolean vala_array_list_real_add (ValaCollection *base,
                                          gconstpointer item);
static void vala_array_list_grow_if_needed (ValaArrayList *self,
                                            gint new_count);
static void vala_array_list_real_insert (ValaList *base, gint index,
                                         gconstpointer item);
static void vala_array_list_shift (ValaArrayList *self, gint start,
                                   gint delta);
static gboolean vala_array_list_real_remove (ValaCollection *base,
                                             gconstpointer item);
static gpointer vala_array_list_real_remove_at (ValaList *base, gint index);
static void vala_array_list_real_clear (ValaCollection *base);
static void vala_array_list_set_capacity (ValaArrayList *self, gint value);
static void vala_array_list_iterator_set_list (ValaArrayListIterator *self,
                                               ValaArrayList *value);
static gboolean vala_array_list_iterator_real_next (ValaIterator *base);
static gboolean vala_array_list_iterator_real_has_next (ValaIterator *base);
static gpointer vala_array_list_iterator_real_get (ValaIterator *base);
static void vala_array_list_iterator_real_remove (ValaIterator *base);
static void vala_array_list_iterator_finalize (ValaIterator *obj);
static GType vala_array_list_iterator_get_type_once (void);
static void vala_array_list_finalize (ValaIterable *obj);
static GType vala_array_list_get_type_once (void);
G_GNUC_INTERNAL void vala_array_free (gpointer, gint, GDestroyNotify);
G_GNUC_INTERNAL void vala_array_move (gpointer, gsize, gint, gint, gint);

static inline gpointer
vala_array_list_get_instance_private (ValaArrayList *self)
{
  return G_STRUCT_MEMBER_P (self, vala_array_list_private_offset);
}

static gint
vala_array_list_real_get_size (ValaCollection *base)
{
  return ((ValaArrayList *)base)->size;
}

void
vala_array_list_set_equal_func (ValaArrayList *self, GEqualFunc value)
{
  g_return_if_fail (self != NULL);
  self->priv->equal_func = value;
}

ValaArrayList *
vala_array_list_construct (GType object_type, GType g_type,
                           GBoxedCopyFunc g_dup_func,
                           GDestroyNotify g_destroy_func,
                           GEqualFunc equal_func)
{
  ValaArrayList *self = NULL;
  self = (ValaArrayList *)vala_list_construct (object_type, g_type,
                                               g_dup_func,
                                               g_destroy_func);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  vala_array_list_set_equal_func (self, equal_func);
  return self;
}

ValaArrayList *
vala_array_list_new (GType g_type, GBoxedCopyFunc g_dup_func,
                     GDestroyNotify g_destroy_func, GEqualFunc equal_func)
{
  return vala_array_list_construct (VALA_TYPE_ARRAY_LIST, g_type, g_dup_func,
                                    g_destroy_func, equal_func);
}

static GType
vala_array_list_real_get_element_type (ValaIterable *base)
{
  return ((ValaArrayList *)base)->priv->g_type;
}

static ValaIterator *
vala_array_list_real_iterator (ValaIterable *base)
{
  ValaArrayList *self = (ValaArrayList *)base;
  return
    (ValaIterator *)
    vala_array_list_iterator_new (self->priv->g_type,
                                  self->priv->g_dup_func,
                                  self->priv->g_destroy_func,
                                  self);
}

static gboolean
vala_array_list_real_contains (ValaCollection *base, gconstpointer item)
{
  return vala_list_index_of ((ValaList *)base, item) != -1;
}

static gint
vala_array_list_real_index_of (ValaList *base, gconstpointer item)
{
  ValaArrayList *self = (ValaArrayList *)base;
  for (gint index = 0; index < self->size; ++index)
    if (self->priv->equal_func (self->items[index], item))
      return index;
  return -1;
}

static gpointer
vala_array_list_real_get (ValaList *base, gint index)
{
  ValaArrayList *self = (ValaArrayList *)base;
  g_assert (index >= 0 && index <= self->size);
  gpointer item = self->items[index];
  if (item && self->priv->g_dup_func)
    return self->priv->g_dup_func (item);
  else
    return item;
}

static void
vala_array_list_do_real_set (ValaArrayList *self, gint index, gconstpointer item)
{
  gpointer old = self->items[index];
  if (old && self->priv->g_destroy_func)
    self->priv->g_destroy_func (old);

  if (item && self->priv->g_dup_func)
    self->items[index] = self->priv->g_dup_func ((gpointer)item);
  else
    self->items[index] = (gpointer)item;
}

static void
vala_array_list_real_set (ValaList *base, gint index, gconstpointer item)
{
  ValaArrayList *self = (ValaArrayList *)base;
  g_assert (index >= 0 && index < self->size);
  vala_array_list_do_real_set (self, index, item);
}

static gboolean
vala_array_list_real_add (ValaCollection *base, gconstpointer item)
{
  ValaArrayList *self = (ValaArrayList *)base;
  if (self->size == self->capacity)
    vala_array_list_grow_if_needed (self, 1);
  vala_array_list_do_real_set (self, self->size, item);
  self->size += 1;
  self->priv->stamp += 1;
  return TRUE;
}

static void
vala_array_list_real_insert (ValaList *base, gint index, gconstpointer item)
{
  ValaArrayList *self = (ValaArrayList *)base;
  g_assert (index >= 0 && index <= self->size);
  if (self->size == self->capacity)
    vala_array_list_grow_if_needed (self, 1);
  vala_array_list_shift (self, index, 1);
  vala_array_list_do_real_set (self, index, item);
  self->priv->stamp += 1;
}

static gboolean
vala_array_list_real_remove (ValaCollection *base, gconstpointer item)
{
  ValaArrayList *self = (ValaArrayList *)base;
  gint index = vala_list_index_of ((ValaList *)self, item);
  if (index >= 0)
    {
      gpointer removed = vala_list_remove_at ((ValaList *)self, index);
      if (removed && self->priv->g_destroy_func)
        self->priv->g_destroy_func (removed);
      return TRUE;
    }
  else
    return FALSE;
}

static gpointer
vala_array_list_real_remove_at (ValaList *base, gint index)
{
  ValaArrayList *self = (ValaArrayList *)base;
  gpointer item, result;
  g_assert (index >= 0 && index < self->size);
  item = self->items[index];
  if (item && self->priv->g_dup_func)
    result = self->priv->g_dup_func ((gpointer)item);
  else
    result = (gpointer)item;
  if (item && self->priv->g_destroy_func)
    self->priv->g_destroy_func (item);
  self->items[index] = NULL;
  vala_array_list_shift (self, index + 1, -1);
  self->priv->stamp += 1;
  return result;
}

static void
vala_array_list_real_clear (ValaCollection *base)
{
  ValaArrayList *self = (ValaArrayList *)base;
  for (gint index = 0; index < self->size; ++index)
    if (self->items[index] && self->priv->g_destroy_func)
      {
        self->priv->g_destroy_func (self->items[index]);
        self->items[index] = NULL;
      }
  self->size = 0;
}

static void
vala_array_list_shift (ValaArrayList *self, gint start, gint delta)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (start >= 0 && start <= self->size && start >= -delta);
  vala_array_move (self->items, sizeof (gpointer), start, start + delta,
                   self->size - start);
  self->size += delta;
}

static void
vala_array_list_grow_if_needed (ValaArrayList *self, gint new_count)
{
  g_assert (new_count >= 0);
  gint minimum_size = self->size + new_count;
  gint capacity = self->capacity;
  if (minimum_size > self->capacity)
    {
      if (new_count > self->capacity)
        capacity = minimum_size;
      else
        capacity *= 2;
    }
  vala_array_list_set_capacity (self, capacity);
}

static void
vala_array_list_set_capacity (ValaArrayList *self, gint capacity)
{
  g_return_if_fail (self != NULL);
  g_assert (capacity >= self->size);
  self->items = g_renew (gpointer, self->items, capacity);
  if (capacity > self->capacity)
    memset (self->items + self->capacity, 0,
            sizeof (gpointer) * (capacity - self->capacity));
  self->capacity = capacity;
}

static inline gpointer
vala_array_list_iterator_get_instance_private (ValaArrayListIterator *self)
{
  return G_STRUCT_MEMBER_P (self, vala_array_list_iterator_private_offset);
}

static void
vala_array_list_iterator_set_list (ValaArrayListIterator *self,
                                   ValaArrayList *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->list)
    {
      vala_iterable_unref (self->priv->list);
      self->priv->list = NULL;
    }
  self->priv->list = vala_iterable_ref (value);
  self->stamp = value->priv->stamp;
}

static ValaArrayListIterator *
vala_array_list_iterator_construct (GType object_type, GType g_type,
                                    GBoxedCopyFunc g_dup_func,
                                    GDestroyNotify g_destroy_func,
                                    ValaArrayList *list)
{
  ValaArrayListIterator *self = NULL;
  g_return_val_if_fail (list != NULL, NULL);
  self =
    (ValaArrayListIterator *)
    vala_iterator_construct (object_type,
                             g_type,
                             (GBoxedCopyFunc)g_dup_func,
                             (GDestroyNotify)g_destroy_func);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  vala_array_list_iterator_set_list (self, list);
  return self;
}

static ValaArrayListIterator *
vala_array_list_iterator_new (GType g_type, GBoxedCopyFunc g_dup_func,
                              GDestroyNotify g_destroy_func,
                              ValaArrayList *list)
{
  return vala_array_list_iterator_construct (VALA_ARRAY_LIST_TYPE_ITERATOR,
                                             g_type,
                                             g_dup_func,
                                             g_destroy_func,
                                             list);
}

static gboolean
vala_array_list_iterator_real_next (ValaIterator *base)
{
  ValaArrayListIterator *self = (ValaArrayListIterator *)base;
  ValaArrayList *list = self->priv->list;
  if (self->priv->index < list->size)
    {
      self->priv->index += 1;
      self->removed = FALSE;
    }
  return self->priv->index < list->size;
}

static gboolean
vala_array_list_iterator_real_has_next (ValaIterator *base)
{
  ValaArrayListIterator *self = (ValaArrayListIterator *)base;
  ValaArrayList *list = self->priv->list;
  g_assert (self->stamp == list->priv->stamp);
  return (self->priv->index + 1) < list->size;
}

static gpointer
vala_array_list_iterator_real_get (ValaIterator *base)
{
  ValaArrayListIterator *self = (ValaArrayListIterator *)base;
  ValaArrayList *list = self->priv->list;
  g_assert (self->stamp == list->priv->stamp);
  g_assert (!self->removed);
  if (self->priv->index < 0 || self->priv->index >= list->size)
    return NULL;
  return vala_list_get ((ValaList *)list, self->priv->index);
}

static void
vala_array_list_iterator_real_remove (ValaIterator *base)
{
  ValaArrayListIterator *self = (ValaArrayListIterator *)base;
  ValaArrayList *list = self->priv->list;
  g_assert (self->stamp == list->priv->stamp);
  g_assert (!self->removed);
  g_assert(self->priv->index >= 0 && self->priv->index < list->size);
  gpointer item = vala_list_remove_at ((ValaList *)list, self->priv->index);
  if (item && self->priv->g_destroy_func)
    self->priv->g_destroy_func (item);
  self->priv->index -= 1;
  self->removed = TRUE;
  self->stamp = list->priv->stamp;
}

static gboolean
vala_array_list_iterator_real_get_valid (ValaIterator *base)
{
  ValaArrayListIterator *self = (ValaArrayListIterator *)base;
  return
    self->priv->index >= 0 &&
    self->priv->index < self->priv->list->size &&
    !self->removed;
}

static void
vala_array_list_iterator_class_init (ValaArrayListIteratorClass *klass,
                                     gpointer klass_data)
{
  vala_array_list_iterator_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERATOR_CLASS (klass)->finalize = vala_array_list_iterator_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_array_list_iterator_private_offset);
  VALA_ITERATOR_CLASS (klass)->next = vala_array_list_iterator_real_next;
  VALA_ITERATOR_CLASS (klass)->has_next = vala_array_list_iterator_real_has_next;
  VALA_ITERATOR_CLASS (klass)->get = vala_array_list_iterator_real_get;
  VALA_ITERATOR_CLASS (klass)->remove = vala_array_list_iterator_real_remove;
  VALA_ITERATOR_CLASS (klass)->get_valid = vala_array_list_iterator_real_get_valid;
}

static void
vala_array_list_iterator_instance_init (ValaArrayListIterator *self,
                                        gpointer klass)
{
  self->priv = vala_array_list_iterator_get_instance_private (self);
  self->priv->index = -1;
  self->removed = FALSE;
  self->stamp = 0;
}

static void
vala_array_list_iterator_finalize (ValaIterator *obj)
{
  ValaArrayListIterator *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_ARRAY_LIST_TYPE_ITERATOR,
                                     ValaArrayListIterator);
  if (self->priv->list)
    {
      vala_iterable_unref (self->priv->list);
      self->priv->list = NULL;
    }
  VALA_ITERATOR_CLASS (vala_array_list_iterator_parent_class)->finalize (obj);
}

static GType
vala_array_list_iterator_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaArrayListIteratorClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_array_list_iterator_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaArrayListIterator),
      0,
      (GInstanceInitFunc)vala_array_list_iterator_instance_init,
      NULL
    };
  GType vala_array_list_iterator_type_id;
  vala_array_list_iterator_type_id =
    g_type_register_static (VALA_TYPE_ITERATOR, "ValaArrayListIterator",
                            &g_define_type_info, 0);
  vala_array_list_iterator_private_offset =
    g_type_add_instance_private (vala_array_list_iterator_type_id,
                                 sizeof (ValaArrayListIteratorPrivate));
  return vala_array_list_iterator_type_id;
}

static GType
vala_array_list_iterator_get_type (void)
{
  static volatile gsize vala_array_list_iterator_type_id__volatile = 0;
  if (g_once_init_enter (&vala_array_list_iterator_type_id__volatile))
    {
      GType vala_array_list_iterator_type_id;
      vala_array_list_iterator_type_id =
        vala_array_list_iterator_get_type_once ();
      g_once_init_leave (&vala_array_list_iterator_type_id__volatile,
                         vala_array_list_iterator_type_id);
    }
  return vala_array_list_iterator_type_id__volatile;
}

static void
vala_array_list_class_init (ValaArrayListClass *klass, gpointer klass_data)
{
  vala_array_list_parent_class = g_type_class_peek_parent (klass);
  VALA_ITERABLE_CLASS (klass)->finalize = vala_array_list_finalize;
  g_type_class_adjust_private_offset (klass, &vala_array_list_private_offset);
  VALA_ITERABLE_CLASS (klass)->get_element_type = vala_array_list_real_get_element_type;
  VALA_ITERABLE_CLASS (klass)->iterator = vala_array_list_real_iterator;
  VALA_COLLECTION_CLASS (klass)->contains = vala_array_list_real_contains;
  VALA_LIST_CLASS (klass)->index_of = vala_array_list_real_index_of;
  VALA_LIST_CLASS (klass)->get = vala_array_list_real_get;
  VALA_LIST_CLASS (klass)->set = vala_array_list_real_set;
  VALA_COLLECTION_CLASS (klass)->add = vala_array_list_real_add;
  VALA_LIST_CLASS (klass)->insert = vala_array_list_real_insert;
  VALA_COLLECTION_CLASS (klass)->remove = vala_array_list_real_remove;
  VALA_LIST_CLASS (klass)->remove_at = vala_array_list_real_remove_at;
  VALA_COLLECTION_CLASS (klass)->clear = vala_array_list_real_clear;
  VALA_COLLECTION_CLASS (klass)->get_size = vala_array_list_real_get_size;
}

static void
vala_array_list_instance_init (ValaArrayList *self, gpointer klass)
{
  self->priv = vala_array_list_get_instance_private (self);
  self->items = g_new0 (gpointer, 4);
  self->capacity = 4;
  self->priv->stamp = 0;
}

static void
vala_array_list_finalize (ValaIterable *obj)
{
  ValaArrayList *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_ARRAY_LIST, ValaArrayList);
  vala_array_free (self->items, self->capacity, self->priv->g_destroy_func);
  self->items = NULL;
  VALA_ITERABLE_CLASS (vala_array_list_parent_class)->finalize (obj);
}

/**
 * Arrays of arbitrary elements which grow automatically as elements are added.
 */
static GType
vala_array_list_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaArrayListClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_array_list_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaArrayList),
      0,
      (GInstanceInitFunc)vala_array_list_instance_init,
      NULL
    };
  GType vala_array_list_type_id;
  vala_array_list_type_id =
    g_type_register_static (VALA_TYPE_LIST, "ValaArrayList",
                            &g_define_type_info, 0);
  vala_array_list_private_offset =
    g_type_add_instance_private (vala_array_list_type_id,
                                 sizeof (ValaArrayListPrivate));
  return vala_array_list_type_id;
}

GType
vala_array_list_get_type (void)
{
  static volatile gsize vala_array_list_type_id__volatile = 0;
  if (g_once_init_enter (&vala_array_list_type_id__volatile))
    {
      GType vala_array_list_type_id;
      vala_array_list_type_id = vala_array_list_get_type_once ();
      g_once_init_leave (&vala_array_list_type_id__volatile,
                         vala_array_list_type_id);
    }
  return vala_array_list_type_id__volatile;
}
