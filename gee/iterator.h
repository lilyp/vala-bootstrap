#ifndef __VALAGEE_ITERATOR_H__
#define __VALAGEE_ITERATOR_H__

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define VALA_TYPE_ITERATOR (vala_iterator_get_type ())
#define VALA_ITERATOR(obj)                                                    \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_ITERATOR, ValaIterator))
#define VALA_ITERATOR_CLASS(klass)                                            \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_ITERATOR, ValaIteratorClass))
#define VALA_IS_ITERATOR(obj)                                                 \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_ITERATOR))
#define VALA_IS_ITERATOR_CLASS(klass)                                         \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_ITERATOR))
#define VALA_ITERATOR_GET_CLASS(obj)                                          \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_ITERATOR, ValaIteratorClass))

typedef struct _ValaIteratorPrivate ValaIteratorPrivate;

typedef struct _ValaIterator
{
  GTypeInstance parent_instance;
  volatile int ref_count;
  ValaIteratorPrivate *priv;
} ValaIterator;

typedef struct _ValaIteratorClass
{
  GTypeClass parent_class;
  void (*finalize) (ValaIterator *self);
  gboolean (*next) (ValaIterator *self);
  gboolean (*has_next) (ValaIterator *self);
  gpointer (*get) (ValaIterator *self);
  void (*remove) (ValaIterator *self);
  gboolean (*get_valid) (ValaIterator *self);
} ValaIteratorClass;

gpointer vala_iterator_ref (gpointer instance);
void vala_iterator_unref (gpointer instance);
GParamSpec *vala_param_spec_iterator (const gchar *name, const gchar *nick,
                                      const gchar *blurb, GType object_type,
                                      GParamFlags flags);
void vala_value_set_iterator (GValue *value, gpointer v_object);
void vala_value_take_iterator (GValue *value, gpointer v_object);
gpointer vala_value_get_iterator (const GValue *value);
GType vala_iterator_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ValaIterator, vala_iterator_unref)

gboolean vala_iterator_next (ValaIterator *self);
gboolean vala_iterator_has_next (ValaIterator *self);
gpointer vala_iterator_get (ValaIterator *self);
void vala_iterator_remove (ValaIterator *self);
gboolean vala_iterator_get_valid (ValaIterator *self);
ValaIterator *vala_iterator_construct (GType object_type, GType g_type,
                                       GBoxedCopyFunc g_dup_func,
                                       GDestroyNotify g_destroy_func);

G_END_DECLS

#endif
