#ifndef __VALAGEE_H__
#define __VALAGEE_H__

#include "gee/iterator.h"
#include "gee/iterable.h"
#include "gee/collection.h"
#include "gee/list.h"
#include "gee/arraylist.h"
#include "gee/set.h"
#include "gee/mapiterator.h"
#include "gee/map.h"
#include "gee/hashmap.h"
#include "gee/hashset.h"

#endif
