/* timsort.c
 *
 * Copyright (C) 2009  Didier Villevalois
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 */

#include "valagee.h"
#include <glib-object.h>
#include <glib.h>
#include <gobject/gvaluecollector.h>

#define VALA_TYPE_TIM_SORT (vala_tim_sort_get_type ())
#define VALA_TIM_SORT(obj)                                                    \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VALA_TYPE_TIM_SORT, ValaTimSort))
#define VALA_TIM_SORT_CLASS(klass)                                            \
  (G_TYPE_CHECK_CLASS_CAST ((klass), VALA_TYPE_TIM_SORT, ValaTimSortClass))
#define VALA_IS_TIM_SORT(obj)                                                 \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_TIM_SORT))
#define VALA_IS_TIM_SORT_CLASS(klass)                                         \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_TIM_SORT))
#define VALA_TIM_SORT_GET_CLASS(obj)                                          \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), VALA_TYPE_TIM_SORT, ValaTimSortClass))

typedef struct _ValaTimSortSlice
{
  void **list;
  void **new_list;
  gint index;
  gint length;
} ValaTimSortSlice;

typedef struct _ValaTimSortPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
  ValaList *list_collection;
  gpointer *array;
  gint array_size;
  void **list;
  gint index;
  gint size;
  ValaTimSortSlice **pending;
  gint n_pending;
  gint pending_capacity;
  gint minimum_gallop;
  GCompareDataFunc compare;
  gpointer compare_target;
} ValaTimSortPrivate;

typedef struct _ValaTimSort
{
  GTypeInstance parent_instance;
  volatile int ref_count;
  ValaTimSortPrivate *priv;
} ValaTimSort;

typedef struct _ValaTimSortClass
{
  GTypeClass parent_class;
  void (*finalize) (ValaTimSort *self);
} ValaTimSortClass;

typedef gboolean (*ValaTimSortLowerFunc) (gconstpointer left,
                                          gconstpointer right,
                                          gpointer user_data);
typedef struct _ValaParamSpecTimSort ValaParamSpecTimSort;

static gint vala_tim_sort_private_offset;
static gpointer vala_tim_sort_parent_class = NULL;

G_GNUC_INTERNAL gpointer vala_tim_sort_ref (gpointer instance);
G_GNUC_INTERNAL void vala_tim_sort_unref (gpointer instance);
G_GNUC_INTERNAL GType vala_tim_sort_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
static void vala_tim_sort_slice_free (ValaTimSortSlice *self);
#define VALA_TIM_SORT_MINIMUM_GALLOP 7
G_GNUC_INTERNAL void
vala_tim_sort_sort (GType g_type, GBoxedCopyFunc g_dup_func,
                    GDestroyNotify g_destroy_func, ValaList *list,
                    GCompareDataFunc compare, gpointer compare_target);
static void vala_tim_sort_sort_arraylist (
    GType g_type, GBoxedCopyFunc g_dup_func, GDestroyNotify g_destroy_func,
    ValaArrayList *list, GCompareDataFunc compare, gpointer compare_target);
static void vala_tim_sort_sort_list (GType g_type, GBoxedCopyFunc g_dup_func,
                                     GDestroyNotify g_destroy_func,
                                     ValaList *list, GCompareDataFunc compare,
                                     gpointer compare_target);
G_GNUC_INTERNAL ValaTimSort *vala_tim_sort_new (GType g_type,
                                                GBoxedCopyFunc g_dup_func,
                                                GDestroyNotify g_destroy_func);
G_GNUC_INTERNAL ValaTimSort *
vala_tim_sort_construct (GType object_type, GType g_type,
                         GBoxedCopyFunc g_dup_func,
                         GDestroyNotify g_destroy_func);
static void vala_tim_sort_do_sort (ValaTimSort *self);
static ValaTimSortSlice *vala_tim_sort_slice_new (void **list, gint index,
                                                  gint length);
static gint vala_tim_sort_compute_minimum_run_length (ValaTimSort *self,
                                                      gint length);
static ValaTimSortSlice *
vala_tim_sort_compute_longest_run (ValaTimSort *self, ValaTimSortSlice *a,
                                   gboolean *descending);
static void vala_tim_sort_slice_reverse (ValaTimSortSlice *self);
static void vala_tim_sort_insertion_sort (ValaTimSort *self,
                                          ValaTimSortSlice *a, gint offset);
static inline void vala_tim_sort_slice_shorten_start (ValaTimSortSlice *self,
                                                      gint n);
static void _vala_array_add1 (ValaTimSortSlice ***array, gint *length,
                              gint *size, ValaTimSortSlice *value);
static void vala_tim_sort_merge_collapse (ValaTimSort *self);
static void vala_tim_sort_merge_force_collapse (ValaTimSort *self);
static inline gboolean vala_tim_sort_lower_than (ValaTimSort *self,
                                                 gconstpointer left,
                                                 gconstpointer right);
static inline gboolean
vala_tim_sort_lower_than_or_equal_to (ValaTimSort *self, gconstpointer left,
                                      gconstpointer right);
static void vala_tim_sort_merge_at (ValaTimSort *self, gint index);
static gint vala_tim_sort_gallop_rightmost (ValaTimSort *self,
                                            gconstpointer key,
                                            ValaTimSortSlice *a, gint hint);
static inline void *vala_tim_sort_slice_peek_first (ValaTimSortSlice *self);
static gint vala_tim_sort_gallop_leftmost (ValaTimSort *self,
                                           gconstpointer key,
                                           ValaTimSortSlice *a, gint hint);
static inline void *vala_tim_sort_slice_peek_last (ValaTimSortSlice *self);
static void vala_tim_sort_merge_low (ValaTimSort *self, ValaTimSortSlice *a,
                                     ValaTimSortSlice *b);
static void vala_tim_sort_merge_high (ValaTimSort *self, ValaTimSortSlice *a,
                                      ValaTimSortSlice *b);
static void vala_tim_sort_slice_copy (ValaTimSortSlice *self);
static inline void *vala_tim_sort_slice_pop_first (ValaTimSortSlice *self);
static inline void vala_tim_sort_slice_merge_in (ValaTimSortSlice *self,
                                                 void **dest_array, gint index,
                                                 gint dest_index, gint count);
static inline void *vala_tim_sort_slice_pop_last (ValaTimSortSlice *self);
static inline void
vala_tim_sort_slice_merge_in_reversed (ValaTimSortSlice *self,
                                       void **dest_array, gint index,
                                       gint dest_index, gint count);
static inline void vala_tim_sort_slice_shorten_end (ValaTimSortSlice *self,
                                                    gint n);
static void vala_tim_sort_slice_instance_init (ValaTimSortSlice *self);
static inline void vala_tim_sort_slice_swap (ValaTimSortSlice *self, gint i,
                                             gint j);
static void vala_tim_sort_finalize (ValaTimSort *obj);
static GType vala_tim_sort_get_type_once (void);
G_GNUC_INTERNAL void vala_array_add (gpointer **, gint *, gint *, gpointer);
G_GNUC_INTERNAL void vala_array_free (gpointer, gint, GDestroyNotify);
G_GNUC_INTERNAL void vala_array_move (gpointer, gsize, gint, gint, gint);

static inline gpointer
vala_tim_sort_get_instance_private (ValaTimSort *self)
{
  return G_STRUCT_MEMBER_P (self, vala_tim_sort_private_offset);
}

G_GNUC_INTERNAL void
vala_tim_sort_sort (GType g_type, GBoxedCopyFunc g_dup_func,
                    GDestroyNotify g_destroy_func, ValaList *list,
                    GCompareDataFunc compare, gpointer compare_target)
{
  g_return_if_fail (list != NULL);
  if (VALA_IS_ARRAY_LIST (list))
    vala_tim_sort_sort_arraylist (g_type,
                                  g_dup_func,
                                  g_destroy_func,
                                  G_TYPE_CHECK_INSTANCE_CAST (list,
                                                              VALA_TYPE_ARRAY_LIST,
                                                              ValaArrayList),
                                  compare,
                                  compare_target);
  else
    vala_tim_sort_sort_list (g_type,
                             g_dup_func,
                             g_destroy_func,
                             list,
                             compare,
                             compare_target);
}

static void
vala_tim_sort_sort_list (GType g_type, GBoxedCopyFunc g_dup_func,
                         GDestroyNotify g_destroy_func, ValaList *list,
                         GCompareDataFunc compare, gpointer compare_target)
{
  g_return_if_fail (list != NULL);
  ValaTimSort *helper = vala_tim_sort_new (g_type, g_dup_func, g_destroy_func);
  helper->priv->list_collection = vala_iterable_ref (list);
  helper->priv->array = vala_collection_to_array (list, &helper->priv->array_size);
  helper->priv->size = vala_collection_get_size ((ValaCollection *)list);
  helper->priv->compare = compare;
  helper->priv->compare_target = compare_target;
  vala_tim_sort_do_sort (helper);
  vala_collection_clear ((ValaCollection *)list);
  for (gint item_it = 0; item_it < helper->priv->array_size; ++item_it)
    {
      gpointer item = helper->priv->array[item_it];
      if (item && g_dup_func)
        vala_collection_add ((ValaCollection *)list, g_dup_func (item));
      else
        vala_collection_add ((ValaCollection *)list, item);

      if (item && g_destroy_func)
        g_destroy_func (item);
    }
  vala_tim_sort_unref (helper);
}

static void
vala_tim_sort_sort_arraylist (GType g_type, GBoxedCopyFunc g_dup_func,
                              GDestroyNotify g_destroy_func,
                              ValaArrayList *list, GCompareDataFunc compare,
                              gpointer compare_target)
{
  g_return_if_fail (list != NULL);
  ValaTimSort *helper = vala_tim_sort_new (g_type, g_dup_func, g_destroy_func);
  helper->priv->list_collection = vala_iterable_ref ((ValaList *)list);
  helper->priv->list = list->items;
  helper->priv->index = 0;
  helper->priv->size = list->size;
  helper->priv->compare = compare;
  helper->priv->compare_target = compare_target;
  vala_tim_sort_do_sort (helper);
  vala_tim_sort_unref (helper);
}

static void
vala_tim_sort_do_sort (ValaTimSort *self)
{
  g_return_if_fail (self != NULL);
  if (self->priv->size < 2)
    return;
  self->priv->pending = g_new0 (ValaTimSortSlice *, 1);
  self->priv->n_pending = 0;
  self->priv->pending_capacity = 0;
  self->priv->minimum_gallop = VALA_TIM_SORT_MINIMUM_GALLOP;
  ValaTimSortSlice *remaining =vala_tim_sort_slice_new (self->priv->list,
                                                        self->priv->index,
                                                        self->priv->size);
  gint minimum_length =
    vala_tim_sort_compute_minimum_run_length (self, remaining->length);
  while (remaining->length)
    {
      gboolean descending = FALSE;
      ValaTimSortSlice *run = NULL;
      run = vala_tim_sort_compute_longest_run (self, remaining, &descending);
      if (descending)
        vala_tim_sort_slice_reverse (run);

      if (run->length < minimum_length)
        {
          gint sorted_count = run->length;
          run->length = MIN (minimum_length, remaining->length);
          vala_tim_sort_insertion_sort (self, run, sorted_count);
        }
      vala_tim_sort_slice_shorten_start (remaining, run->length);
      vala_array_add (&self->priv->pending, &self->priv->n_pending,
                      &self->priv->pending_capacity, run);
      vala_tim_sort_merge_collapse (self);
    }
  g_assert (remaining->index == self->priv->size);
  vala_tim_sort_merge_force_collapse (self);
  g_assert (self->priv->n_pending == 1);
  ValaTimSortSlice *pending = *self->priv->pending;
  g_assert (pending->index == 0 && pending->length == self->priv->size);
  vala_tim_sort_slice_free (remaining);
}

static inline gboolean
vala_tim_sort_lower_than (ValaTimSort *self, gconstpointer left,
                          gconstpointer right)
{
  g_return_val_if_fail (self != NULL, FALSE);
  return self->priv->compare (left, right, self->priv->compare_target) < 0;
}

static inline gboolean
vala_tim_sort_lower_than_or_equal_to (ValaTimSort *self, gconstpointer left,
                                      gconstpointer right)
{
  g_return_val_if_fail (self != NULL, FALSE);
  return self->priv->compare (left, right, self->priv->compare_target) <= 0;
}

static gint
vala_tim_sort_compute_minimum_run_length (ValaTimSort *self, gint length)
{
  gint run_length = 0;
  g_return_val_if_fail (self != NULL, 0);
  while (length >= 64)
    {
      run_length |= length & 1;
      length >>= 1;
    }
  return length + run_length;
}

static ValaTimSortSlice *
vala_tim_sort_compute_longest_run (ValaTimSort *self, ValaTimSortSlice *slice,
                                   gboolean *descending)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (slice != NULL, NULL);
  gboolean desc = FALSE;
  gint run_length = 0;
  ValaTimSortSlice *result;
  if (slice->length <= 1)
    {
      run_length = slice->length;
      desc = FALSE;
    }
  else
    {
      gint i = slice->index + 1;
      gpointer *a = slice->list;
      run_length = 2;
      desc = vala_tim_sort_lower_than (self, a[i], a[i - 1]);

      for (++i; i < slice->index + slice->length; ++i)
        {
          if (desc == vala_tim_sort_lower_than (self, a[i], a[i - 1]))
            ++run_length;
          else
            break;
        }
    }
  result = vala_tim_sort_slice_new (slice->list, slice->index, run_length);
  if (descending)
    *descending = desc;
  return result;
}

static void
vala_tim_sort_insertion_sort (ValaTimSort *self, ValaTimSortSlice *slice,
                              gint offset)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (slice != NULL);

  for (gint start = slice->index + offset;
       start < slice->index + slice->length;
       ++start)
    {
      gpointer pivot = slice->list[start];
      gint left = slice->index, right = start;
      while (left < right)
        {
          gint middle = left + ((right - left) >> 1);
          if (vala_tim_sort_lower_than (self, pivot, slice->list[middle]))
            right = middle;
          else
            left = middle + 1;
        }
      g_assert (left == right);
      memmove (slice->list + left + 1, slice->list + left,
               sizeof (gpointer) * (start - left));
      slice->list[left] = pivot;
    }
}

static void
vala_tim_sort_merge_collapse (ValaTimSort *self)
{
  g_return_if_fail (self != NULL);
  ValaTimSortSlice **pending = self->priv->pending;
  gint count = self->priv->n_pending;

  while (count > 1)
    {
      if (count >= 3 &&
          pending[count - 3]->length <=
          pending[count - 2]->length + pending[count - 1]->length)
        {
          if (pending[count - 3]->length < pending[count - 1]->length)
            vala_tim_sort_merge_at (self, count - 3);
          else
            vala_tim_sort_merge_at (self, count - 2);
        }
      else if (pending[count - 2]->length <= pending[count - 1]->length)
        vala_tim_sort_merge_at (self, count - 2);
      else
        break;
      count = self->priv->n_pending;
    }
}

static void
vala_tim_sort_merge_force_collapse (ValaTimSort *self)
{
  g_return_if_fail (self != NULL);
  ValaTimSortSlice **pending = self->priv->pending;
  gint count = self->priv->n_pending;

  while (count > 1)
    {
      if (count >= 3 &&
          pending[count - 3]->length <=
          pending[count - 2]->length + pending[count - 1]->length)
        {
          if (pending[count - 3]->length < pending[count - 1]->length)
            vala_tim_sort_merge_at (self, count - 3);
          else
            vala_tim_sort_merge_at (self, count - 2);
        }
      else if (pending[count - 2]->length <= pending[count - 1]->length)
        vala_tim_sort_merge_at (self, count - 2);
      count = self->priv->n_pending;
    }
}

static void
vala_tim_sort_merge_at (ValaTimSort *self, gint index)
{
  g_return_if_fail (self != NULL);
  ValaTimSortSlice *left = self->priv->pending[index];
  ValaTimSortSlice *right = self->priv->pending[index +1];
  g_assert (left->length > 0 && right->length > 0);
  g_assert (left->index + left->length == right->index);
  ValaTimSortSlice *dest = vala_tim_sort_slice_new (self->priv->list, left->index,
                                                    left->length + right->length);
  self->priv->pending[index] = dest;
  vala_array_move (self->priv->pending, sizeof (ValaTimSortSlice *),
                   index + 2, index + 1, self->priv->n_pending - index - 2);
  self->priv->n_pending -= 1;
  gint sorted_count =
    vala_tim_sort_gallop_rightmost (self,
                                    vala_tim_sort_slice_peek_first (right),
                                    left,
                                    0);
  vala_tim_sort_slice_shorten_start (left, sorted_count);
  if (left->length == 0)
    goto done;
  right->length =
    vala_tim_sort_gallop_leftmost (self,
                                   vala_tim_sort_slice_peek_last (left),
                                   left,
                                   right->length - 1);
  if (right->length == 0)
    goto done;

  if (left->length < right->length)
    vala_tim_sort_merge_low (self, left, right);
  else
    vala_tim_sort_merge_high (self, left, right);

 done:
  vala_tim_sort_slice_free (left);
  vala_tim_sort_slice_free (right);
}

static gint
vala_tim_sort__gallop (ValaTimSort *self, gconstpointer key,
                       ValaTimSortSlice *slice, gint hint,
                       gboolean (*gallop_compare)(ValaTimSort *,
                                                  gconstpointer,
                                                  gconstpointer))
{
  g_return_val_if_fail (self != NULL, 0);
  g_return_val_if_fail (slice != NULL, 0);
  g_assert (0 <= hint && hint < slice->length);

  gint offset = 1, last_offset = 0, cursor = slice->index + hint;
  if (gallop_compare (self, slice->list[cursor], key))
    {
      gint max_offset = slice->length - hint;
      while (offset < max_offset)
        {
          if (gallop_compare (self, slice->list[cursor + offset], key))
            {
              last_offset = offset;
              offset = (offset << 1) + 1;
            }
          else
            break;
        }
    }
  else
    {
      gint max_offset = hint + 1;
      while (offset < max_offset)
        {
          if (gallop_compare (self, slice->list[cursor - offset], key))
            break;
          else
            {
              last_offset = offset;
              offset = (offset << 1) + 1;
            }
        }

      if (offset > max_offset)
        offset = max_offset;

      gint last = last_offset, o = offset;
      last_offset = hint - o;
      offset = hint - last;
    }

  g_assert (-1 <= last_offset && last_offset < offset);
  g_assert (offset <= slice->length);

  while (last_offset < offset)
    {
      gint middle = last_offset + ((offset - last_offset) >> 1);
      if (vala_tim_sort_lower_than_or_equal_to (self, slice->list[middle], key))
        last_offset = middle + 1;
      else
        offset = middle;
    }
  g_assert (last_offset == offset);

  return offset;
}

static gint
vala_tim_sort_gallop_leftmost (ValaTimSort *self, gconstpointer key,
                               ValaTimSortSlice *slice, gint hint)
{
  return vala_tim_sort__gallop (self, key, slice, hint,
                                vala_tim_sort_lower_than);
}

static gint
vala_tim_sort_gallop_rightmost (ValaTimSort *self, gconstpointer key,
                                ValaTimSortSlice *slice, gint hint)
{
  return vala_tim_sort__gallop (self, key, slice, hint,
                                vala_tim_sort_lower_than_or_equal_to);
}

static void
vala_tim_sort_merge_low (ValaTimSort *self, ValaTimSortSlice *left,
                         ValaTimSortSlice *right)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (left != NULL);
  g_return_if_fail (right != NULL);
  g_assert (left->length > 0 && right->length > 0);
  g_assert (left->index + left->length > 0 == right->index);
  gint minimum_gallop = self->priv->minimum_gallop;
  vala_tim_sort_slice_copy (left);
  gint dest = left->index;
  self->priv->list[dest++] = vala_tim_sort_slice_pop_first (right);
  if (left->length == 1 || right->length == 0)
    goto last;

  do
    {
      gint n_left = 0, n_right = 0;
      while (n_left <= minimum_gallop && n_right <= minimum_gallop)
        {
          if (vala_tim_sort_lower_than (self,
                                        vala_tim_sort_slice_peek_first (left),
                                        vala_tim_sort_slice_peek_first (right)))
            {
              self->priv->list[dest++] = vala_tim_sort_slice_pop_first (right);
              if (right->length == 0)
                goto last;
              ++n_right;
              n_left = 0;
            }
          else
            {
              self->priv->list[dest++] = vala_tim_sort_slice_pop_first (left);
              if (left->length == 1)
                goto last;
              ++n_left;
              n_right = 0;
            }
        }

      while (n_left >= VALA_TIM_SORT_MINIMUM_GALLOP &&
             n_right >= VALA_TIM_SORT_MINIMUM_GALLOP)
        {
          if (minimum_gallop > 1)
            self->priv->minimum_gallop = --minimum_gallop;
          gpointer key = vala_tim_sort_slice_peek_first (right);
          n_left = vala_tim_sort_gallop_rightmost (self, key, left, 0);
          vala_tim_sort_slice_merge_in (left, self->priv->list, left->index,
                                        dest, n_left);
          dest += n_left;
          vala_tim_sort_slice_shorten_start (left, n_left);
          if (left->length <= 1)
            goto last;
          self->priv->list[dest++] = vala_tim_sort_slice_pop_first (right);
          if (right->length == 0)
            goto last;
          key = vala_tim_sort_slice_peek_first (left);
          n_right = vala_tim_sort_gallop_leftmost (self, key, right, 0);
          vala_tim_sort_slice_merge_in (right, self->priv->list, right->index,
                                        dest, n_right);
          dest += n_right;
          vala_tim_sort_slice_shorten_start (right, n_right);
          if (right->length == 0)
            goto last;
          self->priv->list[dest++] = vala_tim_sort_slice_pop_first (left);
          if (left->length == 1)
            goto last;
        }
      self->priv->minimum_gallop = ++minimum_gallop;
    } while (left->length > 1 && right->length > 0);

 last:
  vala_tim_sort_slice_merge_in (right, self->priv->list, right->index, dest,
                                right->length);
  vala_tim_sort_slice_merge_in (left, self->priv->list, left->index,
                                dest + right->length, left->length);
  vala_tim_sort_slice_free (left);
  vala_tim_sort_slice_free (right);
  return;
}

static void
vala_tim_sort_merge_high (ValaTimSort *self, ValaTimSortSlice *left,
                          ValaTimSortSlice *right)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (left != NULL);
  g_return_if_fail (right != NULL);
  g_assert (left->length > 0 && right->length > 0);
  g_assert (left->index + left->length > 0 == right->index);
  gint minimum_gallop = self->priv->minimum_gallop;
  vala_tim_sort_slice_copy (left);
  gint dest = right->index + right->length;
  self->priv->list[dest--] = vala_tim_sort_slice_pop_last (left);
  if (left->length == 0 || right->length == 1)
    goto last;

  do
    {
      gint n_left = 0, n_right = 0;
      while (n_left <= minimum_gallop && n_right <= minimum_gallop)
        {
          if (vala_tim_sort_lower_than (self,
                                        vala_tim_sort_slice_peek_last (left),
                                        vala_tim_sort_slice_peek_last (right)))
            {
              self->priv->list[dest--] = vala_tim_sort_slice_pop_last (left);
              if (left->length == 0)
                goto last;
              ++n_left;
              n_right = 0;
            }
          else
            {
              self->priv->list[dest--] = vala_tim_sort_slice_pop_last (right);
              if (left->length == 1)
                goto last;
              ++n_right;
              n_left = 0;
            }
        }

      while (n_left >= VALA_TIM_SORT_MINIMUM_GALLOP &&
             n_right >= VALA_TIM_SORT_MINIMUM_GALLOP)
        {
          if (minimum_gallop > 1)
            self->priv->minimum_gallop = --minimum_gallop;
          gpointer key = vala_tim_sort_slice_peek_last (right);
          gint k = vala_tim_sort_gallop_rightmost (self, key, left,
                                                   left->length - 1);
          n_left = left->length - k;
          vala_tim_sort_slice_merge_in (left, self->priv->list, left->index + k,
                                        dest - n_left, n_left);
          dest -= n_left;
          vala_tim_sort_slice_shorten_end (left, n_left);
          if (left->length == 0)
            goto last;
          self->priv->list[--dest] = vala_tim_sort_slice_pop_last (right);
          key = vala_tim_sort_slice_peek_last (left);
          if (right->length == 1)
            goto last;
          k = vala_tim_sort_gallop_leftmost (self, key, right, 0);
          n_right = right->length - k;
          vala_tim_sort_slice_merge_in (right, self->priv->list, right->index + k,
                                        dest - n_right, n_right);
          dest -= n_right;
          vala_tim_sort_slice_shorten_end (right, n_right);
          if (right->length <= 1)
            goto last;
          self->priv->list[dest--] = vala_tim_sort_slice_pop_last (left);
          if (left->length == 0)
            goto last;
        }
      self->priv->minimum_gallop = ++minimum_gallop;
    } while (left->length > 1 && right->length > 0);

 last:
  vala_tim_sort_slice_merge_in_reversed (left, self->priv->list, left->index,
                                         dest - left->length, left->length);
  vala_tim_sort_slice_merge_in_reversed (right, self->priv->list, right->index,
                                         dest - left->length - right->length,
                                         right->length);
  vala_tim_sort_slice_free (left);
  vala_tim_sort_slice_free (right);
  return;
}

G_GNUC_INTERNAL ValaTimSort *
vala_tim_sort_construct (GType object_type, GType g_type,
                         GBoxedCopyFunc g_dup_func,
                         GDestroyNotify g_destroy_func)
{
  ValaTimSort *self = (ValaTimSort *)g_type_create_instance (object_type);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  return self;
}

G_GNUC_INTERNAL ValaTimSort *
vala_tim_sort_new (GType g_type, GBoxedCopyFunc g_dup_func,
                   GDestroyNotify g_destroy_func)
{
  return vala_tim_sort_construct (VALA_TYPE_TIM_SORT, g_type, g_dup_func,
                                  g_destroy_func);
}

static ValaTimSortSlice *
vala_tim_sort_slice_new (void **list, gint index, gint length)
{
  ValaTimSortSlice *self = g_slice_new0 (ValaTimSortSlice);
  vala_tim_sort_slice_instance_init (self);
  self->list = list;
  self->index = index;
  self->length = length;
  return self;
}

static void
vala_tim_sort_slice_copy (ValaTimSortSlice *self)
{
  g_return_if_fail (self != NULL);
  gsize size = (gsize)(sizeof (gpointer) * self->length);
  self->new_list = g_malloc (size);
  memcpy (self->new_list, self->list[self->index], size);
  self->list = self->new_list;
  self->index = 0;
}

static inline void
vala_tim_sort_slice_merge_in (ValaTimSortSlice *self, void **dest_array,
                              gint index, gint dest_index, gint count)
{
  g_return_if_fail (self != NULL);
  memmove (&dest_array[dest_index], self->list + index,
           (gsize)(sizeof (gpointer) * count));
}

static inline void
vala_tim_sort_slice_merge_in_reversed (ValaTimSortSlice *self,
                                       void **dest_array, gint index,
                                       gint dest_index, gint count)
{
  g_return_if_fail (self != NULL);
  memmove (&dest_array[dest_index], self->list + index,
           (gsize)(sizeof (gpointer) * count));
}

static inline void
vala_tim_sort_slice_shorten_start (ValaTimSortSlice *self, gint n)
{
  g_return_if_fail (self != NULL);
  self->index += n;
  self->length -= n;
}

static inline void
vala_tim_sort_slice_shorten_end (ValaTimSortSlice *self, gint n)
{
  g_return_if_fail (self != NULL);
  self->length -= n;
}

static inline void *
vala_tim_sort_slice_pop_first (ValaTimSortSlice *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  self->length -= 1;
  return self->list[self->index++];
}

static inline void *
vala_tim_sort_slice_pop_last (ValaTimSortSlice *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  self->length -= 1;
  return self->list[self->index + self->length];
}

static inline void *
vala_tim_sort_slice_peek_first (ValaTimSortSlice *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->list[self->index];
}

static inline void *
vala_tim_sort_slice_peek_last (ValaTimSortSlice *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->list[self->index + self->length - 1];
}

static void
vala_tim_sort_slice_reverse (ValaTimSortSlice *self)
{
  g_return_if_fail (self != NULL);
  for (gint low = 0, high = (self->index + self->length) - 1;
       low < high;
       ++low, --high)
    vala_tim_sort_slice_swap (self, low, high);
}

static inline void
vala_tim_sort_slice_swap (ValaTimSortSlice *self, gint i, gint j)
{
  g_return_if_fail (self != NULL);
  void *temp = self->list[i];
  self->list[i] = self->list[j];
  self->list[j] = temp;
}

static void
vala_tim_sort_slice_instance_init (ValaTimSortSlice *self)
{
}

static void
vala_tim_sort_slice_free (ValaTimSortSlice *self)
{
  if (self->new_list)
    g_free (self->new_list);
  g_slice_free (ValaTimSortSlice, self);
}

static void
vala_value_tim_sort_init (GValue *value)
{
  value->data[0].v_pointer = NULL;
}

static void
vala_value_tim_sort_free_value (GValue *value)
{
  if (value->data[0].v_pointer)
    {
      vala_tim_sort_unref (value->data[0].v_pointer);
    }
}

static void
vala_value_tim_sort_copy_value (const GValue *src_value, GValue *dest_value)
{
  if (src_value->data[0].v_pointer)
    {
      dest_value->data[0].v_pointer =
        vala_tim_sort_ref (src_value->data[0].v_pointer);
    }
  else
    {
      dest_value->data[0].v_pointer = NULL;
    }
}

static gpointer
vala_value_tim_sort_peek_pointer (const GValue *value)
{
  return value->data[0].v_pointer;
}

static gchar *
vala_value_tim_sort_collect_value (GValue *value, guint n_collect_values,
                                   GTypeCValue *collect_values,
                                   guint collect_flags)
{
  if (collect_values[0].v_pointer)
    {
      ValaTimSort *object;
      object = collect_values[0].v_pointer;
      if (object->parent_instance.g_class == NULL)
        {
          return g_strconcat ("invalid unclassed object pointer for "
                              "value type `", G_VALUE_TYPE_NAME (value), "'",
                              NULL);
        }
      else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object),
                                         G_VALUE_TYPE (value)))
        {
          return g_strconcat ("invalid object type `",
                              g_type_name (G_TYPE_FROM_INSTANCE (object)),
                              "' for value type `", G_VALUE_TYPE_NAME (value),
                              "'", NULL);
        }
      value->data[0].v_pointer = vala_tim_sort_ref (object);
    }
  else
    {
      value->data[0].v_pointer = NULL;
    }
  return NULL;
}

static gchar *
vala_value_tim_sort_lcopy_value (const GValue *value, guint n_collect_values,
                                 GTypeCValue *collect_values,
                                 guint collect_flags)
{
  ValaTimSort **object_p;
  object_p = collect_values[0].v_pointer;
  if (!object_p)
    {
      return g_strdup_printf ("value location for `%s' passed as NULL",
                              G_VALUE_TYPE_NAME (value));
    }
  if (!value->data[0].v_pointer)
    {
      *object_p = NULL;
    }
  else if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
    {
      *object_p = value->data[0].v_pointer;
    }
  else
    {
      *object_p = vala_tim_sort_ref (value->data[0].v_pointer);
    }
  return NULL;
}


static void
vala_tim_sort_class_init (ValaTimSortClass *klass, gpointer klass_data)
{
  vala_tim_sort_parent_class = g_type_class_peek_parent (klass);
  VALA_TIM_SORT_CLASS (klass)->finalize = vala_tim_sort_finalize;
  g_type_class_adjust_private_offset (klass, &vala_tim_sort_private_offset);
}

static void
vala_tim_sort_instance_init (ValaTimSort *self, gpointer klass)
{
  self->priv = vala_tim_sort_get_instance_private (self);
  self->ref_count = 1;
}

static void
vala_tim_sort_finalize (ValaTimSort *obj)
{
  ValaTimSort *self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_TIM_SORT,
                                                  ValaTimSort);
  g_signal_handlers_destroy (self);
  g_clear_pointer (&self->priv->list_collection, vala_iterable_unref);
  vala_array_free (g_steal_pointer (&self->priv->array),
                   self->priv->array_size,
                   (GDestroyNotify)self->priv->g_destroy_func);
  vala_array_free (g_steal_pointer (&self->priv->pending),
                   self->priv->n_pending,
                   (GDestroyNotify)vala_tim_sort_slice_free);
}

/**
 * A stable, adaptive, iterative mergesort that requires far fewer than n*lg(n)
 * comparisons when running on partially sorted arrays, while offering
 * performance comparable to a traditional mergesort when run on random arrays.
 * Like all proper mergesorts, this sort is stable and runs O(n*log(n)) time
 * (worst case). In the worst case, this sort requires temporary storage space
 * for n/2 object references; in the best case, it requires only a small
 * constant amount of space.
 *
 * This implementation was adapted from Tim Peters's list sort for Python,
 * which is described in detail here:
 * [[http://svn.python.org/projects/python/trunk/Objects/listsort.txt]]
 *
 * Tim's C code may be found here:
 * [[http://svn.python.org/projects/python/trunk/Objects/listobject.c]]
 *
 * The underlying techniques are described in this paper (and may have even
 * earlier origins):
 *
 *   "Optimistic Sorting and Information Theoretic Complexity"
 *   Peter McIlroy
 *   SODA (Fourth Annual ACM-SIAM Symposium on Discrete Algorithms), pp
 *   467-474, Austin, Texas, 25-27 January 1993.
 */
static GType
vala_tim_sort_get_type_once (void)
{
  static const GTypeValueTable g_define_type_value_table =
    {
      vala_value_tim_sort_init,
      vala_value_tim_sort_free_value,
      vala_value_tim_sort_copy_value,
      vala_value_tim_sort_peek_pointer,
      "p",
      vala_value_tim_sort_collect_value,
      "p",
      vala_value_tim_sort_lcopy_value
    };
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaTimSortClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_tim_sort_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaTimSort),
      0,
      (GInstanceInitFunc)vala_tim_sort_instance_init,
      &g_define_type_value_table
    };
  static const GTypeFundamentalInfo g_define_type_fundamental_info =
    { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE
       | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
  GType vala_tim_sort_type_id;
  vala_tim_sort_type_id =
    g_type_register_fundamental (g_type_fundamental_next (), "ValaTimSort",
                                 &g_define_type_info,
                                 &g_define_type_fundamental_info,
                                 0);
  vala_tim_sort_private_offset =
    g_type_add_instance_private (vala_tim_sort_type_id,
                                 sizeof (ValaTimSortPrivate));
  return vala_tim_sort_type_id;
}

G_GNUC_INTERNAL GType
vala_tim_sort_get_type (void)
{
  static volatile gsize vala_tim_sort_type_id__volatile = 0;
  if (g_once_init_enter (&vala_tim_sort_type_id__volatile))
    {
      GType vala_tim_sort_type_id;
      vala_tim_sort_type_id = vala_tim_sort_get_type_once ();
      g_once_init_leave (&vala_tim_sort_type_id__volatile,
                         vala_tim_sort_type_id);
    }
  return vala_tim_sort_type_id__volatile;
}

G_GNUC_INTERNAL gpointer
vala_tim_sort_ref (gpointer instance)
{
  ValaTimSort *self = instance;
  g_atomic_int_inc (&self->ref_count);
  return instance;
}

G_GNUC_INTERNAL void
vala_tim_sort_unref (gpointer instance)
{
  ValaTimSort *self = instance;
  if (g_atomic_int_dec_and_test (&self->ref_count))
    {
      VALA_TIM_SORT_GET_CLASS (self)->finalize (self);
      g_type_free_instance ((GTypeInstance *)self);
    }
}
