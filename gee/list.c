/* list.c
 *
 * Copyright (C) 2007  Jürg Billeter
 * Copyright (C) 2022  Liliana Marie Prikler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.priker@gmail.com>
 */

#include "list.h"
#include <glib-object.h>
#include <glib.h>

struct _ValaListPrivate
{
  GType g_type;
  GBoxedCopyFunc g_dup_func;
  GDestroyNotify g_destroy_func;
};

static gint vala_list_private_offset;
static gpointer vala_list_parent_class = NULL;

static gpointer vala_list_real_get (ValaList *self, gint index);
static void vala_list_real_set (ValaList *self, gint index,
                                gconstpointer item);
static gint vala_list_real_index_of (ValaList *self, gconstpointer item);
static void vala_list_real_insert (ValaList *self, gint index,
                                   gconstpointer item);
static gpointer vala_list_real_remove_at (ValaList *self, gint index);
static gpointer vala_list_real_first (ValaList *self);
static gpointer vala_list_real_last (ValaList *self);
static void vala_list_real_insert_all (ValaList *self, gint index,
                                       ValaCollection *collection);
static void
vala_list_real_sort (ValaList *self, GCompareDataFunc compare_func,
                     gpointer compare_func_target,
                     GDestroyNotify compare_func_target_destroy_notify);
G_GNUC_INTERNAL void
vala_tim_sort_sort (GType g_type, GBoxedCopyFunc g_dup_func,
                    GDestroyNotify g_destroy_func, ValaList *list,
                    GCompareDataFunc compare, gpointer compare_target);
static GType vala_list_get_type_once (void);

static inline gpointer
vala_list_get_instance_private (ValaList *self)
{
  return G_STRUCT_MEMBER_P (self, vala_list_private_offset);
}

/**
 * Returns the item at the specified index in this list.
 *
 * @param index zero-based index of the item to be returned
 *
 * @return      the item at the specified index in the list
 */
static gpointer
vala_list_real_get (ValaList *self, gint index)
{
  g_critical ("Type `%s' does not implement abstract method `vala_list_get'",
              g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return NULL;
}

gpointer
vala_list_get (ValaList *self, gint index)
{
  ValaListClass *klass;
  g_return_val_if_fail (self != NULL, NULL);
  klass = VALA_LIST_GET_CLASS (self);
  if (klass->get)
    {
      return klass->get (self, index);
    }
  return NULL;
}

/**
 * Sets the item at the specified index in this list.
 *
 * @param index zero-based index of the item to be set
 */
static void
vala_list_real_set (ValaList *self, gint index, gconstpointer item)
{
  g_critical ("Type `%s' does not implement abstract method `vala_list_set'",
              g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return;
}

void
vala_list_set (ValaList *self, gint index, gconstpointer item)
{
  ValaListClass *klass;
  g_return_if_fail (self != NULL);
  klass = VALA_LIST_GET_CLASS (self);
  if (klass->set)
    {
      klass->set (self, index, item);
    }
}

/**
 * Returns the index of the first occurrence of the specified item in
 * this list.
 *
 * @return the index of the first occurrence of the specified item, or
 *         -1 if the item could not be found
 */
static gint
vala_list_real_index_of (ValaList *self, gconstpointer item)
{
  gint _tmp0_ = 0;
  g_critical (
      "Type `%s' does not implement abstract method `vala_list_index_of'",
      g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return _tmp0_;
}

gint
vala_list_index_of (ValaList *self, gconstpointer item)
{
  ValaListClass *klass;
  g_return_val_if_fail (self != NULL, 0);
  klass = VALA_LIST_GET_CLASS (self);
  if (klass->index_of)
    {
      return klass->index_of (self, item);
    }
  return -1;
}

/**
 * Inserts an item into this list at the specified position.
 *
 * @param index zero-based index at which item is inserted
 * @param item  item to insert into the list
 */
static void
vala_list_real_insert (ValaList *self, gint index, gconstpointer item)
{
  g_critical (
      "Type `%s' does not implement abstract method `vala_list_insert'",
      g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return;
}

void
vala_list_insert (ValaList *self, gint index, gconstpointer item)
{
  ValaListClass *klass;
  g_return_if_fail (self != NULL);
  klass = VALA_LIST_GET_CLASS (self);
  if (klass->insert)
    {
      klass->insert (self, index, item);
    }
}

/**
 * Removes the item at the specified index of this list.
 *
 * @param index zero-based index of the item to be removed
 *
 * @return      the removed element
 */
static gpointer
vala_list_real_remove_at (ValaList *self, gint index)
{
  g_critical (
      "Type `%s' does not implement abstract method `vala_list_remove_at'",
      g_type_name (G_TYPE_FROM_INSTANCE (self)));
  return NULL;
}

gpointer
vala_list_remove_at (ValaList *self, gint index)
{
  ValaListClass *klass;
  g_return_val_if_fail (self != NULL, NULL);
  klass = VALA_LIST_GET_CLASS (self);
  if (klass->remove_at)
    {
      return klass->remove_at (self, index);
    }
  return NULL;
}

/**
 * Returns the first item of the list. Fails if the list is empty.
 *
 * @return      first item in the list
 */
static gpointer
vala_list_real_first (ValaList *self)
{
  return vala_list_get (self, 0);
}

gpointer
vala_list_first (ValaList *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  ValaListClass *klass = VALA_LIST_GET_CLASS (self);
  if (klass->first)
    return klass->first (self);
  return NULL;
}

/**
 * Returns the last item of the list. Fails if the list is empty.
 *
 * @return      last item in the list
 */
static gpointer
vala_list_real_last (ValaList *self)
{
  return
    vala_list_get (self, vala_collection_get_size ((ValaCollection *)self) - 1);
}

gpointer
vala_list_last (ValaList *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  ValaListClass *klass = VALA_LIST_GET_CLASS (self);
  if (klass->last)
    return klass->last (self);
  return NULL;
}

/**
 * Inserts items into this list for the input collection at the
 * specified position.
 *
 * @param index zero-based index of the items to be inserted
 * @param collection collection of items to be inserted
 */
static void
vala_list_real_insert_all (ValaList *self, gint index,
                           ValaCollection *collection)
{
  g_return_if_fail (collection != NULL);

  ValaIterator *it = vala_iterable_iterator ((ValaIterable *)collection);
  while (vala_iterator_next (it))
    {
      gpointer item = vala_iterator_get (it);
      vala_list_insert (self, index++, item);
      if (item && self->priv->g_destroy_func)
        self->priv->g_destroy_func (item);
    }
  vala_iterable_unref (it);
}

void
vala_list_insert_all (ValaList *self, gint index, ValaCollection *collection)
{
  g_return_if_fail (self != NULL);
  ValaListClass *klass = VALA_LIST_GET_CLASS (self);
  if (klass->insert_all)
    klass->insert_all (self, index, collection);
}

/**
 * Sorts items by comparing with the specified compare function.
 *
 * @param compare_func compare function to use to compare items
 */
static void
vala_list_real_sort (ValaList *self, GCompareDataFunc compare_func,
                     gpointer compare_func_target,
                     GDestroyNotify compare_func_target_destroy_notify)
{
  vala_tim_sort_sort (self->priv->g_type,
                      (GBoxedCopyFunc)self->priv->g_dup_func,
                      (GDestroyNotify)self->priv->g_destroy_func, self,
                      compare_func, compare_func_target);

  if (compare_func_target_destroy_notify)
    compare_func_target_destroy_notify (compare_func_target);
  compare_func = NULL;
  compare_func_target = NULL;
  compare_func_target_destroy_notify = NULL;
}

void
vala_list_sort (ValaList *self, GCompareDataFunc compare_func,
                gpointer compare_func_target,
                GDestroyNotify compare_func_target_destroy_notify)
{
  g_return_if_fail (self != NULL);
  ValaListClass *klass = VALA_LIST_GET_CLASS (self);
  if (klass->sort)
    klass->sort (self, compare_func, compare_func_target,
                compare_func_target_destroy_notify);
}

ValaList *
vala_list_construct (GType object_type, GType g_type,
                     GBoxedCopyFunc g_dup_func, GDestroyNotify g_destroy_func)
{
  ValaList *self = NULL;
  self = (ValaList *)vala_collection_construct (object_type, g_type,
                                                (GBoxedCopyFunc)g_dup_func,
                                                (GDestroyNotify)g_destroy_func);
  self->priv->g_type = g_type;
  self->priv->g_dup_func = g_dup_func;
  self->priv->g_destroy_func = g_destroy_func;
  return self;
}

static void
vala_list_class_init (ValaListClass *klass, gpointer klass_data)
{
  vala_list_parent_class = g_type_class_peek_parent (klass);
  g_type_class_adjust_private_offset (klass, &vala_list_private_offset);
  VALA_LIST_CLASS (klass)->get = vala_list_real_get;
  VALA_LIST_CLASS (klass)->set = vala_list_real_set;
  VALA_LIST_CLASS (klass)->index_of = vala_list_real_index_of;
  VALA_LIST_CLASS (klass)->insert = vala_list_real_insert;
  VALA_LIST_CLASS (klass)->remove_at = vala_list_real_remove_at;
  VALA_LIST_CLASS (klass)->first = vala_list_real_first;
  VALA_LIST_CLASS (klass)->last = vala_list_real_last;
  VALA_LIST_CLASS (klass)->insert_all = vala_list_real_insert_all;
  VALA_LIST_CLASS (klass)->sort = vala_list_real_sort;
}

static void
vala_list_instance_init (ValaList *self, gpointer klass)
{
  self->priv = vala_list_get_instance_private (self);
}

/**
 * Represents a collection of items in a well-defined order.
 */
static GType
vala_list_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaListClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_list_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaList),
      0,
      (GInstanceInitFunc)vala_list_instance_init,
      NULL
    };
  GType vala_list_type_id;
  vala_list_type_id = g_type_register_static (VALA_TYPE_COLLECTION, "ValaList",
                                              &g_define_type_info,
                                              G_TYPE_FLAG_ABSTRACT);
  vala_list_private_offset =
    g_type_add_instance_private (vala_list_type_id, sizeof (ValaListPrivate));
  return vala_list_type_id;
}

GType
vala_list_get_type (void)
{
  static volatile gsize vala_list_type_id__volatile = 0;
  if (g_once_init_enter (&vala_list_type_id__volatile))
    {
      GType vala_list_type_id;
      vala_list_type_id = vala_list_get_type_once ();
      g_once_init_leave (&vala_list_type_id__volatile, vala_list_type_id);
    }
  return vala_list_type_id__volatile;
}
