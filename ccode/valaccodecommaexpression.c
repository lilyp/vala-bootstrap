/* valaccodecommaexpression.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006-2010  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib-object.h>
#include <glib.h>
#include <valagee.h>

struct _ValaCCodeCommaExpressionPrivate
{
  ValaList *inner;
};

static gint vala_ccode_comma_expression_private_offset;
static gpointer vala_ccode_comma_expression_parent_class = NULL;

static inline gpointer
vala_ccode_comma_expression_get_instance_private (ValaCCodeCommaExpression *self)
{
  return G_STRUCT_MEMBER_P (self, vala_ccode_comma_expression_private_offset);
}

/**
 * Appends the specified expression to the expression list.
 *
 * @param expr a C code expression
 */
void
vala_ccode_comma_expression_append_expression (ValaCCodeCommaExpression *self,
                                               ValaCCodeExpression *expr)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (expr != NULL);
  vala_collection_add ((ValaCollection *)self->priv->inner, expr);
}

void
vala_ccode_comma_expression_set_expression (ValaCCodeCommaExpression *self,
                                            gint index,
                                            ValaCCodeExpression *expr)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (expr != NULL);
  vala_list_set (self->priv->inner, index, expr);
}

ValaList *
vala_ccode_comma_expression_get_inner (ValaCCodeCommaExpression *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->priv->inner;
}

static void
vala_ccode_comma_expression_real_write (ValaCCodeNode *base,
                                        ValaCCodeWriter *writer)
{
  ValaCCodeCommaExpression *self = (ValaCCodeCommaExpression *)base;
  g_return_if_fail (writer != NULL);
  gboolean first = TRUE;
  vala_ccode_writer_write_string (writer, "(");
  for (gsize i = 0; i < vala_collection_get_size (self->priv->inner); ++i)
    {
      if (first)
        first = FALSE;
      else
        vala_ccode_writer_write_string (writer, ", ");
      ValaCCodeExpression *expr = vala_list_get (self->priv->inner, i);
      vala_ccode_node_write (expr, writer);
      vala_ccode_node_unref (expr);
    }
  vala_ccode_writer_write_string (writer, ")");
}

ValaCCodeCommaExpression *
vala_ccode_comma_expression_construct (GType object_type)
{
  return
    (ValaCCodeCommaExpression *)
    vala_ccode_expression_construct (object_type);
}

ValaCCodeCommaExpression *
vala_ccode_comma_expression_new (void)
{
  return
    vala_ccode_comma_expression_construct (VALA_TYPE_CCODE_COMMA_EXPRESSION);
}

static void
vala_ccode_comma_expression_finalize (ValaCCodeNode *obj)
{
  ValaCCodeCommaExpression *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_CCODE_COMMA_EXPRESSION,
                                     ValaCCodeCommaExpression);
  g_clear_pointer (&self->priv->inner, vala_iterable_unref);
  VALA_CCODE_NODE_CLASS (vala_ccode_comma_expression_parent_class)
    ->finalize (obj);
}

static void
vala_ccode_comma_expression_class_init (ValaCCodeCommaExpressionClass *klass,
                                        gpointer klass_data)
{
  vala_ccode_comma_expression_parent_class = g_type_class_peek_parent (klass);
  ((ValaCCodeNodeClass *)klass)->finalize =
    vala_ccode_comma_expression_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_ccode_comma_expression_private_offset);
  ((ValaCCodeNodeClass *)klass)->write =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))
    vala_ccode_comma_expression_real_write;
}

static void
vala_ccode_comma_expression_instance_init (ValaCCodeCommaExpression *self,
                                           gpointer klass)
{
  self->priv = vala_ccode_comma_expression_get_instance_private (self);
  self->priv->inner =
    vala_array_list_new (VALA_TYPE_CCODE_EXPRESSION,
                         (GBoxedCopyFunc)vala_ccode_node_ref,
                         (GDestroyNotify)vala_ccode_node_unref,
                         g_direct_equal);
}

/**
 * Represents a comma separated expression list in the C code.
 */
static GType
vala_ccode_comma_expression_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeCommaExpressionClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_comma_expression_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeCommaExpression),
      0,
      (GInstanceInitFunc)vala_ccode_comma_expression_instance_init,
      NULL
    };
  GType vala_ccode_comma_expression_type_id =
    g_type_register_static (VALA_TYPE_CCODE_EXPRESSION,
                            "ValaCCodeCommaExpression",
                            &g_define_type_info, 0);
  vala_ccode_comma_expression_private_offset =
    g_type_add_instance_private (vala_ccode_comma_expression_type_id,
                                 sizeof (ValaCCodeCommaExpressionPrivate));
  return vala_ccode_comma_expression_type_id;
}

GType
vala_ccode_comma_expression_get_type (void)
{
  static volatile gsize vala_ccode_comma_expression_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_comma_expression_type_id__volatile))
    {
      GType vala_ccode_comma_expression_type_id;
      vala_ccode_comma_expression_type_id =
        vala_ccode_comma_expression_get_type_once ();
      g_once_init_leave (&vala_ccode_comma_expression_type_id__volatile,
                         vala_ccode_comma_expression_type_id);
    }
  return vala_ccode_comma_expression_type_id__volatile;
}
