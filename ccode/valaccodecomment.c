/* valaccodecomment.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib.h>
#include <stdlib.h>
#include <string.h>

struct _ValaCCodeCommentPrivate
{
  gchar *_text;
};

static gint vala_ccode_comment_private_offset;
static gpointer vala_ccode_comment_parent_class = NULL;

static void
vala_ccode_comment_finalize (ValaCCodeNode *obj)
{
  ValaCCodeComment *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_CCODE_COMMENT,
                                     ValaCCodeComment);
  g_clear_pointer (&self->priv->_text, g_free);
  VALA_CCODE_NODE_CLASS (vala_ccode_comment_parent_class)->finalize (obj);
}

static inline gpointer
vala_ccode_comment_get_instance_private (ValaCCodeComment *self)
{
  return G_STRUCT_MEMBER_P (self, vala_ccode_comment_private_offset);
}

const gchar *
vala_ccode_comment_get_text (ValaCCodeComment *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->priv->_text;
}

void
vala_ccode_comment_set_text (ValaCCodeComment *self, const gchar *value)
{
  g_return_if_fail (self != NULL);
  g_clear_pointer (&self->priv->_text, g_free);
  self->priv->_text = g_strdup (value);
}

ValaCCodeComment *
vala_ccode_comment_construct (GType object_type, const gchar *_text)
{
  ValaCCodeComment *self = NULL;
  g_return_val_if_fail (_text != NULL, NULL);
  self = (ValaCCodeComment *)vala_ccode_node_construct (object_type);
  vala_ccode_comment_set_text (self, _text);
  return self;
}

ValaCCodeComment *
vala_ccode_comment_new (const gchar *_text)
{
  return vala_ccode_comment_construct (VALA_TYPE_CCODE_COMMENT, _text);
}

static void
vala_ccode_comment_real_write (ValaCCodeNode *base, ValaCCodeWriter *writer)
{
  ValaCCodeComment *self = (ValaCCodeComment *)base;
  g_return_if_fail (writer != NULL);
  vala_ccode_writer_write_comment (writer, self->priv->_text);
}

static void
vala_ccode_comment_class_init (ValaCCodeCommentClass *klass,
                               gpointer klass_data)
{
  vala_ccode_comment_parent_class = g_type_class_peek_parent (klass);
  ((ValaCCodeNodeClass *)klass)->finalize = vala_ccode_comment_finalize;
  g_type_class_adjust_private_offset (klass, &vala_ccode_comment_private_offset);
  ((ValaCCodeNodeClass *)klass)->write =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))vala_ccode_comment_real_write;
}

static void
vala_ccode_comment_instance_init (ValaCCodeComment *self, gpointer klass)
{
  self->priv = vala_ccode_comment_get_instance_private (self);
}

/**
 * Represents a comment in the C code.
 */
static GType
vala_ccode_comment_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeCommentClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_comment_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeComment),
      0,
      (GInstanceInitFunc)vala_ccode_comment_instance_init,
      NULL
    };
  GType vala_ccode_comment_type_id;
  vala_ccode_comment_type_id = g_type_register_static (VALA_TYPE_CCODE_NODE,
                                                       "ValaCCodeComment",
                                                       &g_define_type_info, 0);
  vala_ccode_comment_private_offset =
    g_type_add_instance_private (vala_ccode_comment_type_id,
                                 sizeof (ValaCCodeCommentPrivate));
  return vala_ccode_comment_type_id;
}

GType
vala_ccode_comment_get_type (void)
{
  static volatile gsize vala_ccode_comment_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_comment_type_id__volatile))
    {
      GType vala_ccode_comment_type_id;
      vala_ccode_comment_type_id = vala_ccode_comment_get_type_once ();
      g_once_init_leave (&vala_ccode_comment_type_id__volatile,
                         vala_ccode_comment_type_id);
    }
  return vala_ccode_comment_type_id__volatile;
}
