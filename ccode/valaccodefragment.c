/* valaccodefragment.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006-2007  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib-object.h>
#include <glib.h>
#include <valagee.h>

struct _ValaCCodeFragmentPrivate
{
  ValaList *children;
};

static gint vala_ccode_fragment_private_offset;
static gpointer vala_ccode_fragment_parent_class = NULL;

static void
vala_ccode_fragment_finalize (ValaCCodeNode *obj)
{
  ValaCCodeFragment *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_CCODE_FRAGMENT,
                                     ValaCCodeFragment);
  g_clear_pointer (&self->priv->children, vala_iterable_unref);
  VALA_CCODE_NODE_CLASS (vala_ccode_fragment_parent_class)->finalize (obj);
}

static inline gpointer
vala_ccode_fragment_get_instance_private (ValaCCodeFragment *self)
{
  return G_STRUCT_MEMBER_P (self, vala_ccode_fragment_private_offset);
}

/**
 * Appends the specified code node to this code fragment.
 *
 * @param node a C code node
 */
void
vala_ccode_fragment_append (ValaCCodeFragment *self, ValaCCodeNode *node)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (node != NULL);
  vala_collection_add ((ValaCollection *)self->priv->children, node);
}

/**
 * Returns the list of children.
 *
 * @return children list
 */
ValaList *
vala_ccode_fragment_get_children (ValaCCodeFragment *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->priv->children;
}

static void
vala_ccode_fragment_real_write (ValaCCodeNode *base, ValaCCodeWriter *writer)
{
  g_return_if_fail (writer != NULL);
  ValaCCodeFragment *self = (ValaCCodeFragment *)base;
  for (gint i = 0; i < vala_collection_get_size (self->priv->children); ++i)
    {
      ValaCCodeNode *node = vala_list_get (self->priv->children, i);
      vala_ccode_node_write (node, writer);
      vala_ccode_node_unref (node);
    }
}

static void
vala_ccode_fragment_real_write_declaration (ValaCCodeNode *base,
                                            ValaCCodeWriter *writer)
{
  g_return_if_fail (writer != NULL);
  ValaCCodeFragment *self = (ValaCCodeFragment *)base;
  for (gint i = 0; i < vala_collection_get_size (self->priv->children); ++i)
    {
      ValaCCodeNode *node = vala_list_get (self->priv->children, i);
      vala_ccode_node_write_declaration (node, writer);
      vala_ccode_node_unref (node);
    }
}

static void
vala_ccode_fragment_real_write_combined (ValaCCodeNode *base,
                                         ValaCCodeWriter *writer)
{
  g_return_if_fail (writer != NULL);
  ValaCCodeFragment *self = (ValaCCodeFragment *)base;
  for (gint i = 0; i < vala_collection_get_size (self->priv->children); ++i)
    {
      ValaCCodeNode *node = vala_list_get (self->priv->children, i);
      vala_ccode_node_write_combined (node, writer);
      vala_ccode_node_unref (node);
    }
}

ValaCCodeFragment *
vala_ccode_fragment_construct (GType object_type)
{
  ValaCCodeFragment *self = NULL;
  self = (ValaCCodeFragment *)vala_ccode_node_construct (object_type);
  return self;
}

ValaCCodeFragment *
vala_ccode_fragment_new (void)
{
  return vala_ccode_fragment_construct (VALA_TYPE_CCODE_FRAGMENT);
}

static void
vala_ccode_fragment_class_init (ValaCCodeFragmentClass *klass,
                                gpointer klass_data)
{
  vala_ccode_fragment_parent_class = g_type_class_peek_parent (klass);
  ((ValaCCodeNodeClass *)klass)->finalize = vala_ccode_fragment_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_ccode_fragment_private_offset);
  ((ValaCCodeNodeClass *)klass)->write =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))vala_ccode_fragment_real_write;
  ((ValaCCodeNodeClass *)klass)->write_declaration =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))
    vala_ccode_fragment_real_write_declaration;
  ((ValaCCodeNodeClass *)klass)->write_combined =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))
    vala_ccode_fragment_real_write_combined;
}

static void
vala_ccode_fragment_instance_init (ValaCCodeFragment *self, gpointer klass)
{
  self->priv = vala_ccode_fragment_get_instance_private (self);
  self->priv->children = (ValaList *)
    vala_array_list_new (VALA_TYPE_CCODE_NODE,
                         (GBoxedCopyFunc)vala_ccode_node_ref,
                         (GDestroyNotify)vala_ccode_node_unref, g_direct_equal);
}


/**
 * Represents a container for C code nodes.
 */
static GType
vala_ccode_fragment_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeFragmentClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_fragment_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeFragment),
      0,
      (GInstanceInitFunc)vala_ccode_fragment_instance_init,
      NULL
    };
  GType vala_ccode_fragment_type_id =
    g_type_register_static (VALA_TYPE_CCODE_NODE, "ValaCCodeFragment",
                            &g_define_type_info, 0);
  vala_ccode_fragment_private_offset =
    g_type_add_instance_private (vala_ccode_fragment_type_id,
                                 sizeof (ValaCCodeFragmentPrivate));
  return vala_ccode_fragment_type_id;
}

GType
vala_ccode_fragment_get_type (void)
{
  static volatile gsize vala_ccode_fragment_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_fragment_type_id__volatile))
    {
      GType vala_ccode_fragment_type_id;
      vala_ccode_fragment_type_id = vala_ccode_fragment_get_type_once ();
      g_once_init_leave (&vala_ccode_fragment_type_id__volatile,
                         vala_ccode_fragment_type_id);
    }
  return vala_ccode_fragment_type_id__volatile;
}
