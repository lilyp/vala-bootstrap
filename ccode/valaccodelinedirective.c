/* valaccodelinedirective.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib.h>
#include <stdlib.h>
#include <string.h>

#define _g_free0(var) (var = (g_free (var), NULL))

struct _ValaCCodeLineDirectivePrivate
{
  gchar *_filename;
  gint _line_number;
};

static gint vala_ccode_line_directive_private_offset;
static gpointer vala_ccode_line_directive_parent_class = NULL;

static void
vala_ccode_line_directive_finalize (ValaCCodeNode *obj)
{
  ValaCCodeLineDirective *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_CCODE_LINE_DIRECTIVE,
                                     ValaCCodeLineDirective);
  g_clear_pointer (&self->priv->_filename, g_free);
  VALA_CCODE_NODE_CLASS (vala_ccode_line_directive_parent_class)
    ->finalize (obj);
}

static inline gpointer
vala_ccode_line_directive_get_instance_private (ValaCCodeLineDirective *self)
{
  return G_STRUCT_MEMBER_P (self, vala_ccode_line_directive_private_offset);
}

const gchar *
vala_ccode_line_directive_get_filename (ValaCCodeLineDirective *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->priv->_filename;
}

void
vala_ccode_line_directive_set_filename (ValaCCodeLineDirective *self,
                                        const gchar *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->_filename)
    g_free (self->priv->_filename);
  self->priv->_filename = g_strdup (value);
}

gint
vala_ccode_line_directive_get_line_number (ValaCCodeLineDirective *self)
{
  g_return_val_if_fail (self != NULL, 0);
  return self->priv->_line_number;
}

void
vala_ccode_line_directive_set_line_number (ValaCCodeLineDirective *self,
                                           gint value)
{
  g_return_if_fail (self != NULL);
  self->priv->_line_number = value;
}

ValaCCodeLineDirective *
vala_ccode_line_directive_construct (GType object_type, const gchar *filename,
                                     gint line)
{
  ValaCCodeLineDirective *self = NULL;
  g_return_val_if_fail (filename != NULL, NULL);
  self = (ValaCCodeLineDirective *)vala_ccode_node_construct (object_type);
  vala_ccode_line_directive_set_filename (self, filename);
  vala_ccode_line_directive_set_line_number (self, line);
  return self;
}

ValaCCodeLineDirective *
vala_ccode_line_directive_new (const gchar *filename, gint line)
{
  return vala_ccode_line_directive_construct (VALA_TYPE_CCODE_LINE_DIRECTIVE,
                                              filename, line);
}

static void
vala_ccode_line_directive_real_write (ValaCCodeNode *base,
                                      ValaCCodeWriter *writer)
{
  ValaCCodeLineDirective *self = (ValaCCodeLineDirective *)base;
  g_return_if_fail (writer != NULL);
  if (!vala_ccode_writer_get_bol (writer))
    vala_ccode_writer_write_newline (writer);
  gchar *directive = g_strdup_printf ("#line %d \"%s\"",
                                      self->priv->_line_number,
                                      self->priv->_filename);
  vala_ccode_writer_write_string (writer, directive);
  g_free (directive);
  vala_ccode_writer_write_newline (writer);
}

static void
vala_ccode_line_directive_class_init (ValaCCodeLineDirectiveClass *klass,
                                      gpointer klass_data)
{
  vala_ccode_line_directive_parent_class = g_type_class_peek_parent (klass);
  ((ValaCCodeNodeClass *)klass)->finalize = vala_ccode_line_directive_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_ccode_line_directive_private_offset);
  ((ValaCCodeNodeClass *)klass)->write =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))
    vala_ccode_line_directive_real_write;
}

static void
vala_ccode_line_directive_instance_init (ValaCCodeLineDirective *self,
                                         gpointer klass)
{
  self->priv = vala_ccode_line_directive_get_instance_private (self);
}

/**
 * Represents a line directive in the C code.
 */
static GType
vala_ccode_line_directive_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeLineDirectiveClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_line_directive_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeLineDirective),
      0,
      (GInstanceInitFunc)vala_ccode_line_directive_instance_init,
      NULL
    };
  GType vala_ccode_line_directive_type_id =
    g_type_register_static (VALA_TYPE_CCODE_NODE, "ValaCCodeLineDirective",
                            &g_define_type_info, 0);
  vala_ccode_line_directive_private_offset =
    g_type_add_instance_private (vala_ccode_line_directive_type_id,
                                 sizeof (ValaCCodeLineDirectivePrivate));
  return vala_ccode_line_directive_type_id;
}

GType
vala_ccode_line_directive_get_type (void)
{
  static volatile gsize vala_ccode_line_directive_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_line_directive_type_id__volatile))
    {
      GType vala_ccode_line_directive_type_id;
      vala_ccode_line_directive_type_id
          = vala_ccode_line_directive_get_type_once ();
      g_once_init_leave (&vala_ccode_line_directive_type_id__volatile,
                         vala_ccode_line_directive_type_id);
    }
  return vala_ccode_line_directive_type_id__volatile;
}
