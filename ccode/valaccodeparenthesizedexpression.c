/* valaccodeparenthesizedexpression.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib.h>

struct _ValaCCodeParenthesizedExpressionPrivate
{
  ValaCCodeExpression *_inner;
};

static gint vala_ccode_parenthesized_expression_private_offset;
static gpointer vala_ccode_parenthesized_expression_parent_class = NULL;

static inline gpointer
vala_ccode_parenthesized_expression_get_instance_private (
    ValaCCodeParenthesizedExpression *self)
{
  return G_STRUCT_MEMBER_P (self,
                            vala_ccode_parenthesized_expression_private_offset);
}

ValaCCodeExpression *
vala_ccode_parenthesized_expression_get_inner (
    ValaCCodeParenthesizedExpression *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->priv->_inner;
}

void
vala_ccode_parenthesized_expression_set_inner (
    ValaCCodeParenthesizedExpression *self, ValaCCodeExpression *value)
{
  g_return_if_fail (self != NULL);
  g_clear_pointer (&self->priv->_inner, vala_ccode_node_unref);
  self->priv->_inner = value ? vala_ccode_node_ref (value) : value;
}

ValaCCodeParenthesizedExpression *
vala_ccode_parenthesized_expression_construct (GType object_type,
                                               ValaCCodeExpression *expr)
{
  g_return_val_if_fail (expr != NULL, NULL);
  ValaCCodeParenthesizedExpression *self =
    (ValaCCodeParenthesizedExpression *)
    vala_ccode_expression_construct (object_type);
  vala_ccode_parenthesized_expression_set_inner (self, expr);
  return self;
}

ValaCCodeParenthesizedExpression *
vala_ccode_parenthesized_expression_new (ValaCCodeExpression *expr)
{
  return vala_ccode_parenthesized_expression_construct (VALA_TYPE_CCODE_PARENTHESIZED_EXPRESSION,
                                                        expr);
}

static void
vala_ccode_parenthesized_expression_real_write (ValaCCodeNode *base,
                                                ValaCCodeWriter *writer)
{
  ValaCCodeParenthesizedExpression *self = (ValaCCodeParenthesizedExpression *)base;
  g_return_if_fail (writer != NULL);
  vala_ccode_writer_write_string (writer, "(");
  vala_ccode_node_write ((ValaCCodeNode *)self->priv->_inner, writer);
  vala_ccode_writer_write_string (writer, ")");
}

static void
vala_ccode_parenthesized_expression_finalize (ValaCCodeNode *obj)
{
  ValaCCodeParenthesizedExpression *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj,
                                     VALA_TYPE_CCODE_PARENTHESIZED_EXPRESSION,
                                     ValaCCodeParenthesizedExpression);
  g_clear_pointer (&self->priv->_inner, vala_ccode_node_unref);
  VALA_CCODE_NODE_CLASS (vala_ccode_parenthesized_expression_parent_class)
    ->finalize (obj);
}

static void
vala_ccode_parenthesized_expression_class_init (ValaCCodeParenthesizedExpressionClass *klass,
                                                gpointer klass_data)
{
  vala_ccode_parenthesized_expression_parent_class =
    g_type_class_peek_parent (klass);
  ((ValaCCodeNodeClass *)klass)->finalize =
    vala_ccode_parenthesized_expression_finalize;
  g_type_class_adjust_private_offset (klass,
                                      &vala_ccode_parenthesized_expression_private_offset);
  ((ValaCCodeNodeClass *)klass)->write =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))
    vala_ccode_parenthesized_expression_real_write;
}

static void
vala_ccode_parenthesized_expression_instance_init (
    ValaCCodeParenthesizedExpression *self, gpointer klass)
{
  self->priv = vala_ccode_parenthesized_expression_get_instance_private (self);
}

/**
 * Represents a parenthesized expression in the C code.
 */
static GType
vala_ccode_parenthesized_expression_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeParenthesizedExpressionClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_parenthesized_expression_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeParenthesizedExpression),
      0,
      (GInstanceInitFunc)vala_ccode_parenthesized_expression_instance_init,
      NULL
    };
  GType vala_ccode_parenthesized_expression_type_id =
    g_type_register_static (VALA_TYPE_CCODE_EXPRESSION,
                            "ValaCCodeParenthesizedExpression",
                            &g_define_type_info, 0);
  vala_ccode_parenthesized_expression_private_offset =
    g_type_add_instance_private (vala_ccode_parenthesized_expression_type_id,
                                 sizeof (ValaCCodeParenthesizedExpressionPrivate));
  return vala_ccode_parenthesized_expression_type_id;
}

GType
vala_ccode_parenthesized_expression_get_type (void)
{
  static volatile gsize vala_ccode_parenthesized_expression_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_parenthesized_expression_type_id__volatile))
    {
      GType vala_ccode_parenthesized_expression_type_id;
      vala_ccode_parenthesized_expression_type_id =
        vala_ccode_parenthesized_expression_get_type_once ();
      g_once_init_leave (&vala_ccode_parenthesized_expression_type_id__volatile,
                         vala_ccode_parenthesized_expression_type_id);
    }
  return vala_ccode_parenthesized_expression_type_id__volatile;
}
