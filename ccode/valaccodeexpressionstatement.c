/* valaccodeexpressionstatement.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006-2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib-object.h>
#include <glib.h>
#include <valagee.h>

struct _ValaCCodeExpressionStatementPrivate
{
  ValaCCodeExpression *_expression;
};

static gint vala_ccode_expression_statement_private_offset;
static gpointer vala_ccode_expression_statement_parent_class = NULL;

static inline gpointer
vala_ccode_expression_statement_get_instance_private (ValaCCodeExpressionStatement *self)
{
  return G_STRUCT_MEMBER_P (self, vala_ccode_expression_statement_private_offset);
}

ValaCCodeExpression *
vala_ccode_expression_statement_get_expression (ValaCCodeExpressionStatement *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->priv->_expression;
}

void
vala_ccode_expression_statement_set_expression (ValaCCodeExpressionStatement *self,
                                                ValaCCodeExpression *value)
{
  g_return_if_fail (self != NULL);
  g_clear_pointer (&self->priv->_expression, vala_ccode_node_unref);
  self->priv->_expression = value ? vala_ccode_node_ref (value) : value;
}

ValaCCodeExpressionStatement *
vala_ccode_expression_statement_construct (GType object_type,
                                           ValaCCodeExpression *expr)
{
  g_return_val_if_fail (expr != NULL, NULL);
  ValaCCodeExpressionStatement *self =
    (ValaCCodeExpressionStatement *)vala_ccode_statement_construct (object_type);
  vala_ccode_expression_statement_set_expression (self, expr);
  return self;
}

ValaCCodeExpressionStatement *
vala_ccode_expression_statement_new (ValaCCodeExpression *expr)
{
  return vala_ccode_expression_statement_construct (VALA_TYPE_CCODE_EXPRESSION_STATEMENT,
                                                    expr);
}

static void
vala_ccode_expression_statement_write_expression (ValaCCodeExpressionStatement *self,
                                                  ValaCCodeWriter *writer,
                                                  ValaCCodeExpression *expr)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (writer != NULL);
  ValaCCodeLineDirective *directive =
    vala_ccode_node_get_line ((ValaCCodeNode *)self);
  vala_ccode_writer_write_indent (writer, directive);
  if (expr != NULL)
    vala_ccode_node_write ((ValaCCodeNode *)expr, writer);
  vala_ccode_writer_write_string (writer, ";");
  vala_ccode_writer_write_newline (writer);
}

static void
vala_ccode_expression_statement_real_write (ValaCCodeNode *base,
                                            ValaCCodeWriter *writer)
{
  ValaCCodeExpressionStatement *self = (ValaCCodeExpressionStatement *)base;
  g_return_if_fail (writer != NULL);
  g_assert (self->priv->_expression != NULL);
  if (VALA_IS_CCODE_COMMA_EXPRESSION (self->priv->_expression))
    {
      ValaCCodeCommaExpression *ccomma =
        vala_ccode_node_ref (self->priv->_expression);
      ValaList *inner =
        vala_ccode_comma_expression_get_inner (ccomma);
      for (gint i = 0; i < vala_collection_get_size (inner); ++i)
        {
          ValaCCodeExpression *expr =
            (ValaCCodeExpression *)vala_list_get (inner, i);
          vala_ccode_expression_statement_write_expression (self, writer, expr);
          vala_ccode_node_unref (expr);
        }
      vala_ccode_node_unref (ccomma);
    }
  else if (VALA_IS_CCODE_PARENTHESIZED_EXPRESSION (self->priv->_expression))
    {
      ValaCCodeParenthesizedExpression *cpar =
        vala_ccode_node_ref (self->priv->_expression);

      ValaCCodeExpression *inner =
        vala_ccode_parenthesized_expression_get_inner (cpar);
      vala_ccode_expression_statement_write_expression (self, writer, inner);

      vala_ccode_node_unref (cpar);
    }
  else
    vala_ccode_expression_statement_write_expression (self, writer,
                                                      self->priv->_expression);
}

static void
vala_ccode_expression_statement_finalize (ValaCCodeNode *obj)
{
  ValaCCodeExpressionStatement *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_CCODE_EXPRESSION_STATEMENT,
                                     ValaCCodeExpressionStatement);
  g_clear_pointer (&self->priv->_expression, vala_ccode_node_unref);
  VALA_CCODE_NODE_CLASS (vala_ccode_expression_statement_parent_class)
    ->finalize (obj);
}

static void
vala_ccode_expression_statement_class_init (
    ValaCCodeExpressionStatementClass *klass, gpointer klass_data)
{
  vala_ccode_expression_statement_parent_class =
    g_type_class_peek_parent (klass);
  ((ValaCCodeNodeClass *)klass)->finalize =
    vala_ccode_expression_statement_finalize;
  g_type_class_adjust_private_offset (
      klass, &vala_ccode_expression_statement_private_offset);
  ((ValaCCodeNodeClass *)klass)->write =
    (void (*) (ValaCCodeNode *, ValaCCodeWriter *))
    vala_ccode_expression_statement_real_write;
}

static void
vala_ccode_expression_statement_instance_init (
    ValaCCodeExpressionStatement *self, gpointer klass)
{
  self->priv = vala_ccode_expression_statement_get_instance_private (self);
}

/**
 * Represents a C code statement that evaluates a given expression.
 */
static GType
vala_ccode_expression_statement_get_type_once (void)
{
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeExpressionStatementClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_expression_statement_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeExpressionStatement),
      0,
      (GInstanceInitFunc)vala_ccode_expression_statement_instance_init,
      NULL
    };
  GType vala_ccode_expression_statement_type_id =
    g_type_register_static (VALA_TYPE_CCODE_STATEMENT,
                            "ValaCCodeExpressionStatement",
                            &g_define_type_info, 0);
  vala_ccode_expression_statement_private_offset =
    g_type_add_instance_private (vala_ccode_expression_statement_type_id,
                                 sizeof (ValaCCodeExpressionStatementPrivate));
  return vala_ccode_expression_statement_type_id;
}

GType
vala_ccode_expression_statement_get_type (void)
{
  static volatile gsize vala_ccode_expression_statement_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_expression_statement_type_id__volatile))
    {
      GType vala_ccode_expression_statement_type_id;
      vala_ccode_expression_statement_type_id
          = vala_ccode_expression_statement_get_type_once ();
      g_once_init_leave (&vala_ccode_expression_statement_type_id__volatile,
                         vala_ccode_expression_statement_type_id);
    }
  return vala_ccode_expression_statement_type_id__volatile;
}
