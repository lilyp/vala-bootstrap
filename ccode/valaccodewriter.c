/* valaccodewriter.c
 *
 * Copyright (C) 2023  Liliana Marie Prikler
 * Copyright (C) 2006-2009  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 *  Jürg Billeter <j@bitron.ch>
 */

#include "valaccode.h"
#include <glib.h>
#include <glib/gstdio.h>
#include <gobject/gvaluecollector.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <utime.h>
#include <vala.h>

struct _ValaCCodeWriterPrivate
{
  gchar *_filename;
  gchar *source_filename;
  gboolean _line_directives;
  gchar *temp_filename;
  gboolean file_exists;
  FILE *stream;
  gint indent;
  gint current_line_number;
  gboolean using_line_directive;
  gboolean _bol;
  gboolean _bael;
};

struct _ValaParamSpecCCodeWriter
{
  GParamSpec parent_instance;
};

static gint vala_ccode_writer_private_offset;
static gpointer vala_ccode_writer_parent_class = NULL;
static GRegex *vala_ccode_writer_fix_indent_regex;
static GRegex *vala_ccode_writer_fix_indent_regex = NULL;

static void vala_ccode_writer_finalize (ValaCCodeWriter *obj);
static GType vala_ccode_writer_get_type_once (void);
static void _vala_array_destroy (gpointer array, gint array_length,
                                 GDestroyNotify destroy_func);
static void _vala_array_free (gpointer array, gint array_length,
                              GDestroyNotify destroy_func);
static gint _vala_array_length (gpointer array);

static inline gpointer
vala_ccode_writer_get_instance_private (ValaCCodeWriter *self)
{
  return G_STRUCT_MEMBER_P (self, vala_ccode_writer_private_offset);
}

const gchar *
vala_ccode_writer_get_filename (ValaCCodeWriter *self)
{
  const gchar *result;
  const gchar *_tmp0_;
  g_return_val_if_fail (self != NULL, NULL);
  _tmp0_ = self->priv->_filename;
  result = _tmp0_;
  return result;
}

void
vala_ccode_writer_set_filename (ValaCCodeWriter *self, const gchar *value)
{
  g_return_if_fail (self != NULL);
  if (self->priv->_filename)
    g_free (self->priv->_filename);
  self->priv->_filename = g_strdup (value);
}

gboolean
vala_ccode_writer_get_line_directives (ValaCCodeWriter *self)
{
  g_return_val_if_fail (self != NULL, FALSE);
  return self->priv->_line_directives;
}

void
vala_ccode_writer_set_line_directives (ValaCCodeWriter *self, gboolean value)
{
  g_return_if_fail (self != NULL);
  self->priv->_line_directives = value;
}

gboolean
vala_ccode_writer_get_bol (ValaCCodeWriter *self)
{
  g_return_val_if_fail (self != NULL, FALSE);
  return self->priv->_bol;
}

ValaCCodeWriter *
vala_ccode_writer_construct (GType object_type, const gchar *filename,
                             const gchar *source_filename)
{
  ValaCCodeWriter *self = NULL;
  g_return_val_if_fail (filename != NULL, NULL);
  self = (ValaCCodeWriter *)g_type_create_instance (object_type);
  vala_ccode_writer_set_filename (self, filename);
  self->priv->source_filename = g_strdup (source_filename);
  return self;
}

ValaCCodeWriter *
vala_ccode_writer_new (const gchar *filename, const gchar *source_filename)
{
  return vala_ccode_writer_construct (VALA_TYPE_CCODE_WRITER, filename,
                                      source_filename);
}

/**
 * Opens the file.
 *
 * @return true if the file has been opened successfully,
 *         false otherwise
 */
gboolean
vala_ccode_writer_open (ValaCCodeWriter *self, gboolean write_version)
{
  g_return_val_if_fail (self != NULL, FALSE);
  self->priv->file_exists = g_file_test (self->priv->_filename, G_FILE_TEST_EXISTS);
  if (self->priv->file_exists)
    {
      if (self->priv->temp_filename)
        g_free (self->priv->temp_filename);
      self->priv->temp_filename =
        g_strdup_printf ("%s.valatmp", self->priv->_filename);
      if (self->priv->stream)
        fclose (self->priv->stream);
      self->priv->stream = g_fopen (self->priv->temp_filename, "w");
    }
  else
    {
      gchar *dirname = g_path_get_dirname (self->priv->_filename);
      g_mkdir_with_parents (dirname, 0755);
      self->priv->stream = g_fopen (self->priv->_filename, "w");
      g_free (dirname);
    }

  if (self->priv->stream == NULL)
    return FALSE;

  gchar *opening =
    write_version ?
    g_strdup_printf ("/* %s generated by valac %s, the Vala compiler",
                     self->priv->_filename, VALA_BUILD_VERSION) :
    g_strdup_printf ("/* %s generated by valac, the Vala compiler",
                     self->priv->_filename);
  vala_ccode_writer_write_string (self, opening);

  if (self->priv->source_filename != NULL)
    {
      gchar *from = g_strdup_printf (" * generated from %s",
                                     self->priv->source_filename);
      vala_ccode_writer_write_newline (self);
      vala_ccode_writer_write_string (self, from);
      g_free (from);
    }

  vala_ccode_writer_write_string (self, ", do not modify */");
  vala_ccode_writer_write_newline (self);
  vala_ccode_writer_write_newline (self);
  g_free (opening);

  return TRUE;
}

/**
 * Closes the file.
 */
void
vala_ccode_writer_close (ValaCCodeWriter *self)
{
  GError *error = NULL;
  g_return_if_fail (self != NULL);
  g_clear_pointer (&self->priv->stream, fclose);

  gboolean changed = FALSE;
  if (self->priv->file_exists)
    {
      GMappedFile *old_file = g_mapped_file_new (self->priv->_filename,
                                                 FALSE, &error);
      if (G_UNLIKELY (error != NULL))
        {
          if (error->domain != G_FILE_ERROR)
            g_critical ("file %s: line %d: unexpected error %s (%s, %d)",
                        __FILE__, __LINE__, error->message,
                        g_quark_to_string (error->domain), error->code);
          g_clear_error (&error);
          return;
        }
      GMappedFile *new_file = g_mapped_file_new (self->priv->temp_filename,
                                                 FALSE, &error);
      if (G_UNLIKELY (error != NULL))
        {
          if (error->domain != G_FILE_ERROR)
            g_critical ("file %s: line %d: unexpected error %s (%s, %d)",
                        __FILE__, __LINE__, error->message,
                        g_quark_to_string (error->domain), error->code);
          g_clear_error (&error);
          return;
        }

      gsize len = g_mapped_file_get_length (old_file);
      if (len == g_mapped_file_get_length (new_file))
        changed = (memcmp (g_mapped_file_get_contents (old_file),
                           g_mapped_file_get_contents (new_file),
                           len) != 0);
      else
        changed = TRUE;
      g_mapped_file_unref (old_file);
      g_mapped_file_unref (new_file);
    }

  if (changed)
    g_rename (self->priv->temp_filename, self->priv->_filename);
  else
    {
      g_unlink (self->priv->temp_filename);
      if (self->priv->source_filename != NULL)
        {
          GStatBuf stats = { 0 };
          GStatBuf target_stats = { 0 };

          g_stat (self->priv->source_filename, &stats);
          g_stat (self->priv->_filename, &target_stats);

          if (stats.st_mtime >= target_stats.st_mtime)
            {
              struct utimbuf timebuf = { 0 };
              timebuf.actime = stats.st_atime + 1;
              timebuf.modtime = stats.st_mtime + 1;
              g_utime (self->priv->_filename, &timebuf);
            }
        }
    }
}

/**
 * Writes tabs according to the current indent level.
 */
void
vala_ccode_writer_write_indent (ValaCCodeWriter *self,
                                ValaCCodeLineDirective *line)
{
  g_return_if_fail (self != NULL);

  if (self->priv->_line_directives)
    {
      if (line != NULL)
        {
          vala_ccode_node_write ((ValaCCodeNode *)line, self);
          self->priv->using_line_directive = TRUE;
        }
      else if (self->priv->using_line_directive)
        {
          gchar *basename = g_path_get_basename (self->priv->_filename);
          gchar *directive =
            g_strdup_printf ("#line %d \"%s\"",
                             self->priv->current_line_number + 1,
                             basename);
          vala_ccode_writer_write_string (self, directive);
          vala_ccode_writer_write_newline (self);
          self->priv->using_line_directive = FALSE;
          g_free (basename);
          g_free (directive);
        }
    }

  if (!self->priv->_bol)
    vala_ccode_writer_write_newline (self);

  gchar *indent = g_strnfill ((gsize)self->priv->indent, '\t');
  fputs (indent, self->priv->stream);
  g_free (indent);
  self->priv->_bol = FALSE;
}

/**
 * Writes n spaces.
 */
void
vala_ccode_writer_write_nspaces (ValaCCodeWriter *self, guint n)
{
  g_return_if_fail (self != NULL);
  gchar *spaces = g_strnfill ((gsize)n, ' ');
  fputs (spaces, self->priv->stream);
  g_free (spaces);
  self->priv->_bol = FALSE;
}

/**
 * Writes the specified string.
 *
 * @param s a string
 */
void
vala_ccode_writer_write_string (ValaCCodeWriter *self, const gchar *s)
{
  FILE *_tmp0_;
  g_return_if_fail (self != NULL);
  g_return_if_fail (s != NULL);
  fputs (s, self->priv->stream);
  self->priv->_bol = FALSE;
}

/**
 * Writes a newline.
 */
void
vala_ccode_writer_write_newline (ValaCCodeWriter *self)
{
  g_return_if_fail (self != NULL);
  if (!self->priv->_bol)
    self->priv->_bael = FALSE;
  else if (!self->priv->_bael)
    self->priv->_bael = TRUE;
  else
    return;

  fputc ('\n', self->priv->stream);
  self->priv->current_line_number++;
  self->priv->_bol = TRUE;
}

/**
 * Opens a new block, increasing the indent level.
 */
void
vala_ccode_writer_write_begin_block (ValaCCodeWriter *self)
{
  g_return_if_fail (self != NULL);
  if (!self->priv->_bol)
    fputc (' ', self->priv->stream);
  else
    vala_ccode_writer_write_indent (self, NULL);
  fputc ('{', self->priv->stream);
  vala_ccode_writer_write_newline (self);
  self->priv->indent += 1;
}

/**
 * Closes the current block, decreasing the indent level.
 */
void
vala_ccode_writer_write_end_block (ValaCCodeWriter *self)
{
  g_return_if_fail (self != NULL);
  g_assert (self->priv->indent > 0);
  self->priv->indent -= 1;
  vala_ccode_writer_write_indent (self, NULL);
  fputc ('}', self->priv->stream);
}

/**
 * Writes the specified text as comment.
 *
 * @param text the comment text
 */
void
vala_ccode_writer_write_comment (ValaCCodeWriter *self, const gchar *text)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (text != NULL);
  GError *error = NULL;
  gboolean first = TRUE;

  vala_ccode_writer_write_indent (self, NULL);
  fputs ("/*", self->priv->stream);
  if (vala_ccode_writer_fix_indent_regex == NULL)
    {
      vala_ccode_writer_fix_indent_regex = g_regex_new ("^\t+", 0, 0, &error);
      if (G_UNLIKELY (error != NULL))
        goto fail;
    }

  GStrv lines = g_strsplit (text, "\n", 0);
  for (gsize lineno = 0; lineno < g_strv_length (lines); ++lineno)
    {
      const gchar *line = lines[lineno];
      if (!first)
        vala_ccode_writer_write_indent (self, NULL);
      else
        first = FALSE;

      gchar *unindented =
        g_regex_replace_literal (vala_ccode_writer_fix_indent_regex,
                                 line, (gssize)-1, 0,
                                 "", 0, &error);
      if (G_UNLIKELY (error != NULL))
        {
          g_strfreev (lines);
          goto fail;
        }

      GStrv parts = g_strsplit (unindented, "*/", 0);
      for (gsize partno = 0; partno < g_strv_length (parts); ++partno)
        {
          if (partno)
            fputs ("* /", self->priv->stream);
          fputs (parts[partno], self->priv->stream);
        }
      g_strfreev (parts);
      g_free (unindented);
    }
  g_strfreev (lines);
  fputs ("*/", self->priv->stream);
  vala_ccode_writer_write_newline (self);
  return;

 fail:
  if (error->domain != G_REGEX_ERROR)
    g_critical ("file %s: line %d: unexpected error %s (%s, %d)",
                __FILE__, __LINE__, error->message,
                g_quark_to_string (error->domain), error->code);
  g_clear_error (&error);
  return;
}

static void
vala_value_ccode_writer_init (GValue *value)
{
  value->data[0].v_pointer = NULL;
}

static void
vala_value_ccode_writer_free_value (GValue *value)
{
  if (value->data[0].v_pointer)
    vala_ccode_writer_unref (value->data[0].v_pointer);
}

static void
vala_value_ccode_writer_copy_value (const GValue *src_value,
                                    GValue *dest_value)
{
  if (src_value->data[0].v_pointer)
    dest_value->data[0].v_pointer =
      vala_ccode_writer_ref (src_value->data[0].v_pointer);
  else
    dest_value->data[0].v_pointer = NULL;
}

static gpointer
vala_value_ccode_writer_peek_pointer (const GValue *value)
{
  return value->data[0].v_pointer;
}

static gchar *
vala_value_ccode_writer_collect_value (GValue *value, guint n_collect_values,
                                       GTypeCValue *collect_values,
                                       guint collect_flags)
{
  if (collect_values[0].v_pointer)
    {
      ValaCCodeWriter *object;
      object = collect_values[0].v_pointer;
      if (object->parent_instance.g_class == NULL)
        return g_strconcat ("invalid unclassed object pointer for value type `",
                            G_VALUE_TYPE_NAME (value), "'", NULL);
      else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object),
                                         G_VALUE_TYPE (value)))
        return g_strconcat ("invalid object type `",
                            g_type_name (G_TYPE_FROM_INSTANCE (object)),
                            "' for value type `", G_VALUE_TYPE_NAME (value),
                            "'", NULL);
      value->data[0].v_pointer = vala_ccode_writer_ref (object);
    }
  else
    value->data[0].v_pointer = NULL;
  return NULL;
}

static gchar *
vala_value_ccode_writer_lcopy_value (const GValue *value,
                                     guint n_collect_values,
                                     GTypeCValue *collect_values,
                                     guint collect_flags)
{
  ValaCCodeWriter **object_p;
  object_p = collect_values[0].v_pointer;
  if (!object_p)
    return g_strdup_printf ("value location for `%s' passed as NULL",
                            G_VALUE_TYPE_NAME (value));
  if (!value->data[0].v_pointer)
    *object_p = NULL;
  else if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
    *object_p = value->data[0].v_pointer;
  else
    *object_p = vala_ccode_writer_ref (value->data[0].v_pointer);
  return NULL;
}

GParamSpec *
vala_param_spec_ccode_writer (const gchar *name, const gchar *nick,
                              const gchar *blurb, GType object_type,
                              GParamFlags flags)
{
  struct _ValaParamSpecCCodeWriter *spec;
  g_return_val_if_fail (g_type_is_a (object_type, VALA_TYPE_CCODE_WRITER),
                        NULL);
  spec = g_param_spec_internal (G_TYPE_PARAM_OBJECT, name, nick, blurb, flags);
  G_PARAM_SPEC (spec)->value_type = object_type;
  return G_PARAM_SPEC (spec);
}

gpointer
vala_value_get_ccode_writer (const GValue *value)
{
  g_return_val_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, VALA_TYPE_CCODE_WRITER),
                        NULL);
  return value->data[0].v_pointer;
}

void
vala_value_set_ccode_writer (GValue *value, gpointer v_object)
{
  ValaCCodeWriter *old;
  g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, VALA_TYPE_CCODE_WRITER));
  old = value->data[0].v_pointer;
  if (v_object)
    {
      g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object,
                                                    VALA_TYPE_CCODE_WRITER));
      g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object),
                                                 G_VALUE_TYPE (value)));
      value->data[0].v_pointer = v_object;
      vala_ccode_writer_ref (value->data[0].v_pointer);
    }
  else
    value->data[0].v_pointer = NULL;
  if (old)
    vala_ccode_writer_unref (old);
}

void
vala_value_take_ccode_writer (GValue *value, gpointer v_object)
{
  ValaCCodeWriter *old;
  g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, VALA_TYPE_CCODE_WRITER));
  old = value->data[0].v_pointer;
  if (v_object)
    {
      g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object,
                                                    VALA_TYPE_CCODE_WRITER));
      g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object),
                                                 G_VALUE_TYPE (value)));
      value->data[0].v_pointer = v_object;
    }
  else
    value->data[0].v_pointer = NULL;
  if (old)
    vala_ccode_writer_unref (old);
}

static void
vala_ccode_writer_class_init (ValaCCodeWriterClass *klass, gpointer klass_data)
{
  vala_ccode_writer_parent_class = g_type_class_peek_parent (klass);
  ((ValaCCodeWriterClass *)klass)->finalize = vala_ccode_writer_finalize;
  g_type_class_adjust_private_offset (klass, &vala_ccode_writer_private_offset);
}

static void
vala_ccode_writer_instance_init (ValaCCodeWriter *self, gpointer klass)
{
  self->priv = vala_ccode_writer_get_instance_private (self);
  self->priv->current_line_number = 1;
  self->priv->_bol = TRUE;
  self->priv->_bael = FALSE;
  self->ref_count = 1;
}

static void
vala_ccode_writer_finalize (ValaCCodeWriter *obj)
{
  ValaCCodeWriter *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_CCODE_WRITER,
                                     ValaCCodeWriter);
  g_signal_handlers_destroy (self);
  g_clear_pointer (&self->priv->_filename, g_free);
  g_clear_pointer (&self->priv->source_filename, g_free);
  g_clear_pointer (&self->priv->temp_filename, g_free);
  g_clear_pointer (&self->priv->stream, fclose);
}

/**
 * Represents a writer to write C source files.
 */
static GType
vala_ccode_writer_get_type_once (void)
{
  static const GTypeValueTable g_define_type_value_table =
    {
      vala_value_ccode_writer_init,
      vala_value_ccode_writer_free_value,
      vala_value_ccode_writer_copy_value,
      vala_value_ccode_writer_peek_pointer,
      "p",
      vala_value_ccode_writer_collect_value,
      "p",
      vala_value_ccode_writer_lcopy_value
    };
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCCodeWriterClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_ccode_writer_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCCodeWriter),
      0,
      (GInstanceInitFunc)vala_ccode_writer_instance_init,
      &g_define_type_value_table
    };
  static const GTypeFundamentalInfo g_define_type_fundamental_info =
    { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE
       | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
  GType vala_ccode_writer_type_id =
    g_type_register_fundamental (g_type_fundamental_next (), "ValaCCodeWriter",
                                 &g_define_type_info,
                                 &g_define_type_fundamental_info, 0);
  vala_ccode_writer_private_offset =
    g_type_add_instance_private (vala_ccode_writer_type_id,
                                 sizeof (ValaCCodeWriterPrivate));
  return vala_ccode_writer_type_id;
}

GType
vala_ccode_writer_get_type (void)
{
  static volatile gsize vala_ccode_writer_type_id__volatile = 0;
  if (g_once_init_enter (&vala_ccode_writer_type_id__volatile))
    {
      GType vala_ccode_writer_type_id;
      vala_ccode_writer_type_id = vala_ccode_writer_get_type_once ();
      g_once_init_leave (&vala_ccode_writer_type_id__volatile,
                         vala_ccode_writer_type_id);
    }
  return vala_ccode_writer_type_id__volatile;
}

gpointer
vala_ccode_writer_ref (gpointer instance)
{
  ValaCCodeWriter *self = instance;
  g_atomic_int_inc (&self->ref_count);
  return instance;
}

void
vala_ccode_writer_unref (gpointer instance)
{
  ValaCCodeWriter *self = instance;
  if (g_atomic_int_dec_and_test (&self->ref_count))
    {
      VALA_CCODE_WRITER_GET_CLASS (self)->finalize (self);
      g_type_free_instance ((GTypeInstance *)self);
    }
}
