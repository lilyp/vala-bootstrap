/* valacompiler.c
 *
 * Copyright (C) 2022  Liliana Marie Prikler
 * Copyright (C) 2006-2012  Jürg Billeter
 * Copyright (C) 1996-2002, 2004, 2005, 2006 Free Software Foundation, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Author:
 *  Liliana Marie Prikler <liliana.prikler@gmail.com>
 */

#include <config.h>
#include <glib-object.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gobject/gvaluecollector.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vala.h>
#include <valacodegen.h>
#include <valagee.h>

#define VALA_TYPE_COMPILER (vala_compiler_get_type ())
#define VALA_COMPILER(obj)                                              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),                                   \
                               VALA_TYPE_COMPILER,                      \
                               ValaCompiler))
#define VALA_COMPILER_CLASS(klass)                                      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),                                    \
                            VALA_TYPE_COMPILER,                         \
                            ValaCompilerClass))
#define VALA_IS_COMPILER(obj)                                   \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VALA_TYPE_COMPILER))
#define VALA_IS_COMPILER_CLASS(klass)                           \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), VALA_TYPE_COMPILER))
#define VALA_COMPILER_GET_CLASS(obj)                                    \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),                                    \
                              VALA_TYPE_COMPILER,                       \
                              ValaCompilerClass))

typedef struct _ValaCompilerPrivate
{
  ValaCodeContext *context;
} ValaCompilerPrivate;

typedef struct _ValaCompiler
{
  GTypeInstance parent_instance;
  volatile int ref_count;
  ValaCompilerPrivate *priv;
} ValaCompiler;

typedef struct _ValaCompilerClass
{
  GTypeClass parent_class;
  void (*finalize) (ValaCompiler *self);
} ValaCompilerClass;

typedef struct _RunSourceChildWatchData
{
  int _ref_count_;
  GMainLoop *loop;
  gint child_status;
} RunSourceChildWatchData;

static gint vala_compiler_private_offset;
static gpointer vala_compiler_parent_class = NULL;
static gchar *vala_compiler_basedir = NULL;
static gchar *vala_compiler_directory = NULL;
static gboolean vala_compiler_version = FALSE;
static gboolean vala_compiler_api_version = FALSE;
static gchar **vala_compiler_sources = NULL;
static gchar **vala_compiler_vapi_directories = NULL;
static gchar **vala_compiler_gir_directories = NULL;
static gchar **vala_compiler_metadata_directories = NULL;
static gchar *vala_compiler_vapi_filename = NULL;
static gchar *vala_compiler_library = NULL;
static gchar *vala_compiler_shared_library = NULL;
static gchar *vala_compiler_gir = NULL;
static gchar **vala_compiler_packages = NULL;
static gchar **vala_compiler_fast_vapis = NULL;
static gchar *vala_compiler_target_glib = NULL;
static gchar **vala_compiler_gresources = NULL;
static gchar **vala_compiler_gresources_directories = NULL;
static gboolean vala_compiler_ccode_only = FALSE;
static gboolean vala_compiler_abi_stability = FALSE;
static gchar *vala_compiler_header_filename = NULL;
static gchar *vala_compiler_internal_header_filename = NULL;
static gchar *vala_compiler_internal_vapi_filename = NULL;
static gchar *vala_compiler_fast_vapi_filename = NULL;
static gboolean vala_compiler_vapi_comments = FALSE;
static gchar *vala_compiler_symbols_filename = NULL;
static gchar *vala_compiler_includedir = NULL;
static gboolean vala_compiler_compile_only = FALSE;
static gchar *vala_compiler_output = NULL;
static gboolean vala_compiler_debug = FALSE;
static gboolean vala_compiler_mem_profiler = FALSE;
static gboolean vala_compiler_disable_assert = FALSE;
static gboolean vala_compiler_enable_checking = FALSE;
static gboolean vala_compiler_deprecated = FALSE;
static gboolean vala_compiler_hide_internal = FALSE;
static gboolean vala_compiler_experimental = FALSE;
static gboolean vala_compiler_experimental_non_null = FALSE;
static gboolean vala_compiler_gobject_tracing = FALSE;
static gboolean vala_compiler_disable_since_check = FALSE;
static gboolean vala_compiler_disable_warnings = FALSE;
static gboolean vala_compiler_keep_going = FALSE;
static gboolean vala_compiler_list_sources = FALSE;
static gchar *vala_compiler_cc_command = NULL;
static gchar **vala_compiler_cc_options = NULL;
static gchar *vala_compiler_pkg_config_command = NULL;
static gchar *vala_compiler_dump_tree = NULL;
static gboolean vala_compiler_save_temps = FALSE;
static gchar **vala_compiler_defines = NULL;
static gboolean vala_compiler_quiet_mode = FALSE;
static gboolean vala_compiler_verbose_mode = FALSE;
static ValaProfile vala_compiler_profile = 0;
static gboolean vala_compiler_nostdpkg = FALSE;
static gboolean vala_compiler_enable_version_header = FALSE;
static gboolean vala_compiler_disable_version_header = FALSE;
static gboolean vala_compiler_fatal_warnings = FALSE;
static gboolean vala_compiler_disable_colored_output = FALSE;
static ValaReportColored vala_compiler_colored_output = VALA_REPORT_COLORED_AUTO;
static gchar *vala_compiler_dependencies = NULL;
static gchar *vala_compiler_depfile = NULL;
static gchar *vala_compiler_entry_point = NULL;
static gboolean vala_compiler_run_output = FALSE;
static gchar *vala_compiler_run_args = NULL;

G_GNUC_INTERNAL gpointer vala_compiler_ref (gpointer instance);
G_GNUC_INTERNAL void vala_compiler_unref (gpointer instance);
G_GNUC_INTERNAL GType vala_compiler_get_type (void) G_GNUC_CONST G_GNUC_UNUSED;
#define VALA_COMPILER_DEFAULT_COLORS                                    \
  "error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01"
static gboolean vala_compiler_option_deprecated (const gchar *option_name,
                                                 const gchar *val, void *data,
                                                 GError **error);
static gboolean vala_compiler_option_parse_profile (const gchar *option_name,
                                                    const gchar *val,
                                                    void *data,
                                                    GError **error);
static gboolean vala_compiler_option_parse_color (const gchar *option_name,
                                                  const gchar *val, void *data,
                                                  GError **error);
static gint vala_compiler_quit (ValaCompiler *self);
static gint vala_compiler_run (ValaCompiler *self);
static gint vala_compiler_run_source (gint argc, gchar **argv);
G_GNUC_INTERNAL ValaCompiler *vala_compiler_new (void);
G_GNUC_INTERNAL ValaCompiler *vala_compiler_construct (GType object_type);
static void vala_compiler_finalize (ValaCompiler *obj);
static GType vala_compiler_get_type_once (void);
static void _vala_array_destroy (gpointer array, gint array_length,
                                 GDestroyNotify destroy_func);
static void _vala_array_free (gpointer array, gint array_length,
                              GDestroyNotify destroy_func);
static gint _vala_array_length (gpointer array);

static const GOptionEntry VALA_COMPILER_OPTIONS[63] = {
  { "vapidir", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME_ARRAY,
    &vala_compiler_vapi_directories, "Look for package bindings in DIRECTORY",
    "DIRECTORY..." },
  { "girdir", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME_ARRAY,
    &vala_compiler_gir_directories, "Look for .gir files in DIRECTORY",
    "DIRECTORY..." },
  { "metadatadir", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME_ARRAY,
    &vala_compiler_metadata_directories,
    "Look for GIR .metadata files in DIRECTORY", "DIRECTORY..." },
  { "pkg", (gchar)0, (gint)0, G_OPTION_ARG_STRING_ARRAY,
    &vala_compiler_packages, "Include binding for PACKAGE", "PACKAGE..." },
  { "vapi", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_vapi_filename, "Output VAPI file name", "FILE" },
  { "library", (gchar)0, (gint)0, G_OPTION_ARG_STRING, &vala_compiler_library,
    "Library name", "NAME" },
  { "shared-library", (gchar)0, (gint)0, G_OPTION_ARG_STRING,
    &vala_compiler_shared_library, "Shared library name used in generated gir",
    "NAME" },
  { "gir", (gchar)0, (gint)0, G_OPTION_ARG_STRING, &vala_compiler_gir,
    "GObject-Introspection repository file name", "NAME-VERSION.gir" },
  { "basedir", 'b', (gint)0, G_OPTION_ARG_FILENAME, &vala_compiler_basedir,
    "Base source directory", "DIRECTORY" },
  { "directory", 'd', (gint)0, G_OPTION_ARG_FILENAME, &vala_compiler_directory,
    "Change output directory from current working directory", "DIRECTORY" },
  { "version", (gchar)0, (gint)0, G_OPTION_ARG_NONE, &vala_compiler_version,
    "Display version number", NULL },
  { "api-version", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_api_version, "Display API version number", NULL },
  { "ccode", 'C', (gint)0, G_OPTION_ARG_NONE, &vala_compiler_ccode_only,
    "Output C code", NULL },
  { "header", 'H', (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_header_filename, "Output C header file", "FILE" },
  { "use-header", (gchar)0,
    (gint)(G_OPTION_FLAG_OPTIONAL_ARG | G_OPTION_FLAG_NO_ARG),
    G_OPTION_ARG_CALLBACK, (void *)vala_compiler_option_deprecated,
    "Use C header file (DEPRECATED AND IGNORED)", NULL },
  { "includedir", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_includedir, "Directory used to include the C header file",
    "DIRECTORY" },
  { "internal-header", 'h', (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_internal_header_filename, "Output internal C header file",
    "FILE" },
  { "internal-vapi", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_internal_vapi_filename, "Output vapi with internal api",
    "FILE" },
  { "fast-vapi", (gchar)0, (gint)0, G_OPTION_ARG_STRING,
    &vala_compiler_fast_vapi_filename,
    "Output vapi without performing symbol resolution", NULL },
  { "use-fast-vapi", (gchar)0, (gint)0, G_OPTION_ARG_STRING_ARRAY,
    &vala_compiler_fast_vapis, "Use --fast-vapi output during this compile",
    NULL },
  { "vapi-comments", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_vapi_comments, "Include comments in generated vapi", NULL },
  { "deps", (gchar)0, (gint)0, G_OPTION_ARG_STRING,
    &vala_compiler_dependencies,
    "Write make-style dependency information to this file", NULL },
  { "depfile", (gchar)0, (gint)0, G_OPTION_ARG_STRING, &vala_compiler_depfile,
    "Write make-style external dependency information for build systems to "
    "this file",
    NULL },
  { "list-sources", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_list_sources,
    "Output a list of all source and binding files which are used", NULL },
  { "symbols", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_symbols_filename, "Output symbols file", "FILE" },
  { "compile", 'c', (gint)0, G_OPTION_ARG_NONE, &vala_compiler_compile_only,
    "Compile but do not link", NULL },
  { "output", 'o', (gint)0, G_OPTION_ARG_FILENAME, &vala_compiler_output,
    "Place output in file FILE", "FILE" },
  { "debug", 'g', (gint)0, G_OPTION_ARG_NONE, &vala_compiler_debug,
    "Produce debug information", NULL },
  { "thread", (gchar)0,
    (gint)(G_OPTION_FLAG_OPTIONAL_ARG | G_OPTION_FLAG_NO_ARG),
    G_OPTION_ARG_CALLBACK, (void *)vala_compiler_option_deprecated,
    "Enable multithreading support (DEPRECATED AND IGNORED)", NULL },
  { "enable-mem-profiler", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_mem_profiler, "Enable GLib memory profiler", NULL },
  { "define", 'D', (gint)0, G_OPTION_ARG_STRING_ARRAY, &vala_compiler_defines,
    "Define SYMBOL", "SYMBOL..." },
  { "main", (gchar)0, (gint)0, G_OPTION_ARG_STRING, &vala_compiler_entry_point,
    "Use SYMBOL as entry point", "SYMBOL..." },
  { "nostdpkg", (gchar)0, (gint)0, G_OPTION_ARG_NONE, &vala_compiler_nostdpkg,
    "Do not include standard packages", NULL },
  { "disable-assert", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_disable_assert, "Disable assertions", NULL },
  { "enable-checking", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_enable_checking, "Enable additional run-time checks",
    NULL },
  { "enable-deprecated", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_deprecated, "Enable deprecated features", NULL },
  { "hide-internal", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_hide_internal, "Hide symbols marked as internal", NULL },
  { "enable-experimental", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_experimental, "Enable experimental features", NULL },
  { "disable-warnings", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_disable_warnings, "Disable warnings", NULL },
  { "fatal-warnings", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_fatal_warnings, "Treat warnings as fatal", NULL },
  { "disable-since-check", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_disable_since_check,
    "Do not check whether used symbols exist in local packages", NULL },
  { "keep-going", 'k', (gint)0, G_OPTION_ARG_NONE, &vala_compiler_keep_going,
    "Continue as much as possible after an error", NULL },
  { "enable-experimental-non-null", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_experimental_non_null,
    "Enable experimental enhancements for non-null types", NULL },
  { "enable-gobject-tracing", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_gobject_tracing, "Enable GObject creation tracing", NULL },
  { "cc", (gchar)0, (gint)0, G_OPTION_ARG_STRING, &vala_compiler_cc_command,
    "Use COMMAND as C compiler command", "COMMAND" },
  { "Xcc", 'X', (gint)0, G_OPTION_ARG_STRING_ARRAY, &vala_compiler_cc_options,
    "Pass OPTION to the C compiler", "OPTION..." },
  { "pkg-config", (gchar)0, (gint)0, G_OPTION_ARG_STRING,
    &vala_compiler_pkg_config_command, "Use COMMAND as pkg-config command",
    "COMMAND" },
  { "dump-tree", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME,
    &vala_compiler_dump_tree, "Write code tree to FILE", "FILE" },
  { "save-temps", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_save_temps, "Keep temporary files", NULL },
  { "profile", (gchar)0, (gint)G_OPTION_FLAG_OPTIONAL_ARG,
    G_OPTION_ARG_CALLBACK, (void *)vala_compiler_option_parse_profile,
    "Use the given profile instead of the default, options are 'gobject' or"
    " 'posix'",
    "PROFILE" },
  { "quiet", 'q', (gint)0, G_OPTION_ARG_NONE, &vala_compiler_quiet_mode,
    "Do not print messages to the console", NULL },
  { "verbose", 'v', (gint)0, G_OPTION_ARG_NONE, &vala_compiler_verbose_mode,
    "Print additional messages to the console", NULL },
  { "no-color", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_disable_colored_output,
    "Disable colored output, alias for --color=never", NULL },
  { "color", (gchar)0, (gint)G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK,
    (void *)vala_compiler_option_parse_color,
    "Enable color output, options are 'always', 'never', or 'auto'", "WHEN" },
  { "target-glib", (gchar)0, (gint)0, G_OPTION_ARG_STRING,
    &vala_compiler_target_glib, "Target version of glib for code generation",
    "'MAJOR.MINOR', or 'auto'" },
  { "gresources", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME_ARRAY,
    &vala_compiler_gresources, "XML of gresources", "FILE..." },
  { "gresourcesdir", (gchar)0, (gint)0, G_OPTION_ARG_FILENAME_ARRAY,
    &vala_compiler_gresources_directories, "Look for resources in DIRECTORY",
    "DIRECTORY..." },
  { "enable-version-header", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_enable_version_header,
    "Write vala build version in generated files", NULL },
  { "disable-version-header", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_disable_version_header,
    "Do not write vala build version in generated files", NULL },
  { "run-args", (gchar)0, (gint)0, G_OPTION_ARG_STRING,
    &vala_compiler_run_args,
    "Arguments passed to directly compiled executable", NULL },
  { "abi-stability", (gchar)0, (gint)0, G_OPTION_ARG_NONE,
    &vala_compiler_abi_stability, "Enable support for ABI stability", NULL },
  { G_OPTION_REMAINING, (gchar)0, (gint)0, G_OPTION_ARG_FILENAME_ARRAY,
    &vala_compiler_sources, NULL, "FILE..." },
  { NULL }
};

static inline gpointer
vala_compiler_get_instance_private (ValaCompiler *self)
{
  return G_STRUCT_MEMBER_P (self, vala_compiler_private_offset);
}

#define PROPAGATE_DOMAIN_ERROR(error, what, DOMAIN)                     \
  do                                                                    \
    {                                                                   \
      if (what->domain == DOMAIN)                                        \
        g_propagate_error (error, what);                                \
      else                                                              \
        {                                                               \
          g_critical ("file %s: line %d: uncaught error %s, (%s, %d)",  \
                      __FILE__, __LINE__, what->message,                 \
                      g_quark_to_string (what->domain), what->code);      \
        }                                                               \
    } while (0)

static gboolean
vala_compiler_option_parse_color (const gchar *option_name, const gchar *val,
                                  void *data, GError **error)
{
  g_return_val_if_fail (option_name != NULL, FALSE);
  GQuark val_quark = val ? g_quark_from_string (val) : 0U;

  if (val_quark == g_quark_from_static_string ("auto"))
    {
      vala_compiler_colored_output = VALA_REPORT_COLORED_AUTO;
    }
  else if (val_quark == g_quark_from_static_string ("never"))
    {
      vala_compiler_colored_output = VALA_REPORT_COLORED_NEVER;
    }
  else if (val_quark == g_quark_from_static_string (NULL) ||
           val_quark == g_quark_from_static_string ("always"))
    {
      vala_compiler_colored_output = VALA_REPORT_COLORED_ALWAYS;
    }
  else
    {
      GError *tmp = g_error_new (G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                                 "Invalid --color argument '%s'", val);
      PROPAGATE_DOMAIN_ERROR (error, tmp, G_OPTION_ERROR);
      return FALSE;
    }
  return TRUE;
}

static gboolean
vala_compiler_option_parse_profile (const gchar *option_name, const gchar *val,
                                    void *data, GError **error)
{
  g_return_val_if_fail (option_name != NULL, FALSE);
  GQuark val_quark = val ? g_quark_from_string (val) : 0U;

  if (val_quark == g_quark_from_static_string ("gobject-2.0") ||
      val_quark == g_quark_from_static_string ("gobject"))
    {
      vala_compiler_profile = VALA_PROFILE_GOBJECT;
    }
  else if (val_quark == g_quark_from_static_string ("posix"))
    {
      vala_compiler_profile = VALA_PROFILE_POSIX;
    }
  else
    {
      GError *tmp = g_error_new (G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                                 "Invalid --profile argument '%s'", val);
      PROPAGATE_DOMAIN_ERROR (error, tmp, G_OPTION_ERROR);
      return FALSE;
    }
  return TRUE;
}

static gboolean
vala_compiler_option_deprecated (const gchar *option_name, const gchar *val,
                                 void *data, GError **error)
{
  g_return_val_if_fail (option_name != NULL, FALSE);
  fprintf (stdout,
           "Command-line option `%s` is deprecated and will be ignored\n",
           option_name);
  return TRUE;
}

static gint
vala_compiler_quit (ValaCompiler *self)
{
  g_return_val_if_fail (self != NULL, 0);

  ValaCodeContext *ctx = self->priv->context;
  ValaReport *report = vala_code_context_get_report (ctx);

  if (vala_report_get_errors (report) == 0 &&
      vala_report_get_warnings (report) == 0)
    {
      vala_code_context_pop ();
      return 0;
    }
  else if (vala_report_get_errors (report) == 0 &&
           !vala_compiler_fatal_warnings)
    {
      if (!vala_compiler_quiet_mode)
        fprintf(stdout, "Compilation succeeded - %d warning(s)\n",
                vala_report_get_warnings (report));
      vala_code_context_pop ();
      return 0;
    }
  else
    {
      if (!vala_compiler_quiet_mode)
        fprintf(stdout, "Compilation failed: %d errors, %d warning(s)\n",
                vala_report_get_warnings (report),
                vala_report_get_warnings (report));
      vala_code_context_pop ();
      return 1;
    }
}

static glong
string_last_index_of_char (const gchar *str, gunichar c, gint start_index)
{
  g_return_val_if_fail (str != NULL, 0);

  gchar *rchr = g_utf8_strrchr (((gchar *)str) + start_index, (gssize)-1, c);
  if (rchr)
    return (glong)(rchr - ((gchar *)str));
  else
    return -1;
}

static glong
string_strnlen (const gchar *str, glong maxlen)
{
  gchar *end = memchr(str, 0, (gsize)maxlen);
  if (end)
    return (glong)(end - str);
  else
    return maxlen;
}

static gchar *
string_substring (const gchar *str, glong offset, glong len)
{
  g_return_val_if_fail (str != NULL, NULL);
  g_return_val_if_fail (offset >= 0, NULL);

  glong string_length = string_strnlen ((gchar *)str, offset + len);
  if (len < 0)
    len = string_length - offset;
  g_return_val_if_fail ((offset + len) <= string_length, NULL);
  return g_strndup(((gchar *)str) + offset, (gsize)len);
}

static gchar
string_get (const gchar *str, glong index)
{
  g_return_val_if_fail (str != NULL, '\0');
  return str[index];
}

static gboolean
string_contains (const gchar *str, const gchar *needle)
{
  g_return_val_if_fail (str != NULL, FALSE);
  g_return_val_if_fail (needle != NULL, FALSE);
  return strstr ((gchar *)str, (gchar *)needle) != NULL;
}

static gint
vala_compiler_run (ValaCompiler *self)
{
  g_return_val_if_fail (self != NULL, 0);
  ValaReport *report;
  gint n_sources;
  gboolean has_c_files = FALSE;
  gboolean has_h_files = FALSE;
  ValaParser *parser = NULL;
  ValaGenieParser *genie_parser = NULL;
  ValaGirParser *gir_parser = NULL;
  gint result = 0;

  if (self->priv->context)
    vala_code_context_unref (self->priv->context);
  self->priv->context = vala_code_context_new ();
  vala_code_context_push (self->priv->context);
  report = vala_code_context_get_report (self->priv->context);

  if (vala_compiler_disable_colored_output)
    {
      vala_compiler_colored_output = VALA_REPORT_COLORED_NEVER;
    }

  if (vala_compiler_colored_output != VALA_REPORT_COLORED_NEVER)
    {
      const gchar *env_colors = g_getenv ("VALA_COLORS");
      if (env_colors)
        vala_report_set_colors (report, env_colors,
                                vala_compiler_colored_output);
      else
        vala_report_set_colors (report, VALA_COMPILER_DEFAULT_COLORS,
                                vala_compiler_colored_output);
    }

  if (!vala_compiler_ccode_only && vala_compiler_output == NULL)
    {
      glong dot = string_last_index_of_char (vala_compiler_sources[0],
                                             (gunichar)'.', 0);
      if (dot != -1)
        {
          const gchar *noext = string_substring (vala_compiler_sources[0],
                                                 (gunichar)'.', 0);
          vala_compiler_output = g_path_get_basename (noext);
        }
    }

  vala_code_context_set_assert (self->priv->context,
                                !vala_compiler_disable_assert);
  vala_code_context_set_checking (self->priv->context,
                                  vala_compiler_enable_checking);
  vala_code_context_set_deprecated (self->priv->context,
                                    vala_compiler_deprecated);
  vala_code_context_set_since_check (self->priv->context,
                                     !vala_compiler_disable_since_check);
  vala_code_context_set_hide_internal (self->priv->context,
                                       vala_compiler_hide_internal);
  vala_code_context_set_experimental (self->priv->context,
                                      vala_compiler_experimental);
  vala_code_context_set_experimental_non_null (self->priv->context,
                                               vala_compiler_experimental_non_null);
  vala_code_context_set_gobject_tracing (self->priv->context,
                                         vala_compiler_gobject_tracing);
  vala_code_context_set_keep_going (self->priv->context,
                                    vala_compiler_keep_going);

  vala_report_set_enable_warnings (report, !vala_compiler_disable_warnings);
  vala_report_set_verbose_errors (report, !vala_compiler_quiet_mode);
  vala_code_context_set_verbose_mode (self->priv->context,
                                      vala_compiler_verbose_mode);
  vala_code_context_set_version_header (self->priv->context,
                                        !vala_compiler_disable_version_header);
  vala_code_context_set_ccode_only (self->priv->context,
                                    vala_compiler_ccode_only);

  if (vala_compiler_ccode_only && vala_compiler_cc_options)
    vala_report_warning (NULL, "-X has no effect when -C or --ccode is set");

  vala_code_context_set_abi_stability (self->priv->context,
                                       vala_compiler_abi_stability);
  vala_code_context_set_compile_only (self->priv->context,
                                      vala_compiler_compile_only);
  vala_code_context_set_header_filename (self->priv->context,
                                         vala_compiler_header_filename);
  vala_code_context_set_internal_header_filename (self->priv->context,
                                                  vala_compiler_internal_header_filename);
  vala_code_context_set_symbols_filename (self->priv->context,
                                          vala_compiler_symbols_filename);
  vala_code_context_set_includedir (self->priv->context,
                                    vala_compiler_includedir);
  vala_code_context_set_output (self->priv->context, vala_compiler_output);

  if (vala_compiler_output && vala_compiler_ccode_only)
    vala_report_warning (NULL,
                         "--output and -o have no effect when "
                         "-C or --ccode is set");

  if (vala_compiler_basedir == NULL)
    vala_code_context_set_basedir (self->priv->context,
                                   vala_code_context_realpath ("."));
  else
    {
      const gchar *real = vala_code_context_realpath (vala_compiler_basedir);
      vala_code_context_set_basedir (self->priv->context, real);
    }

  if (vala_compiler_directory)
    {
      const gchar *real = vala_code_context_realpath (vala_compiler_directory);
      vala_code_context_set_directory (self->priv->context, real);
    }
  else
    {
      const gchar *basedir = vala_code_context_get_basedir (self->priv->context);
      vala_code_context_set_directory (self->priv->context, basedir);
    }

  vala_code_context_set_vapi_directories (self->priv->context,
                                          vala_compiler_vapi_directories,
                                          _vala_array_length (vala_compiler_vapi_directories));
  vala_code_context_set_vapi_comments (self->priv->context,
                                       vala_compiler_vapi_comments);
  vala_code_context_set_gir_directories (self->priv->context,
                                         vala_compiler_gir_directories,
                                         _vala_array_length (vala_compiler_vapi_directories));
  vala_code_context_set_metadata_directories (self->priv->context,
                                              vala_compiler_metadata_directories,
                                              _vala_array_length (vala_compiler_metadata_directories));
  vala_code_context_set_debug (self->priv->context, vala_compiler_debug);
  vala_code_context_set_mem_profiler (self->priv->context,
                                      vala_compiler_mem_profiler);
  vala_code_context_set_save_temps (self->priv->context,
                                    vala_compiler_save_temps);

  if (vala_compiler_ccode_only && vala_compiler_save_temps)
      vala_report_warning (NULL,
                           "--save-temps has no effect when "
                           "-C or --ccode is set");

  vala_compiler_nostdpkg = vala_compiler_nostdpkg ||
    (vala_compiler_fast_vapi_filename != NULL);

  vala_code_context_set_entry_point_name (self->priv->context,
                                          vala_compiler_entry_point);
  vala_code_context_set_run_output (self->priv->context,
                                    vala_compiler_run_output);
  if (!vala_compiler_pkg_config_command)
    {
      const gchar *pkg_config = g_getenv ("PKG_CONFIG");
      if (pkg_config == NULL) pkg_config = "pkg-config";
      vala_compiler_pkg_config_command = g_strdup (pkg_config);
    }
  vala_code_context_set_pkg_config_command (self->priv->context,
                                            vala_compiler_pkg_config_command);
  vala_code_context_set_target_profile (self->priv->context,
                                        vala_compiler_profile,
                                        !vala_compiler_nostdpkg);
  if (vala_compiler_target_glib)
    vala_code_context_set_target_glib_version (self->priv->context,
                                               vala_compiler_target_glib);

  if (vala_compiler_defines)
    {
      gchar **define_collection = vala_compiler_defines;
      gint define_collection_length = _vala_array_length (vala_compiler_defines);

      for (gint define_it = 0; define_it < define_collection_length;
           define_it++)
        vala_code_context_add_define (self->priv->context,
                                      g_strdup (define_collection[define_it]));
    }

  if (vala_compiler_packages)
    {
      gchar **package_collection = vala_compiler_packages;
      gint package_collection_length = _vala_array_length (vala_compiler_packages);

      for (gint package_it = 0; package_it < package_collection_length;
           package_it++)
        vala_code_context_add_external_package (self->priv->context,
                                                g_strdup (package_collection[package_it]));

      _vala_array_free (vala_compiler_packages,
                        _vala_array_length (vala_compiler_packages),
                        (GDestroyNotify)g_free);
      vala_compiler_packages = NULL;
    }

  if (vala_compiler_fast_vapis)
    {
      gchar **vapi_collection = vala_compiler_fast_vapis;
      gint vapi_collection_length = _vala_array_length (vala_compiler_fast_vapis);

      for (gint vapi_it = 0; vapi_it < vapi_collection_length;
           vapi_it++)
        {
          gchar *vapi = g_strdup (vapi_collection[vapi_it]);
          gchar *rpath = vala_code_context_realpath (vapi);
          ValaSourceFile *source_file =
            vala_source_file_new (self->priv->context,
                                  VALA_SOURCE_FILE_TYPE_FAST,
                                  rpath, NULL, FALSE);
          vala_code_context_add_source_file (self->priv->context,
                                             source_file);
          vala_source_file_unref (source_file);
        }
      vala_code_context_set_use_fast_vapi (self->priv->context, TRUE);
    }
  vala_code_context_set_gresources (self->priv->context,
                                    vala_compiler_gresources,
                                    _vala_array_length (vala_compiler_gresources));

  vala_code_context_set_gresources_directories (self->priv->context,
                                                vala_compiler_gresources_directories,
                                                _vala_array_length (vala_compiler_gresources_directories));

  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  if (vala_code_context_get_profile (self->priv->context) == VALA_PROFILE_GOBJECT)
    {
      ValaGDBusServerModule *codegen = vala_gd_bus_server_module_new ();
      vala_code_context_set_codegen (self->priv->context,
                                     (ValaCodeGenerator *)codegen);
      vala_code_visitor_unref (codegen);
    }
  else
    {
      ValaCCodeDelegateModule *codegen = vala_ccode_delegate_module_new ();
      vala_code_context_set_codegen (self->priv->context,
                                     (ValaCodeGenerator *)codegen);
      vala_code_visitor_unref (codegen);
    }

  n_sources = _vala_array_length (vala_compiler_sources);
  for (gint source_it = 0; source_it < n_sources; source_it++)
    {
      gchar *source = g_strdup (vala_compiler_sources[source_it]);
      if (vala_code_context_add_source_filename (self->priv->context,
                                                 source,
                                                 vala_compiler_run_output,
                                                 TRUE))
        {
          if (g_str_has_suffix (source, ".c"))
            has_c_files = TRUE;
          else if (g_str_has_suffix (source, ".h"))
            has_h_files = TRUE;
        }
      g_free (source);
    }

  _vala_array_free (vala_compiler_sources, n_sources, (GDestroyNotify)g_free);
  vala_compiler_sources = NULL;

  if (vala_compiler_ccode_only && (has_c_files || has_h_files))
    {
      vala_report_warning (NULL,
                           "C header and source files are ignored when "
                           "-C or --ccode is set");
    }

  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  if (vala_compiler_list_sources)
    {
      ValaList *file_list = vala_code_context_get_source_files (self->priv->context);
      gint file_size = vala_collection_get_size ((ValaCollection *)file_list);

      for (gint file_index = 0; file_index < file_size; file_index++)
        {
          ValaSourceFile *file = vala_list_get (file_list, file_index);
          g_print ("%s\n", vala_source_file_get_filename (file));
          vala_source_file_unref (file);
        }

      if (!vala_compiler_ccode_only)
        {
          file_list = vala_code_context_get_c_source_files (self->priv->context);
          file_size = vala_collection_get_size ((ValaCollection *)file_list);

          for (gint file_index = 0; file_index < file_size; file_index++)
            {
              gchar *filename = vala_list_get (file_list, file_index);
              g_print ("%s\n", filename);
              g_free (filename);
            }
        }
      return 0;
    }

  parser = vala_parser_new ();
  genie_parser = vala_genie_parser_new ();
  gir_parser = vala_gir_parser_new ();
  vala_parser_parse (parser, self->priv->context);
  vala_genie_parser_parse (genie_parser, self->priv->context);
  vala_gir_parser_parse (gir_parser, self->priv->context);

  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  if (vala_compiler_fast_vapi_filename)
    {
      ValaCodeWriter *interface_writer = vala_code_writer_new (VALA_CODE_WRITER_TYPE_FAST);
      vala_code_writer_write_file (interface_writer, self->priv->context,
                                   vala_compiler_fast_vapi_filename);
      vala_code_visitor_unref (interface_writer);
      goto done;
    }

  vala_code_context_check (self->priv->context);
  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  if (!vala_compiler_ccode_only &&
      vala_compiler_library == NULL &&
      !has_c_files &&
      vala_code_context_get_entry_point (self->priv->context) == NULL)
    {
      vala_report_error (NULL,
                         "program does not contain a static `main' method");
    }

  if (vala_compiler_dump_tree)
    {
      ValaCodeWriter *code_writer = vala_code_writer_new (VALA_CODE_WRITER_TYPE_DUMP);
      vala_code_writer_write_file (code_writer, self->priv->context,
                                   vala_compiler_dump_tree);
      vala_code_visitor_unref (code_writer);
    }

  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  vala_code_generator_emit (vala_code_context_get_codegen (self->priv->context),
                            self->priv->context);

  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  if (!vala_compiler_fast_vapi_filename && vala_compiler_library)
    {
      g_free (vala_compiler_vapi_filename);
      vala_compiler_vapi_filename = g_strdup_printf ("%s.vapi",
                                                     vala_compiler_library);
    }

  if (vala_compiler_library)
    {
      if (vala_compiler_gir)
        {
          ValaProfile profile = vala_code_context_get_profile (self->priv->context);
          if (profile == VALA_PROFILE_GOBJECT)
            {
              gchar *gir_base = g_path_get_basename (vala_compiler_gir);
              glong gir_len = strlen (gir_base);
              glong last_hyphen =
                string_last_index_of_char (gir_base, (gunichar)'-', 0);
              if (last_hyphen == -1 || !g_str_has_suffix (gir_base, ".gir"))
                {
                  vala_report_error (NULL,
                                     "GIR file name `%s' is not well-formed, "
                                     "expected NAME-VERSION.gir",
                                     vala_compiler_gir);
                }
              else
                {
                  gchar *gir_namespace = string_substring (gir_base, (glong)0, last_hyphen);
                  gchar *gir_version = string_substring (gir_base, last_hyphen + 1,
                                                         (gir_len - last_hyphen) - 5);
                  g_strcanon (gir_version, "0123456789.", '?');
                  if (g_strcmp0 (gir_namespace, "") == 0 ||
                      g_strcmp0 (gir_version, "") == 0 ||
                      !g_ascii_isdigit (string_get (gir_version, 0)) ||
                      string_contains (gir_version, "?"))
                    vala_report_error (NULL,
                                       "GIR file name `%s' is not well-formed, "
                                       "expected NAME-VERSION.gir",
                                       vala_compiler_gir);
                  else
                    {
                      ValaGIRWriter *gir_writer =vala_gir_writer_new ();
                      gchar *gir_directory;
                      if (vala_compiler_directory)
                        gir_directory = g_strdup (vala_compiler_directory);
                      else
                        gir_directory = g_strdup (".");

                      vala_gir_writer_write_file (gir_writer,
                                                  self->priv->context,
                                                  gir_directory,
                                                  vala_compiler_gir,
                                                  gir_namespace,
                                                  gir_version,
                                                  vala_compiler_library,
                                                  vala_compiler_shared_library);

                      g_free (gir_directory);
                      vala_code_visitor_unref (gir_writer);
                    }
                  g_free (gir_version);
                  g_free (gir_namespace);
                }
              g_free (gir_base);
            }
          g_free (vala_compiler_gir);
          vala_compiler_gir = NULL;
        }
      g_free (vala_compiler_library);
      vala_compiler_library = NULL;
    }
  else if (vala_compiler_gir)
    {
      vala_report_warning (NULL, "--gir has no effect without --library");
      g_free (vala_compiler_gir);
      vala_compiler_gir = NULL;
    }
  if (vala_compiler_vapi_filename)
    {
      ValaCodeWriter *interface_writer = vala_code_writer_new (VALA_CODE_WRITER_TYPE_EXTERNAL);
      if (vala_compiler_directory &&
          !g_path_is_absolute (vala_compiler_vapi_filename))
        {
          const gchar *dir =
            vala_code_context_get_directory (self->priv->context);
          gchar *vapi_file = g_strdup_printf ("%s%c%s",
                                              dir, G_DIR_SEPARATOR,
                                              vala_compiler_vapi_filename);
          g_free (vala_compiler_vapi_filename);
          vala_compiler_vapi_filename = vapi_file;
        }
      vala_code_writer_write_file (interface_writer,
                                   self->priv->context,
                                   vala_compiler_vapi_filename);
      vala_code_visitor_unref (interface_writer);
    }
  if (vala_compiler_internal_vapi_filename)
    {
      if (vala_compiler_internal_header_filename &&
          vala_compiler_header_filename)
        {
          ValaCodeWriter *interface_writer =
            vala_code_writer_new (VALA_CODE_WRITER_TYPE_INTERNAL);
          const gchar *includedir =
            vala_code_context_get_includedir (self->priv->context);
          gchar *vapi_filename =
            g_strdup (vala_compiler_internal_vapi_filename);

          if (includedir)
            {
              gchar *header_basename =
                g_path_get_basename (vala_compiler_header_filename);
              gchar *internal_header_basename =
                g_path_get_basename (vala_compiler_internal_header_filename);
              gchar *prefixed_header_filename =
                g_build_path ("/", includedir, header_basename);
              gchar *prefixed_internal_header_filename =
                g_build_path ("/", includedir, internal_header_basename);

              g_free (header_basename);
              g_free (internal_header_basename);
              vala_code_writer_set_cheader_override (interface_writer,
                                                     prefixed_header_filename,
                                                     prefixed_internal_header_filename);
              g_free (prefixed_internal_header_filename);
              g_free (prefixed_header_filename);
            }
          else
            {
              vala_code_writer_set_cheader_override (interface_writer,
                                                     vala_compiler_header_filename,
                                                     vala_compiler_internal_header_filename);
            }

          if (vala_compiler_directory && !g_path_is_absolute (vapi_filename))
            {
              const gchar *dir =
                vala_code_context_get_directory (self->priv->context);
              gchar *prefixed_vapi_filename = g_strdup_printf ("%s%c%s",
                                                               dir,
                                                               G_DIR_SEPARATOR,
                                                               vapi_filename);
              g_free (vapi_filename);
              vapi_filename = prefixed_vapi_filename;
            }

          vala_code_writer_write_file (interface_writer, self->priv->context,
                                       vapi_filename);
          g_free (vapi_filename);
          vala_code_visitor_unref (interface_writer);
        }
      else
        {
          vala_report_error (NULL, "--internal-vapi may only be used in "
                             "combination with --header and --in"
                             "ternal-header");
          goto done;
        }
    }

  if (vala_compiler_dependencies)
    vala_code_context_write_dependencies (self->priv->context,
                                          vala_compiler_dependencies);

  if (vala_compiler_depfile)
    vala_code_context_write_external_dependencies (self->priv->context,
                                                   vala_compiler_depfile);

  if (vala_report_get_errors (report) > 0 ||
      (vala_report_get_warnings (report) > 0 &&
       vala_compiler_fatal_warnings))
    goto done;

  if (!vala_compiler_ccode_only)
    {
      ValaCCodeCompiler *ccompiler = vala_ccode_compiler_new ();
      if (!vala_compiler_cc_command)
        {
          const gchar *cc_env = g_getenv ("CC");
          if (cc_env)
            vala_compiler_cc_command = g_strdup (cc_env);
        }

      if (vala_compiler_cc_options)
        {
          vala_ccode_compiler_compile (ccompiler, self->priv->context,
                                       vala_compiler_cc_command,
                                       vala_compiler_cc_options,
                                       _vala_array_length (vala_compiler_cc_options));
        }
      else
        {
          gchar **options = g_new0 (gchar *, 1);
          vala_ccode_compiler_compile (ccompiler, self->priv->context,
                                       vala_compiler_cc_command, options, 0);
          g_free (options);
        }


      vala_compiler_unref (ccompiler);
    }

 done:
  result = vala_compiler_quit (self);
  if (gir_parser)
    vala_code_visitor_unref (gir_parser);
  if (genie_parser)
    vala_code_visitor_unref (genie_parser);
  if (parser)
    vala_code_visitor_unref (parser);
  return result;
}

static void
_vala_array_add1 (gchar ***array, gint *length, gint *size, gchar *value)
{
  if ((*length) == (*size))
    {
      *size = (*size) ? (2 * (*size)) : 4;
      *array = g_renew (gchar *, *array, (*size) + 1);
    }
  (*array)[(*length)++] = value;
  (*array)[*length] = NULL;
}

static RunSourceChildWatchData *
run_source_child_watch_data_ref (RunSourceChildWatchData *data)
{
  g_atomic_int_inc (&data->_ref_count_);
  return data;
}

static void
run_source_child_watch_data_unref (void *data)
{
  RunSourceChildWatchData *watch_data = (RunSourceChildWatchData *)data;
  if (g_atomic_int_dec_and_test (&watch_data->_ref_count_))
    {
      g_main_loop_unref (watch_data->loop);
      watch_data->loop = NULL;
      g_slice_free (RunSourceChildWatchData, watch_data);
    }
}

static void
vala_compiler_run_source__child_watch_func (GPid pid, gint status, gpointer self)
{
  RunSourceChildWatchData *watch = self;
  watch->child_status = (status & 0xff00) >> 8;
  g_main_loop_quit (watch->loop);
}

static gint
vala_compiler_run_source (gint argc, gchar **argv)
{
  GError *error = NULL;
  GOptionContext *opt_context = g_option_context_new ("- Vala Interpreter");
  gint outputfd = 0;
  gchar **target_args = NULL;
  ValaCompiler *compiler = NULL;
  GPid pid = 0;
  GMainLoop *mainloop;
  RunSourceChildWatchData *child_watch;
  gint result = 0;

  g_option_context_set_help_enabled (opt_context, TRUE);
  g_option_context_add_main_entries (opt_context, VALA_COMPILER_OPTIONS, NULL);
  g_option_context_parse (opt_context, &argc, &argv, &error);
  g_option_context_free (opt_context);

  if (G_UNLIKELY (error != NULL))
    {
      if (error->domain == G_OPTION_ERROR)
        {
          fprintf(stdout, "%s\n", error->message);
          g_error_free (error);
          fprintf(stdout,
                  "Run '%s --help' to see a full list of available command line "
                  "options.\n",
                  argv[0]);
          return 1;
        }
      else
        {
          g_critical ("file %s: line %d: unexpected error: %s (%s, %d)",
                      __FILE__, __LINE__, error->message,
                      g_quark_to_string (error->domain), error->code);
          g_clear_error (&error);
          return -1;
        }
    }

  if (vala_compiler_version)
    {
      fprintf(stdout, "Vala %s\n", VALA_BUILD_VERSION);
      return 0;
    }
  else if (vala_compiler_api_version)
    {
      fprintf(stdout, "%s\n", VALA_API_VERSION);
      return 0;
    }

  if (!vala_compiler_sources)
    {
      fprintf(stderr, "No source file specified.\n");
      return 1;
    }

  g_free (vala_compiler_output);
  vala_compiler_output =
    g_strdup_printf ("%s/%s.XXXXXX", g_get_tmp_dir (),
                     g_path_get_basename (vala_compiler_sources[0]));
  outputfd = g_mkstemp (vala_compiler_output);

  if (outputfd < 0)
    return 1;

  vala_compiler_ccode_only = FALSE;
  vala_compiler_compile_only = FALSE;
  vala_compiler_run_output = TRUE;
  vala_compiler_disable_warnings = TRUE;
  vala_compiler_quiet_mode = TRUE;

  g_free (vala_compiler_library);
  g_free (vala_compiler_shared_library);
  vala_compiler_library = NULL;
  vala_compiler_shared_library = NULL;

  compiler = vala_compiler_new ();
  result = vala_compiler_run (compiler);
  if (result)
    return result;
  close (outputfd);

  if (g_chmod (vala_compiler_output, 0700) != 0)
    {
      g_unlink (vala_compiler_output);
      vala_compiler_unref (compiler);
      return 1;
    }

  target_args = g_new0 (gchar *, 2);
  if (vala_compiler_run_args)
    {
      gint run_args_length = _vala_array_length (vala_compiler_run_args);
      target_args = g_new0 (gchar *, 1 + run_args_length);
      for (gint i = 0; i < run_args_length; i ++)
        target_args[1 + i] = g_strdup (vala_compiler_run_args[i]);
      _vala_array_free (vala_compiler_run_args,
                        run_args_length,
                        (GDestroyNotify)g_free);
      vala_compiler_run_args = NULL;
    }
  else
    {
      target_args = g_new0 (gchar *, 2);
    }
  target_args[0] = vala_compiler_output;

  child_watch = g_slice_new0 (RunSourceChildWatchData);
  child_watch->_ref_count_ = 1;
  child_watch->loop = g_main_loop_new (NULL, FALSE);
  child_watch->child_status = 0;
  g_spawn_async (NULL, target_args, NULL,
                 G_SPAWN_CHILD_INHERITS_STDIN | G_SPAWN_DO_NOT_REAP_CHILD,
                 NULL, NULL, &pid, &error);
  if (G_UNLIKELY (error != NULL))
    {
      if (error->domain == G_OPTION_ERROR)
        {
          fprintf(stdout, "%s\n", error->message);
          g_error_free (error);
          _vala_array_free (target_args,
                            _vala_array_length (target_args),
                            (GDestroyNotify)g_free);
          vala_compiler_unref (compiler);
          return 1;
        }
      else
        {
          g_critical ("file %s: line %d: unexpected error: %s (%s, %d)",
                      __FILE__, __LINE__, error->message,
                      g_quark_to_string (error->domain), error->code);
          g_clear_error (&error);
          return -1;
        }
    }
  g_unlink (vala_compiler_output);
  g_child_watch_add_full (G_PRIORITY_DEFAULT_IDLE, pid,
                          vala_compiler_run_source__child_watch_func,
                          run_source_child_watch_data_ref (child_watch),
                          run_source_child_watch_data_unref);
  g_main_loop_run (child_watch->loop);
  result = child_watch->child_status;
  _vala_array_free (target_args,
                    _vala_array_length (target_args),
                    (GDestroyNotify)g_free);
  vala_compiler_unref (compiler);
  return result;
}

int
main (int argc, char **argv)
{
  GError *error = NULL;
  GOptionContext *opt_context;
  gchar *basename;
  ValaCompiler *compiler;
  gint result;

  setlocale (LC_ALL, "");
  if (g_strcmp0 (vala_get_build_version (), VALA_BUILD_VERSION) != 0)
    {
      fprintf(stderr, "Integrity check failed (libvala %s doesn't match valac %s)\n",
              vala_get_build_version (), VALA_BUILD_VERSION);
      return 1;
    }

  basename = (g_path_get_basename (argv[0]));
  if (g_strcmp0 (basename, "vala") == 0 ||
      g_strcmp0 (basename, "vala" PACKAGE_SUFFIX) == 0)
    {
      return vala_compiler_run_source (argc, argv);
    }

  opt_context = g_option_context_new ("- Vala Compiler");
  g_option_context_set_help_enabled (opt_context, TRUE);
  g_option_context_add_main_entries (opt_context, VALA_COMPILER_OPTIONS, NULL);
  g_option_context_parse (opt_context, &argc, &argv, &error);
  g_option_context_free (opt_context);

  if (G_UNLIKELY (error != NULL))
    {
      if (error->domain == G_OPTION_ERROR)
        {
          fprintf(stdout, "%s\n", error->message);
          g_error_free (error);
          fprintf(stdout,
                  "Run '%s --help' to see a full list of available command line "
                  "options.\n",
                  argv[0]);
          return 1;
        }
      else
        {
          g_critical ("file %s: line %d: unexpected error: %s (%s, %d)",
                      __FILE__, __LINE__, error->message,
                      g_quark_to_string (error->domain), error->code);
          g_clear_error (&error);
          return -1;
        }
    }

  if (vala_compiler_version)
    {
      fprintf(stdout, "Vala %s\n", VALA_BUILD_VERSION);
      return 0;
    }
  else if (vala_compiler_api_version)
    {
      fprintf(stdout, "%s\n", VALA_API_VERSION);
      return 0;
    }

  if (!vala_compiler_sources && !vala_compiler_fast_vapis)
    {
      fprintf(stderr, "No source file specified.\n");
      return 1;
    }

  compiler = vala_compiler_new ();
  result = vala_compiler_run (compiler);
  return result;
}

G_GNUC_INTERNAL ValaCompiler *
vala_compiler_construct (GType object_type)
{
  return (ValaCompiler *)g_type_create_instance (object_type);
}

G_GNUC_INTERNAL ValaCompiler *
vala_compiler_new (void)
{
  return vala_compiler_construct (VALA_TYPE_COMPILER);
}

static void
vala_value_compiler_init (GValue *value)
{
  value->data[0].v_pointer = NULL;
}

static void
vala_value_compiler_free_value (GValue *value)
{
  if (value->data[0].v_pointer)
    {
      vala_compiler_unref (value->data[0].v_pointer);
    }
}

static void
vala_value_compiler_copy_value (const GValue *src_value, GValue *dest_value)
{
  if (src_value->data[0].v_pointer)
    {
      dest_value->data[0].v_pointer =
        vala_compiler_ref (src_value->data[0].v_pointer);
    }
  else
    {
      dest_value->data[0].v_pointer = NULL;
    }
}

static gpointer
vala_value_compiler_peek_pointer (const GValue *value)
{
  return value->data[0].v_pointer;
}

static gchar *
vala_value_compiler_collect_value (GValue *value, guint n_collect_values,
                                   GTypeCValue *collect_values,
                                   guint collect_flags)
{
  if (collect_values[0].v_pointer)
    {
      ValaCompiler *object;
      object = collect_values[0].v_pointer;
      if (object->parent_instance.g_class == NULL)
        {
          return g_strconcat (
                              "invalid unclassed object pointer for value type `",
                              G_VALUE_TYPE_NAME (value), "'", NULL);
        }
      else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object),
                                         G_VALUE_TYPE (value)))
        {
          return g_strconcat ("invalid object type `",
                              g_type_name (G_TYPE_FROM_INSTANCE (object)),
                              "' for value type `", G_VALUE_TYPE_NAME (value),
                              "'", NULL);
        }
      value->data[0].v_pointer = vala_compiler_ref (object);
    }
  else
    {
      value->data[0].v_pointer = NULL;
    }
  return NULL;
}

static gchar *
vala_value_compiler_lcopy_value (const GValue *value, guint n_collect_values,
                                 GTypeCValue *collect_values,
                                 guint collect_flags)
{
  ValaCompiler **object_p;
  object_p = collect_values[0].v_pointer;
  if (!object_p)
    {
      return g_strdup_printf ("value location for `%s' passed as NULL",
                              G_VALUE_TYPE_NAME (value));
    }
  if (!value->data[0].v_pointer)
    {
      *object_p = NULL;
    }
  else if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
    {
      *object_p = value->data[0].v_pointer;
    }
  else
    {
      *object_p = vala_compiler_ref (value->data[0].v_pointer);
    }
  return NULL;
}

static void
vala_compiler_class_init (ValaCompilerClass *klass, gpointer klass_data)
{
  vala_compiler_parent_class = g_type_class_peek_parent (klass);
  ((ValaCompilerClass *)klass)->finalize = vala_compiler_finalize;
  g_type_class_adjust_private_offset (klass, &vala_compiler_private_offset);
}

static void
vala_compiler_instance_init (ValaCompiler *self, gpointer klass)
{
  self->priv = vala_compiler_get_instance_private (self);
  self->ref_count = 1;
}

static void
vala_compiler_finalize (ValaCompiler *obj)
{
  ValaCompiler *self;
  self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_COMPILER, ValaCompiler);
  g_signal_handlers_destroy (self);
  vala_code_context_unref (self->priv->context);
  self->priv->context = NULL;
}

static GType
vala_compiler_get_type_once (void)
{
  static const GTypeValueTable g_define_type_value_table =
    {
      vala_value_compiler_init,
      vala_value_compiler_free_value,
      vala_value_compiler_copy_value,
      vala_value_compiler_peek_pointer,
      "p",
      vala_value_compiler_collect_value,
      "p",
      vala_value_compiler_lcopy_value
    };
  static const GTypeInfo g_define_type_info =
    {
      sizeof (ValaCompilerClass),
      (GBaseInitFunc)NULL,
      (GBaseFinalizeFunc)NULL,
      (GClassInitFunc)vala_compiler_class_init,
      (GClassFinalizeFunc)NULL,
      NULL,
      sizeof (ValaCompiler),
      0,
      (GInstanceInitFunc)vala_compiler_instance_init,
      &g_define_type_value_table
    };
  static const GTypeFundamentalInfo g_define_type_fundamental_info =
    {
      (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE
       | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE)
    };
  GType vala_compiler_type_id =
    g_type_register_fundamental (g_type_fundamental_next (),
                                 "ValaCompiler",
                                 &g_define_type_info,
                                 &g_define_type_fundamental_info,
                                 0);
  vala_compiler_private_offset =
    g_type_add_instance_private (vala_compiler_type_id,
                                 sizeof (ValaCompilerPrivate));
  return vala_compiler_type_id;
}

G_GNUC_INTERNAL GType
vala_compiler_get_type (void)
{
  static volatile gsize vala_compiler_type_id__volatile = 0;
  if (g_once_init_enter (&vala_compiler_type_id__volatile))
    {
      GType vala_compiler_type_id;
      vala_compiler_type_id = vala_compiler_get_type_once ();
      g_once_init_leave (&vala_compiler_type_id__volatile,
                         vala_compiler_type_id);
    }
  return vala_compiler_type_id__volatile;
}

G_GNUC_INTERNAL gpointer
vala_compiler_ref (gpointer instance)
{
  ValaCompiler *self = instance;
  g_atomic_int_inc (&self->ref_count);
  return instance;
}

G_GNUC_INTERNAL void
vala_compiler_unref (gpointer instance)
{
  ValaCompiler *self = instance;
  if (g_atomic_int_dec_and_test (&self->ref_count))
    {
      VALA_COMPILER_GET_CLASS (self)->finalize (self);
      g_type_free_instance ((GTypeInstance *)self);
    }
}

static void
_vala_array_destroy (gpointer array, gint array_length,
                     GDestroyNotify destroy_func)
{
  if ((array != NULL) && (destroy_func != NULL))
    {
      gpointer *arr = array;
      for (gint i = 0; i < array_length; i++)
        if (arr[i] != NULL)
          destroy_func (arr[i]);
    }
}

static void
_vala_array_free (gpointer array, gint array_length,
                  GDestroyNotify destroy_func)
{
  _vala_array_destroy (array, array_length, destroy_func);
  g_free (array);
}

static gint
_vala_array_length (gpointer array)
{
  gint length = 0;
  if (array)
    while (((gpointer *)array)[length])
      length++;
  return length;
}
