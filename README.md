## Why Vala Bootstrap?
Vala is self-hosting: in order to build Vala, you need Vala.
Typically, this means you'll build Vala from pre-compiled C files rather than
the Vala sources.  Unfortunately, this is not as good an idea as it sounds,
given that a bug in the Vala compiler could lead it to generate wrong or even
malicious code.
[Reflect on trusting trust.](http://users.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf).
Nevertheless, pre-compiled C files **are** C files, and as such could
potentially be audited by people proficient in C.  But in order to make this
experience pleasant, manual revision is necessary.

This project aims to both reduce and prettify the C code for valac, so that
such audits become simpler.

## Building Vala
The instructions below describe how to build Vala using this bootstrap
package.  As an alternative, you can `guix build -f guix.scm` in this
directory to perform steps one to four in isolated environments.

### Step One:
Install the following packages:

 * a C compiler, e.g. GCC
 * a C library, e.g. glibc
 * glib (>= 2.48)
 * flex
 * bison
 * make
 * autoconf
 * autoconf-archive
 * automake
 * libtool

### Step Two:
Build this package.

```
cd /path/to/vala-bootstrap-source
NOCONFIGURE=1 ./autogen.sh
./configure --prefix=/path/to/vala-bootstrap-install
make
make install
```

Depending on the exact value of `/path/to/vala-bootstrap-install`,
the last line may need to be replaced with `sudo make install`

### Step Three:
Fetch the [official vala package](https://gitlab.gnome.org/GNOME/vala).

### Step Four:
Build vala.

```
cd /path/to/official-vala
PATH="/path/to/vala-bootstrap-install/bin:$PATH"
./autogen.sh
make
sudo make install
```

### (Optional) Step Five:
Compile the official vala compiler using the vala obtained through
Step Four.