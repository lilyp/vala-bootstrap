(use-modules (guix packages)
             (guix gexp)
             (guix build-system gnu)
             (guix download)
             (guix git-download)
             (guix utils)
             ((guix licenses) #:prefix license:)
             (gnu packages autotools)
             (gnu packages bison)
             (gnu packages docbook)
             (gnu packages flex)
             (gnu packages gettext)
             (gnu packages gnome))

(define current-directory (dirname (current-filename)))

(define vala-bootstrap
  (package
   (inherit vala-0.52)
   (name "vala-bootstrap")
   (source
    (local-file current-directory
                #:recursive? #t
                #:select?               ; only include version-controlled files,
                                        ; but allow building from an archive
                (if (file-exists? (string-append current-directory "/.git"))
                    (git-predicate current-directory)
                    (const #t))))
   (native-inputs (modify-inputs (package-native-inputs vala-0.52)
                     (prepend autoconf autoconf-archive automake libtool
                              gettext-minimal
                              bison flex)))
   (arguments (substitute-keyword-arguments (package-arguments vala-0.52)
                 ((#:phases phases)
                  #~(modify-phases #$phases
                      (delete 'pre-check)))))))

(package
 (inherit vala)
 (source (origin
          (inherit (package-source vala))
          (modules '((guix build utils)))
          (snippet
           #~(begin
               (for-each (lambda (dir)
                           (for-each delete-file
                                     (find-files dir "\\.(c|h|vapi|stamp)$")))
                         '("ccode" "codegen" "compiler" "gee" "vala"
                           "vapigen"))))))
 (arguments
  (substitute-keyword-arguments (package-arguments vala)
    ((#:phases phases)
     #~(modify-phases #$phases
         (replace 'use-gcc-by-default
           (lambda _
             (substitute* "codegen/valaccodecompiler.vala"
                (("cc_command = \"cc\"")
                 "cc_command = \"gcc\""))))))))
 (native-inputs (modify-inputs (package-native-inputs vala)
                  (prepend vala-bootstrap))))
